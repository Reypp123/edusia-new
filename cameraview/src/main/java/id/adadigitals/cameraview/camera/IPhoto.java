package id.adadigitals.cameraview.camera;

import android.hardware.Camera;

public class IPhoto {
    public final byte[] data;
    public final Camera.Size previewSize;
    public int rotation;

    public IPhoto(byte[] data, Camera.Size previewSize, int rotation) {
        this.data = data;
        this.previewSize = previewSize;
        this.rotation = rotation;
    }
}

