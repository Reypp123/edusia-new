package id.adadigitals.edusia.component;

import android.util.Base64;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import id.adadigitals.edusia.config.Config;

public class CipherComponent {
    private final Cipher cipher;
    public final static String passPhrase = "C58655D2589287BDF02FC4C6E32471D9A05B3F9A68B58579F4ABD26825E87C9C";
    public final static int KEY_SIZE = 128;
    public final static int ITERATION_COUNT = 12018;

    public static String HASH_ALGORITHM_SHA256 = "SHA-256";
    public static String HASH_ALGORITHM_MD5 = "MD5";

    public CipherComponent() {
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw fail(e);
        }
    }

    public String encrypt(String plaintext, File keyFile) {
        try {
            String salt = generateSignature(keyFile.getAbsolutePath(), HASH_ALGORITHM_SHA256);
            String iv = generateSignature(keyFile.getAbsolutePath(), HASH_ALGORITHM_MD5);
            SecretKey key = generateKey(salt, passPhrase);
            byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes("UTF-8"));
            return Base64.encodeToString(encrypted, Base64.DEFAULT);
        }
        catch (Exception e) {
            Config.getInstance().addLogs("ERROR CIPHER COMPONENT L: " + 53 + " --> " + e.getMessage());
            return null;
        }
    }

    public String decrypt(String encryptedtext, File keyFile){
        try {
            String salt = generateSignature(keyFile.getAbsolutePath(), HASH_ALGORITHM_SHA256);
            String iv = generateSignature(keyFile.getAbsolutePath(), HASH_ALGORITHM_MD5);
            SecretKey key = generateKey(salt, passPhrase);
            byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, Base64.decode(encryptedtext, Base64.DEFAULT));
            return new String(decrypted,"UTF-8");
        }
        catch (Exception e) {
            //Logr.printStackTrace(e);
            return null;
        }
    }

    private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
        try {
            cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
            return cipher.doFinal(bytes);
        } catch (InvalidKeyException
                | InvalidAlgorithmParameterException
                | IllegalBlockSizeException
                | BadPaddingException e) {
            throw fail(e);
        }
    }

    private SecretKey generateKey(String salt, String passPhrase) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), hex(salt), ITERATION_COUNT, KEY_SIZE);
            SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
            return key;
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw fail(e);
        }
    }

    public byte[] hex(String str) {
        try {
            return Hex.decodeHex(str.toCharArray());
        }
        catch (DecoderException e) {
            throw new IllegalStateException(e);
        }
    }

    private IllegalStateException fail(Exception e) {
        return new IllegalStateException(e);
    }

    public byte[] createSecret(String filename, String hashType) throws Exception {
        InputStream fis =  new FileInputStream(filename);

        byte[] buffer = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance(hashType);
        int numRead;

        do {
            numRead = fis.read(buffer);
            if (numRead > 0) {
                complete.update(buffer, 0, numRead);
            }
        } while (numRead != -1);

        fis.close();
        return complete.digest();
    }

    public String generateSignature(String filename, String hashType) throws Exception {
        byte[] b = createSecret(filename, hashType);
        String result = "";

        for (int i=0; i < b.length; i++) {
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }
}
