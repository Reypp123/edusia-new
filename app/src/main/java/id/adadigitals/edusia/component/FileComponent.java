package id.adadigitals.edusia.component;
import java.io.File;
import java.io.IOException;

import id.adadigitals.edusia.config.Config;

public class FileComponent {
    public static String stripExt(String fileName) {
        if (fileName == null)
            return null;
        int pos = fileName.lastIndexOf(".");
        if (pos == -1)
            return fileName;
        return fileName.substring(0, pos);
    }

    public static File getPhotoFile(int faceGroup, String uid) {
        try {
            File photoFile = new File(faceGroup == Config.ORTU_FACE_GROUP ?
                    Config.getInstance().getFolderFotoOrtu():Config.getInstance().getFolderFotoSiswa(), uid+".jpg");
            if(!photoFile.exists())
                photoFile.createNewFile();
            return photoFile;
        } catch (final IOException e) {
            Config.getInstance().addLogs("Unable to create photo file.");
            throw new RuntimeException("Unable to create photo file.");
        }
    }

    public static File getTemplateFile(int faceGroup, String uid) {
        Config.getInstance().addLogs("uid filecomponent -> " + uid);
        try {
            File templateFile = new File(faceGroup == Config.ORTU_FACE_GROUP ? Config.getInstance().getFolderFaceOrtu(): Config.getInstance().getFolderFaceStudent(), uid+".tmp");
            if(!templateFile.exists()){
                templateFile.createNewFile();
            }

            Config.getInstance().addLogs(" file template --> " + templateFile.getName());
            return templateFile;
        } catch (final IOException e) {
            Config.getInstance().addLogs("Unable to create template file.");
            throw new RuntimeException("Unable to create template file.");
        }
    }
}
