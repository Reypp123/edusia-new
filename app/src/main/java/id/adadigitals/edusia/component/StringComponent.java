package id.adadigitals.edusia.component;

import android.text.TextUtils;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.regex.Pattern;

import id.adadigitals.edusia.config.Config;

public class StringComponent {

    private static final String __LOG__ = "__StringComponent__";
    private static MessageDigest digest = null;

    private StringComponent(){
    }

    public static boolean isNull(String value){
        if(TextUtils.isEmpty(value))
            return true;
        else if (value.toLowerCase().equals("null"))
            return true;
        else
            return false;
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isValidPassword(String password){
        String ePattern = "(?=.*\\d)(?=.*[a-zA-Z]).{8,}";
        Pattern p       = Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(password);
        return m.matches();
    }

    public static String parseDomainName(String url){
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;
        }catch(URISyntaxException e){
            Log.e(__LOG__, "Error");
        }
        return url;
    }

    public static String formatThousands(String value){
        long longValue = Long.valueOf(value);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setMonetaryDecimalSeparator(',');
        symbols.setGroupingSeparator('.');
        formatter.setDecimalFormatSymbols(symbols);
        return formatter.format(longValue);
    }

    public synchronized static String hash(String data) {
        if (digest == null) {
            try {
                digest = MessageDigest.getInstance("SHA-1");
            }
            catch (NoSuchAlgorithmException nsae) {
                System.err.println("Failed to load the SHA-1 MessageDigest. " +
                        "Jive will be unable to function normally.");
            }
        }
        // Now, compute hash.
        try {
            digest.update(data.getBytes("UTF-8"));
        }
        catch (UnsupportedEncodingException e) {
            System.err.println(e);
        }
        return encodeHex(digest.digest());
    }

    public static String encodeHex(byte[] bytes){
        StringBuilder hex = new StringBuilder(bytes.length * 2);

        for (byte aByte : bytes) {
            if (((int) aByte & 0xff) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toString((int) aByte & 0xff, 16));
        }

        return hex.toString();
    }

    public static String upperCase( String val ){
        String s1 = val.substring(0, 1).toUpperCase();
        String nameCapitalized = s1 + val.substring(1);
        return nameCapitalized;
    }

    //2019 01 14 15 09 01
    public static String parseDKITimestamp(String number){
        try{
            String year = number.substring(0, 4);
            String month = number.substring(4, 6);
            String day = number.substring(6, 8);
            String hour = number.substring(8, 10);
            String minute = number.substring(10, 12);
            String second = number.substring(12);
            return day+"/"+month+"/"+year+" "+hour+":"+minute+":"+second;
        }catch(Exception e){
            Config.getInstance().addLogs("Error parseDKITimestamp --> " + e.getMessage());
        }
        return number;
    }

    private final static String NON_THIN = "[^iIl1\\.,']";

    private static int textWidth(String str) {
        return (int) (str.length() - str.replaceAll(NON_THIN, "").length() / 2);
    }

    public static String ellipsize(String text, int max) {

        if (textWidth(text) <= max)
            return text;

        // Start by chopping off at the word before max
        // This is an over-approximation due to thin-characters...
        int end = text.lastIndexOf(' ', max - 3);

        // Just one long word. Chop it off.
        if (end == -1)
            return text.substring(0, max-3) + "...";

        // Step forward as long as textWidth allows.
        int newEnd = end;
        do {
            end = newEnd;
            newEnd = text.indexOf(' ', end + 1);

            // No more spaces.
            if (newEnd == -1)
                newEnd = text.length();

        } while (textWidth(text.substring(0, newEnd) + "...") < max);

        return text.substring(0, end) + "...";
    }
}
