package id.adadigitals.edusia.component;

import android.util.Base64;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

public class DummyComponent {
    static byte[] TAGS = new byte[]{'F', 'j', 'G', 'm', 'D', '3', '4', '%', 'e', '@', 'r', '&', 't', '$', '=', '='};
    static char[] SMAS = new char[]{'s', '1', '3', '2', '7', 'a', 'z', '%', '%', '@', 'd', 'd', 'g', 'h', 'o', 's', 't', 'h', 'j', '='};

    public static String goHigh(String value) {
        try {
            KeySpec keySpec = new PBEKeySpec(SMAS, TAGS, 15);
            SecretKey key = SecretKeyFactory.getInstance(
                    "PBEWithMD5AndDES").generateSecret(keySpec);
            Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(TAGS, 15);
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            byte[] utf8 = value.getBytes("UTF8");
            byte[] enc = ecipher.doFinal(utf8);
            return Base64.encodeToString(enc, Base64.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String goLow(String encrypted) {
        try {
            KeySpec keySpec = new PBEKeySpec(SMAS, TAGS, 15);
            SecretKey key = SecretKeyFactory.getInstance(
                    "PBEWithMD5AndDES").generateSecret(keySpec);
            Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(TAGS, 15);
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] dec = Base64.decode(encrypted, Base64.DEFAULT);
            byte[] utf8 = dcipher.doFinal(dec);
            return new String(utf8, "UTF8");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
