package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.component.AesComponent;
import id.adadigitals.edusia.config.Config;

public class ListTransactionRequest {
    public Body body;

    public ListTransactionRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setUsername(String username){
        this.body.username = username;
    }

    public void setPassword(String pin){
        AesComponent aesUtils = new AesComponent();
        String password = aesUtils.encrypt(pin);
        this.body.password = password.substring(0,password.length()-1);
    }

    public void setAccount(String account){
        this.body.account = account;
    }

    public void setDateFrom(String dateFrom){
        this.body.dateFrom = dateFrom;
    }

    public void setDateTo(String dateTo){
        this.body.dateTo = dateTo;
    }

    public class Body {

        public String username;
        public String password;
        public String account;
        public String dateFrom;
        public String dateTo;
        public String enumChannel;
    }
}
