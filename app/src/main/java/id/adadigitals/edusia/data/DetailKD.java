package id.adadigitals.edusia.data;

public class DetailKD {
    public String id_penilaian;
    public String kode;
    public String aspek;
    public String nilai;

    public String getId_penilaian() {
        return id_penilaian;
    }

    public void setId_penilaian(String id_penilaian) {
        this.id_penilaian = id_penilaian;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getAspek() {
        return aspek;
    }

    public void setAspek(String aspek) {
        this.aspek = aspek;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
}
