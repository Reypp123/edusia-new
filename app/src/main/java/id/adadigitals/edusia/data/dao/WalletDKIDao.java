package id.adadigitals.edusia.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;

import id.adadigitals.edusia.data.wallet.WalletBankDKI;

@Dao
public interface WalletDKIDao {
    @Query("SELECT * FROM walletbankdki")
    List<WalletBankDKI> getAll();

    @Query("SELECT * FROM walletbankdki WHERE id = :walletid LIMIT 1")
    WalletBankDKI findByID(String walletid);

    @Query("SELECT * FROM walletbankdki WHERE msisdn = :msisdn LIMIT 1")
    WalletBankDKI findByMsisdn(String msisdn);

    @Query("SELECT * FROM walletbankdki WHERE account_number = :accnumber LIMIT 1")
    WalletBankDKI findByAccNumber(String accnumber);

    @Query("DELETE FROM walletbankdki WHERE account_number LIKE :accnumber")
    void deleteByAccNumber(String accnumber);

    @Insert
    void insertAll(List<WalletBankDKI> walletdkis);

    @Update
    void update(WalletBankDKI walletdki);

    @Delete
    void delete(WalletBankDKI walletdki);

    @Query("DELETE FROM walletbankdki")
    void deleteAll();
}
