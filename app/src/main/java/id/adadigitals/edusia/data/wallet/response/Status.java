package id.adadigitals.edusia.data.wallet.response;

public class Status {
    public String code;
    public String description;
    public String datetime;
}
