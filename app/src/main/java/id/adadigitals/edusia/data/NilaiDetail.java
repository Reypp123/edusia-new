package id.adadigitals.edusia.data;

import java.util.List;

public class NilaiDetail {
    public String id_ki;
    public String ki;
    public List<DataKd> kd;
    public String id_matpel;
    public String nis;
    public String nama;
    public String semester;
    public String ajaran;

    public String getId_ki() {
        return id_ki;
    }

    public void setId_ki(String id_ki) {
        this.id_ki = id_ki;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId_matpel() {
        return id_matpel;
    }

    public void setId_matpel(String id_matpel) {
        this.id_matpel = id_matpel;
    }

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getAjaran() {
        return ajaran;
    }

    public void setAjaran(String ajaran) {
        this.ajaran = ajaran;
    }

    public String getKi() {
        return ki;
    }

    public void setKi(String ki) {
        this.ki = ki;
    }

    public List<DataKd> getKd() {
        return kd;
    }

    public void setKd(List<DataKd> kd) {
        this.kd = kd;
    }
}
