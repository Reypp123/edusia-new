package id.adadigitals.edusia.data.wallet.response;

import id.adadigitals.edusia.config.Config;

public class FaultError {
    static public int CUSTOMER_NOT_FOUND = 2011;
    static public int ACCOUNT_FROM_NOT_FOUND = 6106;
    static public int TRANSACTION_NOT_ALLOWED = 2209;
    static public int PROCESS_SUCCESSFUL = 1000;
    static public int UNKNOWN_ERROR = 9999;
    static public int DATA_NOT_FOUND = 6000;
    static public int TRANSACTION_FAILED = 2222;
    static public int TRANSACTION_NOT_FOUND = 2201;
    static public int ACCOUNT_NOT_FOUND = 6100;
    static public int BAD_CREDENTIAL = 2001;
    static public int TOO_MANY_FAILED_LOGIN = 6111;
    static public int ACCOUNT_BLOCKED = 6104;

    int code = -1;
    String msg = null;

    String faultMessage = null;

    public FaultError(String faultMessage){
        this.faultMessage = faultMessage;
        try {
            String[] message = faultMessage.split(" : ");
            this.code = Integer.valueOf(message[0]);
            this.msg = message[1];
        }catch(Exception e){
            Config.getInstance().addLogs("Fa");
        }
    }

    public int getCode(){
        return code;
    }

    public String getMessage(){
        return msg;
    }

    public String getFaultMessage(){
        return faultMessage;
    }
}
