package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.config.Config;

public class AccountInformationRequest {
    public Body body;

    public AccountInformationRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setMsisdn(String msisdn){
        this.body.msisdn = msisdn;
    }

    public class Body {
        public String msisdn;
        public String enumChannel;
    }
}
