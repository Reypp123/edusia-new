package id.adadigitals.edusia.data;

import android.annotation.SuppressLint;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.util.Calendar;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;

public class KehadiranItem {
    public String siswa_nama;
    public String tgl;
    public String bulan;
    public String tahun;
    public String status_kehadiran;
    public String waktu_kehadiran;

    public CalendarDay date;

    @SuppressLint("ResourceType")
    public String getColor(){
        if( !StringComponent.isNull(status_kehadiran)){
            switch(status_kehadiran){
                case "H":
                    return Config.getInstance().getResources().getString(R.color.green_300);
                case "I":
                    return Config.getInstance().getResources().getString(R.color.blue_500);
                case "S":
                    return Config.getInstance().getResources().getString(R.color.yellow_200);
                default:
                    return Config.getInstance().getResources().getString(R.color.red_400);
            }
        }
        else {
            return Config.getInstance().getResources().getString(R.color.red_400);
        }
    }

    public String getStatus_kehadiran() {
        return status_kehadiran;
    }
}
