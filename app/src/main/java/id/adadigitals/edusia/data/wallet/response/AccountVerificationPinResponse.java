package id.adadigitals.edusia.data.wallet.response;

public class AccountVerificationPinResponse {
    public Status status;
    public Body body;

    public String getReference(){
        if(responseStatus()==1){
            return body.ForgotPasswordExternalResponse.reference._text;
        } else
            return null;
    }

    public String getId(){
        if(responseStatus()==1){
            return body.ForgotPasswordExternalResponse.id._text;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    public int responseStatus(){
        if(body!=null){
            if(body.ForgotPasswordExternalResponse!=null){
                return 1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public class Body{
        public ForgotPasswordExternalResponse ForgotPasswordExternalResponse;
        public Fault Fault;

        public class ForgotPasswordExternalResponse{

            public Reference reference;
            public Id id;

            public class Reference{
                public String _text;
            }

            public class Id{
                public String _text;
            }
        }
    }
}
