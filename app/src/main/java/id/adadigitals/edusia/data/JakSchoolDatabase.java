package id.adadigitals.edusia.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import id.adadigitals.edusia.data.dao.WalletDKIDao;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;

@Database(entities = {WalletBankDKI.class}, version = 1)
public abstract class JakSchoolDatabase extends RoomDatabase {
    public abstract WalletDKIDao walletDKIDao();
}
