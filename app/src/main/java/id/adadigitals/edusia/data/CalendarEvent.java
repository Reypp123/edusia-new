package id.adadigitals.edusia.data;

import android.annotation.SuppressLint;

import id.adadigitals.edusia.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;

public class CalendarEvent {
    public String tahun;
    public String bulan;
    public String tgl;
    public String color;
    public String title;
    public String description;
    public String jenis;

    public CalendarDay date;

    @SuppressLint("ResourceType")
    public String getColor(){
        if(StringComponent.isNull(color)){
            if(!StringComponent.isNull(jenis)){
                switch(jenis){
                    case "0":
                        return Config.getInstance().getResources().getString(R.color.blue_700);
                    case "1":
                        return Config.getInstance().getResources().getString(R.color.orange_700);
                    case "2":
                        return Config.getInstance().getResources().getString(R.color.green_700);
                    case "3":
                        return Config.getInstance().getResources().getString(R.color.pink_700);
                    case "4":
                        return Config.getInstance().getResources().getString(R.color.purple_700);
                    default:
                        return Config.getInstance().getResources().getString(R.color.colorPrimary);
                }
            }else
                return Config.getInstance().getResources().getString(R.color.colorPrimary);
        }else
            return color;
    }
}
