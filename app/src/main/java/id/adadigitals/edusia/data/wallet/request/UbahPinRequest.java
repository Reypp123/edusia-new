package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.component.AesComponent;
import id.adadigitals.edusia.config.Config;

public class UbahPinRequest {
    public Body body;

    public UbahPinRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setPassword( String pin ){
        AesComponent aesComponent = new AesComponent();
        String password = aesComponent.encrypt(pin);
        this.body.password = password.substring(0,password.length()-1);
    }

    public void setId( String id ){
        this.body.id = id;
    }

    public class Body{
        public String id;
        public String password;
        public String enumChannel;
    }
}
