package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.component.AesComponent;
import id.adadigitals.edusia.config.Config;

public class CheckAccountRequest {
    public Body body;

    public CheckAccountRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setUsername(String username){
        this.body.username = username;
    }

    public String getUsername(){ return this.body.username; }

    public void setPassword(String pin){
        AesComponent aesComponent = new AesComponent();
        String password = aesComponent.encrypt(pin);
        this.body.password = password.substring(0,password.length()-1);
    }

    public class Body {
        public String username;
        public String password;
        public String enumChannel;
    }
}
