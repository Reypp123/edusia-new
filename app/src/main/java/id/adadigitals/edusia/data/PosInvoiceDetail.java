package id.adadigitals.edusia.data;

public class PosInvoiceDetail {
    public String id_penjualan_m;
    public String nomor_nota;
    public String tanggal;
    public String grand_total;
    public String keterangan_lain;
    public String id_pelanggan;
    public String nis;

    public String getNomorNota() {
        return nomor_nota;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getGrandTotal() {
        return grand_total;
    }

    public String getKeterangan() {
        return keterangan_lain;
    }

    public String getNis() {
        return nis;
    }

    public String getIdPelanggan() {
        return id_pelanggan;
    }
}
