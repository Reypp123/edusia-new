package id.adadigitals.edusia.data;

public class TahunAkademik {

    public String idms_tahunakademik;
    public String tahunakademik_namatahun;

    public String getIdms_tahunakademik() {
        return idms_tahunakademik;
    }

    public void setIdms_tahunakademik(String idms_tahunakademik) {
        this.idms_tahunakademik = idms_tahunakademik;
    }

    public String getTahunakademik_namatahun() {
        return tahunakademik_namatahun;
    }

    public void setTahunakademik_namatahun(String tahunakademik_namatahun) {
        this.tahunakademik_namatahun = tahunakademik_namatahun;
    }
}
