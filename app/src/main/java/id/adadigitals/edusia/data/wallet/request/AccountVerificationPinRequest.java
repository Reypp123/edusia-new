package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.config.Config;

public class AccountVerificationPinRequest {

    public Body body;

    public AccountVerificationPinRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setEmail( String email ){
        this.body.email = email;
    }

    public void setDateOfBirth(String tanggal){
        this.body.dateOfBirth = tanggal;
    }

    public class Body {
        public String dateOfBirth;
        public String email;
        public String enumChannel;
    }
}
