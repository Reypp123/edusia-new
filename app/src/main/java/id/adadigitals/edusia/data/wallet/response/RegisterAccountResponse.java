package id.adadigitals.edusia.data.wallet.response;

public class RegisterAccountResponse {
    public Status status;
    public Body body;

    public String getId(){
        if(responseStatus()==1){
            return body.RegisterNonCustomerJomiResponse.customer.id._text;
        } else
            return null;
    }

    public String getName(){
        if(responseStatus()==1){
            return body.RegisterNonCustomerJomiResponse.customer.name._text;
        } else
            return null;
    }

    public String getUsername(){
        if(responseStatus()==1){
            return body.RegisterNonCustomerJomiResponse.customer.username._text;
        } else
            return null;
    }

    public String getMsisdn(){
        if(responseStatus()==1){
            return body.RegisterNonCustomerJomiResponse.customer.msisdn._text;
        } else
            return null;
    }

    public String getEmail(){
        if(responseStatus()==1){
            return body.RegisterNonCustomerJomiResponse.customer.email._text;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        int responseStatus = responseStatus();
        if(responseStatus == 1) {
            sb.append("ID: " + getId() + "\n");
            sb.append("Name: " + getName() + "\n");
            sb.append("Username: " + getUsername() + "\n");
            sb.append("MSISDN: " + getMsisdn() + "\n");
            sb.append("Email: " + getEmail());
        }else if(responseStatus() == 0){
            sb.append(getError());
        }else if(responseStatus() == -1){
            sb.append("FaultError Unknown");
        }
        return sb.toString();
    }

    public int responseStatus(){
        if(body!=null){
            if(body.RegisterNonCustomerJomiResponse!=null){
                if(body.RegisterNonCustomerJomiResponse.customer!=null)
                    return 1;
                else
                    return -1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public class Body {

        public RegisterNonCustomerJomiResponse RegisterNonCustomerJomiResponse;
        public Fault Fault;

        public class RegisterNonCustomerJomiResponse{

            public Customer customer;

            public class Customer{

                public Id id;
                public Name name;
                public Username username;
                public Msisdn msisdn;
                public Email email;

                public class Id{
                    public String _text;
                }

                public class Name{
                    public String _text;
                }

                public class Username{
                    public String _text;
                }

                public class Msisdn{
                    public String _text;
                }

                public class Email{
                    public String _text;
                }

            }

        }

    }
}
