package id.adadigitals.edusia.data.wallet.response;

public class CheckAccountResponse {
    public Status status;
    public Body body;

    public String getId(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.id._text;
        } else
            return null;
    }

    public String getAccountNumber(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.accountNumber._text;
        } else
            return null;
    }

    public String getNickname(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.nickname._text;
        } else
            return null;
    }

    public String getCustomerId(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.customerId._text;
        } else
            return null;
    }

    public String getAmount(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.amount._text;
        } else
            return null;
    }

    public String getType(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.type._text;
        } else
            return null;
    }

    public String getAccountStatus(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.accountStatus._text;
        } else
            return null;
    }

    public String getAgentAccount(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.agentAccount._text;
        } else
            return null;
    }

    public String getProductName(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.productName._text;
        } else
            return null;
    }

    public String getSystemId(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.systemId._text;
        } else
            return null;
    }

    public String getDefaultAccount(){
        if(responseStatus()==1){
            return body.GetListPrimaryAccountResponse.VirtualAccount.defaultAccount._text;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        int responseStatus = responseStatus();
        if(responseStatus == 1) {
            sb.append("ID: " + getId() + "\n");
            sb.append("Account Number: " + getAccountNumber() + "\n");
            sb.append("Nickname: " + getNickname() + "\n");
            sb.append("Customer ID: " + getCustomerId() + "\n");
            sb.append("Amount: " + getAmount() + "\n");
            sb.append("Type: " + getType() + "\n");
            sb.append("Account Status: " + getAccountStatus() + "\n");
            sb.append("Agent Account: " + getAgentAccount() + "\n");
            sb.append("Product Name: " + getProductName() + "\n");
            sb.append("System ID: " + getSystemId() + "\n");
            sb.append("Default Account: " + getDefaultAccount() + "\n");
        }else if(responseStatus() == 0){
            sb.append(getError());
        }else if(responseStatus() == -1){
            sb.append("FaultError Unknown");
        }
        return sb.toString();
    }

    public int responseStatus(){
        if(body!=null){
            if(body.GetListPrimaryAccountResponse!=null){
                if(body.GetListPrimaryAccountResponse.VirtualAccount!=null)
                    return 1;
                else
                    return -1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public class Body {

        public GetListPrimaryAccountResponse GetListPrimaryAccountResponse;
        public Fault Fault;

        public class GetListPrimaryAccountResponse{

            public VirtualAccount VirtualAccount;

            public class VirtualAccount{

                public Id id;
                public AccountNumber accountNumber;
                public Nickname nickname;
                public CustomerId customerId;
                public Amount amount;

                public Type type;
                public AccountStatus accountStatus; //boolean
                public AgentAccount agentAccount; //boolean
                public ProductName productName;
                public SystemId systemId;
                public DefaultAccount defaultAccount; //boolean

                public class Id{
                    public String _text;
                }

                public class AccountNumber{
                    public String _text;
                }

                public class Nickname{
                    public String _text;
                }

                public class CustomerId{
                    public String _text;
                }

                public class Amount{
                    public String _text;
                }

                public class Type{
                    public String _text;
                }

                public class AccountStatus{
                    public String _text;
                }

                public class AgentAccount{
                    public String _text;
                }

                public class ProductName{
                    public String _text;
                }

                public class SystemId{
                    public String _text;
                }

                public class DefaultAccount{
                    public String _text;
                }

            }

        }

    }
}
