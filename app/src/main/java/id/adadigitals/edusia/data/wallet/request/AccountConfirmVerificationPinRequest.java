package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.config.Config;

public class AccountConfirmVerificationPinRequest {

    public Body body;

    public AccountConfirmVerificationPinRequest(){
        this.body = new Body();
        this.body.enumChannel = Config.WALLET_ENUM_CHANNEL;
    }

    public void setReference(String reference){
        this.body.reference = reference;
    }

    public void setOtp(String otp){
        this.body.otp = otp;
    }

    public class Body{
        public String reference;
        public String otp;
        public String enumChannel;
    }
}
