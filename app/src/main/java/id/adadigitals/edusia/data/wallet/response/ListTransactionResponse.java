package id.adadigitals.edusia.data.wallet.response;

import java.util.List;

public class ListTransactionResponse {
    public Status status;
    public Body body;

    public List<Body.TrxHistoryEnquiryResponse.TrxHistory> getTrxHistory(){
        if(responseStatus()==1){
            return body.TrxHistoryEnquiryResponse.trxHistory;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        int responseStatus = responseStatus();
        if(responseStatus == 1) {
            List<Body.TrxHistoryEnquiryResponse.TrxHistory> trxHistories = getTrxHistory();
            for(int i=0; i<trxHistories.size();i++) {
                sb.append("Description: " + trxHistories.get(i).description._text + "\n");
                sb.append("Debit: " + trxHistories.get(i).debit._text + "\n");
                sb.append("Charge: " + trxHistories.get(i).charge._text + "\n");
                sb.append("Commission: " + trxHistories.get(i).commission._text + "\n");
                sb.append("Transaction Date: " + trxHistories.get(i).transactionDate._text + "\n");
                sb.append("No Refference Trx: " + trxHistories.get(i).noRefferenceTrx._text+ "\n");
                sb.append("No Reff: " + trxHistories.get(i).noreff._text + "\n");
                sb.append("Name: " + trxHistories.get(i).name._text + "\n");
                sb.append("Acct From: " + trxHistories.get(i).acctFrom._text + "\n");
                sb.append("Acct From Merchant: " + trxHistories.get(i).acctFromMerchant._text+ "\n");
                sb.append("Acct From Name Merchant: " + trxHistories.get(i).acctFromNameMerchant._text + "\n");
                if(i+1<trxHistories.size())
                    sb.append("================================================================\n");
            }
        }else if(responseStatus() == 0){
            sb.append(getError());
        }else if(responseStatus() == -1){
            sb.append("FaultError Unknown");
        }
        return sb.toString();
    }

    public int responseStatus(){
        if(body!=null){
            if(body.TrxHistoryEnquiryResponse!=null){
                return 1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public boolean isNotEmpty(){
        if(body!=null){
            if(body.TrxHistoryEnquiryResponse!=null
                    && body.TrxHistoryEnquiryResponse.trxHistory!=null) {
                return body.TrxHistoryEnquiryResponse.trxHistory.size() > 0;
            }
        }
        return false;
    }

    public class Body {

        public TrxHistoryEnquiryResponse TrxHistoryEnquiryResponse;
        public Fault Fault;

        public class TrxHistoryEnquiryResponse{

            public List<TrxHistory> trxHistory;

            public class TrxHistory{

                public Description description;
                public Debit debit;
                public Credit credit;
                public Charge charge;
                public Commission commission;
                public TransactionDate transactionDate;

                public NoRefferenceTrx noRefferenceTrx;
                public Noreff noreff;
                public Name name;
                public AcctFrom acctFrom;
                public AcctFromMerchant acctFromMerchant;
                public AcctFromNameMerchant acctFromNameMerchant;

                public String getDescription(){
                    if(description!=null){
                        return description._text;
                    } else
                        return null;
                }

                public String getDebit(){
                    if(debit!=null){
                        return debit._text;
                    } else
                        return null;
                }

                public String getCredit(){
                    if(credit!=null){
                        return credit._text;
                    } else
                        return null;
                }

                public String getCharge(){
                    if(charge!=null){
                        return charge._text;
                    } else
                        return null;
                }

                public String getCommission(){
                    if(commission!=null){
                        return commission._text;
                    } else
                        return null;
                }

                public String getTransactionDate(){
                    if(transactionDate!=null){
                        return transactionDate._text;
                    } else
                        return null;
                }

                public String getNoRefferenceTrx(){
                    if(noRefferenceTrx!=null){
                        return noRefferenceTrx._text;
                    } else
                        return null;
                }

                public String getNoreff(){
                    if(noreff!=null){
                        return noreff._text;
                    } else
                        return null;
                }

                public String getName(){
                    if(name!=null){
                        return name._text;
                    } else
                        return null;
                }

                public String getAcctFrom(){
                    if(acctFrom!=null){
                        return acctFrom._text;
                    } else
                        return null;
                }

                public String getAcctFromMerchant(){
                    if(acctFromMerchant!=null){
                        return acctFromMerchant._text;
                    } else
                        return null;
                }

                public String getAcctFromNameMerchant(){
                    if(acctFromNameMerchant!=null){
                        return acctFromNameMerchant._text;
                    } else
                        return null;
                }

                public class Description{
                    public String _text;
                }

                public class Debit{
                    public String _text;
                }

                public class Credit{
                    public String _text;
                }

                public class Charge{
                    public String _text;
                }

                public class Commission{
                    public String _text;
                }

                public class TransactionDate{
                    public String _text;
                }

                public class NoRefferenceTrx{
                    public String _text;
                }

                public class Noreff{
                    public String _text;
                }

                public class Name{
                    public String _text;
                }

                public class AcctFrom{
                    public String _text;
                }

                public class AcctFromMerchant{
                    public String _text;
                }

                public class AcctFromNameMerchant{
                    public String _text;
                }

            }

        }

    }
}
