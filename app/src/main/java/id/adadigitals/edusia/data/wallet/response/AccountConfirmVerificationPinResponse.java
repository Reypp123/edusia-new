package id.adadigitals.edusia.data.wallet.response;

public class AccountConfirmVerificationPinResponse {

    public Status status;
    public Body body;

    public String getId(){
        if(responseStatus()==1){
            return body.ForgotPasswordExternalConfirmResponse.id._text;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    public int responseStatus(){
        if(body!=null){
            if(body.ForgotPasswordExternalConfirmResponse!=null){
                return 1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public class Body{
        public ForgotPasswordExternalConfirmResponse ForgotPasswordExternalConfirmResponse;
        public Fault Fault;

        public class ForgotPasswordExternalConfirmResponse{

            public Id id;
            public Reference reference;

            public class Id{
                public String _text;
            }

            public class Reference{
                public String _text;
            }
        }
    }
}
