package id.adadigitals.edusia.data;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;

import java.io.File;

public class Student {
    public String siswaid;
    public String siswa_nis;
    public String siswa_sekolah;
    public String siswa_sekolah_terdahulu;
    public String siswa_nik;
    public String siswa_akte_lahir;
    public String siswa_warganegara;
    public String siswa_bangsa;
    public String siswa_nama;
    public String siswa_id_jenis_kelamin;
    public String siswa_tempat_lahir;
    public String siswa_tanggal_lahir;
    public String siswa_id_agama;
    public String siswa_riwayat;
    public String siswa_alamat;
    public String siswa_telepon;
    public String siswa_hp;
    public String siswa_foto;
    public String siswa_tgl_daftar;
    public String siswa_tgl_tamat;
    public String siswa_nama_ayah;
    public String siswa_alamat_ayah;
    public String siswa_pekerjaan_ayah;
    public String siswa_penghasilan_ayah;
    public String siswa_perusahaan_ayah;
    public String siswa_notlp_perusahaan_ayah;
    public String siswa_no_telpon_ayah;
    public String siswa_nohp_ayah;
    public String siswa_email_ayah;
    public String siswa_nik_ayah;
    public String tempat_lahir_ayah;
    public String tanggal_lahir_ayah;
    public String siswa_foto_ayah;
    public String siswa_nama_ibu;
    public String siswa_nik_ibu;
    public String tempat_lahir_ibu;
    public String tanggal_lahir_ibu;
    public String siswa_pekerjaan_ibu;
    public String siswa_perusahaan_ibu;
    public String siswa_email_ibu;
    public String siswa_alamat_ibu;
    public String siswa_penghasilan_ibu;
    public String siswa_no_telpon_ibu;
    public String siswa_nohp_ibu;
    public String siswa_notlp_kantor_ibu;
    public String siswa_pic_ibu;
    public String siswa_tingkat_kelas;
    public String siswa_kelas;
    public String siswa_sub_kelas;
    public String siswa_status_awal;
    public String siswa_status_siswa;
    public String siswa_tahun_pelajaran;
    public String registration_status;
    public String cdate;
    public String logdate;
    public String parent_wallet_id;
    public String student_wallet_id;
    public String parent_face_template;
    public String student_face_template;
    public String sekolah_namasekolah;
    public String kelas_nama_kelas;
    public String subkelas_nama;
    public String siswa_daily_limit;
    public String tabungan_nominal_akhir;

    public String getId() {
        return siswaid;
    }

    public String getNIS() {
        return siswa_nis;
    }

    public String getSekolah() {
        return sekolah_namasekolah;
    }

    public String getNIK() {
        return siswa_nik;
    }

    public String getAkteLahir() {
        return siswa_akte_lahir;
    }

    public String getWargaNegara() {
        return siswa_warganegara;
    }

    public String getBangsa() {
        return siswa_bangsa;
    }

    public String getNama() {
        return siswa_nama;
    }

    public String getJenisKelamin() {
        return siswa_id_jenis_kelamin;
    }

    public String getTempatLahir() {
        return siswa_tempat_lahir;
    }

    public String getTanggalLahir() {
        return siswa_tanggal_lahir;
    }

    public String getAgama() {
        return siswa_id_agama;
    }

    public String getRiwayat() {
        return siswa_riwayat;
    }

    public String getAlamat() {
        return siswa_alamat;
    }

    public String getHP() {
        return siswa_hp;
    }

    public String getTlp() {
        return siswa_telepon;
    }

    public String getFoto() {
        return siswa_foto;
    }

    public String getTingkatKelas() {
        return siswa_tingkat_kelas;
    }

    public String getKelas() {
        return siswa_kelas;
    }

    public String getSubKelas() {
        return subkelas_nama;
    }

    public String getTahunPelajaran() {
        return siswa_tahun_pelajaran;
    }

    public String getDailyLimit() {
        return siswa_daily_limit;
    }

    public String getStudentFaceTemplate() {
        return student_face_template;
    }

    public String getStudentEncryptedWalletUsername() {
        return student_wallet_id;
    }

    public String getSaldoTabungan() {
        return tabungan_nominal_akhir;
    }

    public String getAyahNama() {
        return siswa_nama_ayah;
    }

    public String getAyahAlamat() {
        return siswa_alamat_ayah;
    }

    public String getAyahPenghasilan() {
        return siswa_penghasilan_ayah;
    }

    public String getAyahPekerjaan() {
        return siswa_pekerjaan_ayah;
    }

    public String getAyahPerusahaan() {
        return siswa_perusahaan_ayah;
    }

    public String getAyahNoTelpPerusahaan() {
        return siswa_notlp_perusahaan_ayah;
    }

    public String getAyahNoTelp() {
        return siswa_no_telpon_ayah;
    }

    public String getAyahNoHp() {
        return siswa_nohp_ayah;
    }

    public String getAyahEmail() {
        return siswa_email_ayah;
    }

    public String getAyahNIK() {
        return siswa_nik_ayah;
    }

    public String getAyahFoto() {
        return siswa_foto_ayah;
    }

    public String getAyahTglLahir() {
        return tanggal_lahir_ayah;
    }

    public String getAyahTmptLahir() {
        return tempat_lahir_ayah;
    }

    public String getIbuNama() {
        return siswa_nama_ibu;
    }

    public String getIbuNIK() {
        return siswa_nik_ibu;
    }

    public String getIbuEmail() {
        return siswa_email_ibu;
    }

    public String getIbuAlamat() {
        return siswa_alamat_ibu;
    }

    public String getIbuPenghasilan() {
        return siswa_penghasilan_ibu;
    }

    public String getIbuPekerjaan() {
        return siswa_pekerjaan_ibu;
    }

    public String getIbuPerusahaan() {
        return siswa_perusahaan_ibu;
    }

    public String getIbuNoTelp() {
        return siswa_no_telpon_ibu;
    }

    public String getIbuNoHp() {
        return siswa_nohp_ibu;
    }

    public String getIbuNoTelpPerusahaan() {
        return siswa_notlp_kantor_ibu;
    }

    public String getIbuFoto() {
        return siswa_pic_ibu;
    }

    public String getIbuTglLahir() {
        return tanggal_lahir_ibu;
    }

    public String getIbuTmptLahir() {
        return tempat_lahir_ibu;
    }

    public String getParentFaceTemplate() {
        return parent_face_template;
    }

    public String getParentEncryptedWalletUsername() {
        return parent_wallet_id;
    }

    public void setSiswaNama(String siswa_nama) {
        this.siswa_nama = siswa_nama;
    }

    public void setAlamat(String alamat){
        this.siswa_alamat = alamat;
    }

    public void setHP(String hp){
        this.siswa_hp = hp;
    }

    public void setTlp(String tlp){
        this.siswa_telepon = tlp;
    }

    public void setFoto(String foto){
        this.siswa_foto = foto;
    }

    public void setDailyLimit(String dailyLimit){
        this.siswa_daily_limit = dailyLimit;
    }

    public void setFaceTemplate(String faceTemplate){
        this.student_face_template = faceTemplate;
    }

    public void setStudentEncryptedWalletUsername(String encryptedWalletUsername){
        this.student_wallet_id = encryptedWalletUsername;
    }

    public void setAyahAlamat(String alamatAyah){
        this.siswa_alamat_ayah = alamatAyah;
    }

    public void setAyahNoTelp(String ayahNoTelp){
        this.siswa_no_telpon_ayah = ayahNoTelp;
    }

    public void setAyahNoTelpPerusahaan(String ayahNoTelpPerusahaan){
        this.siswa_notlp_perusahaan_ayah = ayahNoTelpPerusahaan;
    }

    public void setAyahEmail(String ayahEmail){
        this.siswa_email_ayah = ayahEmail;
    }

    public void setAyahFoto(String ayahFoto){
        this.siswa_foto_ayah = ayahFoto;
    }

    public void setAyahTglLahir(String ayahTglLahir){
        this.tanggal_lahir_ayah = ayahTglLahir;
    }

    public void setAyahTmptLahir(String ayahTmptLahir){
        this.tempat_lahir_ayah = ayahTmptLahir;
    }

    public void setIbuAlamat(String alamatIbu){
        this.siswa_alamat_ibu = alamatIbu;
    }

    public void setIbuNoTelp(String ibuNoTelp){
        this.siswa_no_telpon_ibu = ibuNoTelp;
    }

    public void setIbuNoTelpPerusahaan(String ibuNoTelpPerusahaan){
        this.siswa_notlp_kantor_ibu = ibuNoTelpPerusahaan;
    }

    public void setIbuEmail(String ibuEmail){
        this.siswa_email_ibu = ibuEmail;
    }

    public void setIbuFoto(String ibuFoto){
        this.siswa_pic_ibu = ibuFoto;
    }

    public void setIbuTglLahir(String ibuTglLahir){
        this.tanggal_lahir_ibu = ibuTglLahir;
    }

    public void setIbuTmptLahir(String ibuTmptLahir){
        this.tempat_lahir_ibu = ibuTmptLahir;
    }

    public void setParentFaceTemplate(String faceTemplate){
        this.parent_face_template = faceTemplate;
    }

    public void setParentEncryptedWalletUsername(String encryptedWalletUsername){
        this.parent_wallet_id = encryptedWalletUsername;
    }

    public boolean isWaliIbu(String hpIbu){
        if(StringComponent.isNull(hpIbu) || StringComponent.isNull(siswa_nohp_ibu))
        {
            return false;
        }

        return siswa_nohp_ibu.equals(hpIbu);
    }

    public boolean isWaliAyah(String hpAyah){
        if( StringComponent.isNull(hpAyah) || StringComponent.isNull(siswa_nohp_ayah)){
            return false;
        }

        return siswa_nohp_ayah.equals(hpAyah);
    }

    public String getSiswaFotoUrl(){
        if( StringComponent.isNull(getFoto()) ){
            return null;
        }
//        return (Config.DEBUG_MODE == true) ? Config.URL_PHOTO_TEMPLATE_DEVEL+siswa_foto : Config.URL_PHOTO_TEMPLATE_PROD+siswa_foto;
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL+siswa_foto : Config.URL_PHOTO_TEMPLATE_PROD+siswa_foto;
    }

    public String getAyahFotoUrl(){
        if(StringComponent.isNull(getAyahFoto())){
            return null;
        }
//        return (Config.DEBUG_MODE == true) ? Config.URL_PHOTO_TEMPLATE_DEVEL+getAyahFoto() : Config.URL_PHOTO_TEMPLATE_PROD+getAyahFoto();
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL+getAyahFoto() : Config.URL_PHOTO_TEMPLATE_PROD+getAyahFoto();
    }

    public String getIbuFotoUrl(){
        if(StringComponent.isNull(getIbuFoto())){
            return null;
        }
//        return (Config.DEBUG_MODE == true) ? Config.URL_PHOTO_TEMPLATE_DEVEL+getIbuFoto() : Config.URL_PHOTO_TEMPLATE_PROD+getIbuFoto();
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL+getIbuFoto() : Config.URL_PHOTO_TEMPLATE_PROD+getIbuFoto();
    }

    public File getAyahPhotoFile() {
        if(StringComponent.isNull(getAyahFoto())){
            return null;
        }

        return new File(Config.getInstance().getFolderFotoOrtu(), getAyahFoto());
    }

    public File getIbuPhotoFile() {
        if(StringComponent.isNull(getIbuFoto())){
            return null;
        }

        return new File(Config.getInstance().getFolderFotoOrtu(), getIbuFoto());
    }

    public File getStudentPhotoFile() {
        if(StringComponent.isNull(getFoto())){
            return null;
        }
        return new File(Config.getInstance().getFolderFotoSiswa(), getFoto());
    }

    public String getParentTemplateUrl() {
        if(StringComponent.isNull(getParentFaceTemplate())){
            return null;
        }

//        return (Config.DEBUG_MODE == true) ? Config.URL_PHOTO_TEMPLATE_DEVEL+getParentFaceTemplate() : Config.URL_PHOTO_TEMPLATE_PROD+getParentFaceTemplate();
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL+getParentFaceTemplate() : Config.URL_PHOTO_TEMPLATE_PROD+getParentFaceTemplate();
    }

    public String getParentFaceTemplateReplace(){
        if( StringComponent.isNull(getParentFaceTemplate()) ){
            return null;
        }
        else {
            String getParentFaceTemplate = getParentFaceTemplate();
            String replace               = getParentFaceTemplate.replace("_","");
            return replace;
        }
    }

    public File getParentTemplateFile() {
        if(StringComponent.isNull(getParentFaceTemplateReplace())){
            return null;
        }

        return new File(Config.getInstance().getFolderFaceOrtu(), getParentFaceTemplateReplace());
    }

    public String getStudentTemplateUrl() {
        if(StringComponent.isNull(getStudentFaceTemplate())){
            return null;
        }
//        return (Config.DEBUG_MODE == true) ? Config.URL_PHOTO_TEMPLATE_DEVEL+getStudentFaceTemplate() : Config.URL_PHOTO_TEMPLATE_PROD+getStudentFaceTemplate();
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL+getStudentFaceTemplate() : Config.URL_PHOTO_TEMPLATE_PROD+getStudentFaceTemplate();
    }

    public File getStudentTemplateFile() {
        if(StringComponent.isNull(getStudentFaceTemplate())){
            return null;
        }
        return new File(Config.getInstance().getFolderFaceStudent(), getStudentFaceTemplate());
    }

    public boolean isParentTemplateExists(String nik){
        if(StringComponent.isNull(getParentFaceTemplate())){
            return false;
        }

        if(getParentFaceTemplate().startsWith(nik)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean isStudentTemplateExists(){
        if(StringComponent.isNull(getStudentFaceTemplate())){
            return false;
        }

        if(getStudentFaceTemplate().startsWith(getNIS())){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean isStudentPhotoExist(){
        if( StringComponent.isNull(getFoto()) ){
            return false;
        }

        if( getFoto().startsWith(getNIS()) ){
            return true;
        }
        else {
            return false;
        }
    }

}
