package id.adadigitals.edusia.data.wallet.request;

import id.adadigitals.edusia.component.AesComponent;
import id.adadigitals.edusia.config.Config;

public class RegisterAccountRequest {
    public Body body;

    public RegisterAccountRequest(){
        this.body = new Body();
        this.body.productName = Config.WALLET_ENUM_CHANNEL;
    }

    public void setFirstName(String firstName){
        this.body.firstName = firstName;
    }

    public void setLastName(String lastName){
        this.body.lastName = lastName;
    }

    public void setPlaceOfBirth(String placeOfBirth){
        this.body.placeOfBirth = placeOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth){
        this.body.dateOfBirth = dateOfBirth;
    }

    public void setPassword(String pin){
        AesComponent aesComponent = new AesComponent();
        String password = aesComponent.encrypt(pin);
        this.body.password = password.substring(0,password.length()-1);
    }

    public void setMsisdn(String msisdn){
        this.body.msisdn = msisdn;
    }

    public void setEmail(String email){
        this.body.email = email;
    }

    public class Body {
        public String firstName;
        public String lastName;
        public String placeOfBirth;
        public String dateOfBirth;
        public String password;
        public String msisdn;
        public String email;
        public String productName;
    }
}
