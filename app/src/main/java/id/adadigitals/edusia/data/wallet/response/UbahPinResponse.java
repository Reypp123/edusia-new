package id.adadigitals.edusia.data.wallet.response;

import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;

public class UbahPinResponse {
    public Status status;
    public Body body;

    public String getId(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.id._text;
        } else
            return null;
    }

    public String getUsername(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.username._text;
        } else
            return null;
    }

    public String getFirstName(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.firstName._text;
        } else
            return null;
    }

    public String getLastName(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.lastName._text;
        } else
            return null;
    }

    public String getPlaceOfBirth(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.placeOfBirth._text;
        } else
            return null;
    }

    public String getDateOfBirth(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.dateOfBirth._text;
        } else
            return null;
    }

    public String getMsisdn(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.msisdn._text;
        } else
            return null;
    }

    public String getEmail(){
        if(responseStatus()==1){
            return body.ExecuteNewPasswordExternalResponse.email._text;
        } else
            return null;
    }

    public FaultError getError(){
        if(body!=null && body.Fault != null){
            return new FaultError(body.Fault.faultstring._text);
        }
        return null;
    }

    public int responseStatus(){
        if(body!=null){
            if(body.ExecuteNewPasswordExternalResponse!=null){
                return 1;
            }else if(body.Fault!=null){
                return 0;
            }else
                return -1;
        }else{
            return -1;
        }
    }

    public WalletBankDKI getWalletInfo(){
        try {
            if (responseStatus() == 1) {
                WalletBankDKI walletDKI = new WalletBankDKI();
                walletDKI.setId(getId());
                walletDKI.setUsername(getUsername());
                walletDKI.setFirst_name(getFirstName());
                walletDKI.setLast_name(getLastName());
                walletDKI.setBirth_place(getPlaceOfBirth());
                walletDKI.setBirth_date(getDateOfBirth());
                walletDKI.setMsisdn(getMsisdn());
                walletDKI.setEmail(getEmail());
                return walletDKI;
            }
        }catch (Exception e){
            Config.getInstance().addLogs("=================== "+e.getMessage());
        }
        return null;
    }

    public class Body{
        public ExecuteNewPasswordExternalResponse ExecuteNewPasswordExternalResponse;
        public Fault Fault;

        public class ExecuteNewPasswordExternalResponse{

            public Id id;
            public Username username;
            public FirstName firstName;
            public LastName lastName;
            public PlaceOfBirth placeOfBirth;
            public DateOfBirth dateOfBirth;
            public Msisdn msisdn;
            public Email email;

            public class Id {
                public String _text;
            }

            public class Username {
                public String _text;
            }

            public class FirstName {
                public String _text;
            }

            public class LastName {
                public String _text;
            }

            public class PlaceOfBirth {
                public String _text;
            }

            public class DateOfBirth {
                public String _text;
            }

            public class Msisdn {
                public String _text;
            }

            public class Email {
                public String _text;
            }
        }
    }
}
