package id.adadigitals.edusia.data;

public class TimeLine {
    public String tgl_timeline;
    public String cdate;
    public String msg_timeline;

    public String getTgl_timeline() {
        return tgl_timeline;
    }

    public void setTgl_timeline(String tgl_timeline) {
        this.tgl_timeline = tgl_timeline;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getMsg_timeline() {
        return msg_timeline;
    }

    public void setMsg_timeline(String msg_timeline) {
        this.msg_timeline = msg_timeline;
    }
}
