package id.adadigitals.edusia.data.wallet.response;

public class Fault {
    public Faultcode faultcode;
    public Faultstring faultstring;

    public class Faultcode {
        public String _text;
    }

    public class Faultstring {
        public Attr _attr;
        public String _text;

        public class Attr {
            public Lang lang;
            public class Lang {
                public String _value;
            }
        }
    }
}
