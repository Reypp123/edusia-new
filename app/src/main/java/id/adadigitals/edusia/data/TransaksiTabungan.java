package id.adadigitals.edusia.data;

public class TransaksiTabungan {
    public String idtransaksi;
    public String transaksi_tabungan_no;
    public String transaksi_tgl;
    public String transaksi_ket;
    public String transaksi_debit;
    public String transaksi_kredit;
    public String transaksi_saldo;
    public String id_keu_tabungan_idtabungan;
    public String cdate;

    public String getIdtransaksi() {
        return idtransaksi;
    }

    public String getTransaksiNo() {
        return transaksi_tabungan_no;
    }

    public String getTgl() {
        return transaksi_tgl;
    }

    public String getDebit() {
        return transaksi_debit;
    }

    public String getKredit() {
        return transaksi_kredit;
    }

    public String getSaldo() {
        return transaksi_saldo;
    }
}
