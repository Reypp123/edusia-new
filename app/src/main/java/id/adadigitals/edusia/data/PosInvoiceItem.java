package id.adadigitals.edusia.data;

public class PosInvoiceItem {
    public String kode_barang;
    public String nama_barang;
    public String id_barang;
    public String jumlah_beli;
    public String harga_satuan;
    public String total;

    public String getKodeBarang() {
        return kode_barang;
    }

    public String getNamaBarang() {
        return nama_barang;
    }

    public String getIdBarang() {
        return id_barang;
    }

    public String getJumlahBeli() {
        return jumlah_beli;
    }

    public String getHargaSatuan() {
        return harga_satuan;
    }

    public String getTotal() {
        return total;
    }
}
