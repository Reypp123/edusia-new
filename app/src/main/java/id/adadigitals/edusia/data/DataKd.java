package id.adadigitals.edusia.data;

import java.util.List;

public class DataKd {
    public String nama;
    public String nilai_rd;
    public String nilai_py;
    public String nilai_nh;
    public List<DetailKD> detail;

    public String getNilai_rd() {
        return nilai_rd;
    }

    public void setNilai_rd(String nilai_rd) {
        this.nilai_rd = nilai_rd;
    }

    public String getNilai_py() {
        return nilai_py;
    }

    public void setNilai_py(String nilai_py) {
        this.nilai_py = nilai_py;
    }

    public String getNilai_nh() {
        return nilai_nh;
    }

    public void setNilai_nh(String nilai_nh) {
        this.nilai_nh = nilai_nh;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<DetailKD> getDetail() {
        return detail;
    }

    public void setDetail(List<DetailKD> detail) {
        this.detail = detail;
    }
}
