package id.adadigitals.edusia.dialog;

import android.support.v4.app.DialogFragment;

public interface OnDialogSelectionListener {
    void onSelected(DialogFragment dialog, int which);
}
