package id.adadigitals.edusia.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import id.adadigitals.edusia.component.StringComponent;

public class SingleSelectionDialog extends DialogFragment implements OnClickListener {
    public OnDialogSelectionListener onDialogSelectionListener;
    public int mDialogId;
    public String mTitle;
    public int mItemsId = -1;
    public String[] mItems;

    public static SingleSelectionDialog newInstance(OnDialogSelectionListener onDialogSelectionListener, int dialogId, String title, String[] items){
        SingleSelectionDialog singleSelectionDialog = new SingleSelectionDialog();
        singleSelectionDialog.setOnDialogSelectionListener(onDialogSelectionListener);
        singleSelectionDialog.setDialogId(dialogId);
        singleSelectionDialog.setTitle(title);
        singleSelectionDialog.setSelections(items);
        return singleSelectionDialog;
    }

    public SingleSelectionDialog(){

    }

    public void setDialogId(int dialogId){
        this.mDialogId = dialogId;
    }

    public int getDialogId(){
        return mDialogId;
    }

    public void setSelections(String[] items){
        this.mItems = items;
    }

    public void setOnDialogSelectionListener(OnDialogSelectionListener onDialogSelectionListener){
        this.onDialogSelectionListener = onDialogSelectionListener;
    }

    public void setTitle(String title){
        this.mTitle = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        if(!StringComponent.isNull(mTitle))
            dialog.setTitle(mTitle);
        if(mItems != null)
            dialog.setItems(mItems, this);
        else
            dialog.setItems(mItemsId, this);
        dialog.setCancelable(false);
        return dialog.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        onDialogSelectionListener.onSelected(this, which);
    }
}
