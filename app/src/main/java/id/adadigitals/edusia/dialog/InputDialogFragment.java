package id.adadigitals.edusia.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StringComponent;

public class InputDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
    public static final String ARGUMENT_TITLE = "id.adadigitals.studo.xui.dialog.InputDialogFragment.ARGUMENT_TITLE";
    public static final String ARGUMENT_VALUE = "id.adadigitals.studo.xui.dialog.InputDialogFragment.ARGUMENT_VALUE";
    private EditText inputEdTxt;

    String currValue = null;
    private OnInputConfirmedListener onInputConfirmedListener;

    public static InputDialogFragment newInstance(String title, String currentValue) {
        InputDialogFragment fragment = new InputDialogFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_TITLE, title);
        arguments.putString(ARGUMENT_VALUE, currentValue);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void setOnInputConfirmedListener(OnInputConfirmedListener onInputConfirmedListener){
        this.onInputConfirmedListener = onInputConfirmedListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        String title = args.getString(ARGUMENT_TITLE, null);
        currValue = args.getString(ARGUMENT_VALUE, null);

        View layout = getActivity().getLayoutInflater().inflate(R.layout.dialog_input, null);
        inputEdTxt = (EditText) layout.findViewById(R.id.inputEdTxt);

        if(!StringComponent.isNull(currValue)){
            inputEdTxt.setText(currValue);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(layout)
                .setPositiveButton(android.R.string.ok, this)
                .setNegativeButton(android.R.string.cancel, this);

        if(!StringComponent.isNull(title)){
            builder.setTitle(title);
        }

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which != Dialog.BUTTON_POSITIVE || onInputConfirmedListener == null) {
            return;
        }

        String newValue = inputEdTxt.getText().toString();
        if ("".equals(newValue) || newValue.equals(currValue)) {
            return;
        }else
            onInputConfirmedListener.onInputConfirmed(newValue);
    }

    public interface OnInputConfirmedListener {
        void onInputConfirmed(String newValue);
    }
}
