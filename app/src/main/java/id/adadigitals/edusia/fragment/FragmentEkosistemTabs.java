package id.adadigitals.edusia.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.adadigitals.edusia.R;

public class FragmentEkosistemTabs extends Fragment {
    public FragmentEkosistemTabs(){
    }

    public static FragmentEkosistemTabs newInstance() {
        FragmentEkosistemTabs fragment = new FragmentEkosistemTabs();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ekosistem, container, false);
        return root;
    }
}
