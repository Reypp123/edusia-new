package id.adadigitals.edusia.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.adadigitals.edusia.R;

public class FragmentDashboardTabs extends Fragment {

    ViewPager viewPager;
    TabLayout mTabsLayout;
    MainPagerAdapter mPagerAdapter;

    public int FRAGMENT_TIMELINE = 0;
    public int FRAGMENT_KALENDER = 1;

    public static FragmentDashboardTabs newInstance() {
        FragmentDashboardTabs fragment = new FragmentDashboardTabs();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        mTabsLayout = (TabLayout) root.findViewById(R.id.tabsLayout);
        mPagerAdapter = new MainPagerAdapter(getActivity(), getChildFragmentManager(), mTabsLayout);

        viewPager = (ViewPager) root.findViewById(R.id.container);
        viewPager.setAdapter(mPagerAdapter);
        mTabsLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabsLayout));
        mTabsLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        TabLayout.Tab tab = mTabsLayout.getTabAt(1);
        tab.select();
        return root;
    }

    public class MainPagerAdapter extends FragmentPagerAdapter{

        private int[] tabTitles = {
                R.string.label_timeline,
                R.string.label_calendar
        };

        Context context;
        TabLayout tabLayout;
        private SparseArray<Fragment> fragments = new SparseArray<Fragment>();

        public MainPagerAdapter(Context context, FragmentManager fragmentManager, TabLayout tabLayout) {
            super(fragmentManager);
            this.context = context;
            this.tabLayout = tabLayout;
        }

        @Override
        public Fragment getItem(int i) {
            if( i == FRAGMENT_TIMELINE ){
                return FragmentTimelineTabs.newInstance();
            }
            else if( i == FRAGMENT_KALENDER ){
                return FragmentKalenderTabs.newInstance();
            }
            else {
                return null;
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            fragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int pos) {
            return getString(tabTitles[pos]);
        }

        public Fragment getFragment(int position) {
            return fragments.get(position);
        }
    }
}
