package id.adadigitals.edusia.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.TextStyle;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.CalendarKehadiranAdapter;
import id.adadigitals.edusia.adapter.DaftarMuridAdapter;
import id.adadigitals.edusia.adapter.KehadiranAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.KehadiranItem;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.getListMuridResponse;
import id.adadigitals.edusia.view.CalendarEventDecorator;
import id.adadigitals.edusia.view.CalendarKehadiranDecorator;
import id.adadigitals.edusia.widget.LineItemDecoration;

public class FragmentKehadiranTabs extends Fragment{

    LinearLayout rootLayout, loadingLayout;
    RecyclerView muridList;
    KehadiranAdapter kehadiranAdapter;
    TextView resultTxt;

    public FragmentKehadiranTabs(){
    }

    public static FragmentKehadiranTabs newInstance() {
        FragmentKehadiranTabs fragment = new FragmentKehadiranTabs();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_kehadiran, container, false);
        rootLayout                              = (LinearLayout) root.findViewById(R.id.rootLayout);
        loadingLayout                           = (LinearLayout) root.findViewById(R.id.loadingLayout);
        muridList                               = (RecyclerView) root.findViewById(R.id.recyclerView);
        resultTxt                               = (TextView) root.findViewById(R.id.resultTxt);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(root.getContext());
        muridList.setLayoutManager(linearLayoutManager);
        muridList.addItemDecoration(new LineItemDecoration(root.getContext(),LinearLayout.VERTICAL));
        kehadiranAdapter                        = new KehadiranAdapter(getContext());
        muridList.setAdapter(kehadiranAdapter);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoadDataSiswa loadDataSiswa = new LoadDataSiswa();
        loadDataSiswa.execute();
    }

    public class LoadDataSiswa extends AsyncTask<Void, Void, List<Student>>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            muridList.setVisibility(View.GONE);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            ApiStudo apistudo               = new ApiStudo();
            getListMuridResponse listMurid  = apistudo.getDaftarMurid();

            if( listMurid != null && listMurid.rc == 1 ){
                if( listMurid.data.size() <= 0 ){
                    msgError = getString(R.string.text_data_student_notFound);
                }
                else {
                    List<Student> students          = listMurid.data;
                    return students;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            if( students != null && students.size() > 0 ){
                loadingLayout.setVisibility(View.GONE);
                muridList.setVisibility(View.VISIBLE);
                kehadiranAdapter.refreshAdapter(students);
            }
            else {
                if( !StringComponent.isNull(msgError) ){
                    Config.getInstance().addLogs("msg error get siswa timeline ==> " + msgError);
                }
                else{
                    loadingLayout.setVisibility(View.GONE);
                    muridList.setVisibility(View.GONE);
                    resultTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
