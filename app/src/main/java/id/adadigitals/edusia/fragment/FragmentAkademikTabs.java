package id.adadigitals.edusia.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.AkademikAdapter;
import id.adadigitals.edusia.adapter.KehadiranAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.getListMuridResponse;
import id.adadigitals.edusia.view.RadarMarkerView;
import id.adadigitals.edusia.widget.LineItemDecoration;

public class FragmentAkademikTabs extends Fragment {

    LinearLayout rootLayout, loadingLayout;
    RecyclerView muridList;
    AkademikAdapter akademikAdapter;
    TextView resultTxt;

    public FragmentAkademikTabs(){
    }

    public static FragmentAkademikTabs newInstance() {
        FragmentAkademikTabs fragment = new FragmentAkademikTabs();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_akademik, container, false);
        rootLayout                              = (LinearLayout) root.findViewById(R.id.rootLayout);
        loadingLayout                           = (LinearLayout) root.findViewById(R.id.loadingLayout);
        muridList                               = (RecyclerView) root.findViewById(R.id.recyclerView);
        resultTxt                               = (TextView) root.findViewById(R.id.resultTxt);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(root.getContext());
        muridList.setLayoutManager(linearLayoutManager);
        muridList.addItemDecoration(new LineItemDecoration(root.getContext(),LinearLayout.VERTICAL));
        akademikAdapter                        = new AkademikAdapter(getContext());
        muridList.setAdapter(akademikAdapter);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoadDataSiswa loadDataSiswa = new LoadDataSiswa();
        loadDataSiswa.execute();
    }

    public class LoadDataSiswa extends AsyncTask<Void, Void, List<Student>> {

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            muridList.setVisibility(View.GONE);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            ApiStudo apistudo               = new ApiStudo();
            getListMuridResponse listMurid  = apistudo.getDaftarMurid();

            if( listMurid != null && listMurid.rc == 1 ){
                if( listMurid.data.size() <= 0 ){
                    msgError = getString(R.string.text_data_student_notFound);
                }
                else {
                    List<Student> students          = listMurid.data;
                    return students;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            if( students != null && students.size() > 0 ){
                loadingLayout.setVisibility(View.GONE);
                muridList.setVisibility(View.VISIBLE);
                akademikAdapter.refreshAdapter(students);
            }
            else {
                if( !StringComponent.isNull(msgError) ){
                    Config.getInstance().addLogs("msg error get siswa timeline ==> " + msgError);
                }
                else{
                    loadingLayout.setVisibility(View.GONE);
                    muridList.setVisibility(View.GONE);
                    resultTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
