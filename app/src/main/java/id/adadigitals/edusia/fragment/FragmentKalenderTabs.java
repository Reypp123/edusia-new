package id.adadigitals.edusia.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.CalendarEventAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.CalendarEvent;
import id.adadigitals.edusia.view.CalendarEventDecorator;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.TextStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class FragmentKalenderTabs extends Fragment implements OnMonthChangedListener {
    RecyclerView calendarEventView;
    CalendarEventAdapter calendarEventAdapter;

    MaterialCalendarView calendarView;
    ProgressBar calendarLoadingBar;
    ProgressBar loadingBar;

    public int yearDisplay = 2018;
    public int monthDisplay = 1;
    CalendarEventTask calendarEventTask;

    HashMap<String, List<CalendarDay>> monthEvents;
    HashMap<String, List<CalendarEvent>> calendarEvents;

    public static FragmentKalenderTabs newInstance() {
        FragmentKalenderTabs fragment = new FragmentKalenderTabs();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        monthEvents = new HashMap<String, List<CalendarDay>>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_kalender, container, false);
        loadingBar = (ProgressBar) root.findViewById(R.id.loadingBar);
        calendarEventView = (RecyclerView) root.findViewById(R.id.calendarEventView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        calendarEventView.setLayoutManager(linearLayoutManager);
        View calendarHeader = LayoutInflater.from(calendarEventView.getContext())
                .inflate(R.layout.list_calendar_header, calendarEventView, false);
        calendarView = (MaterialCalendarView) calendarHeader.findViewById(R.id.calendarView);
        calendarLoadingBar = (ProgressBar) calendarHeader.findViewById(R.id.calendarLoadingBar);

        calendarView.setWeekDayFormatter(new WeekDayFormatter() {
            @Override
            public CharSequence format(DayOfWeek dayOfWeek) {
                return dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault());
            }
        });
        calendarView.setTitleFormatter(new TitleFormatter() {
            @Override
            public CharSequence format(CalendarDay calendarDay) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("LLLL yyyy", Locale.getDefault());
                return calendarDay.getDate().format(formatter);
            }
        });
        calendarEventAdapter = new CalendarEventAdapter(calendarHeader);
        calendarEventAdapter.setOnMonthChangedListener(this);
        calendarEventView.setAdapter(calendarEventAdapter);
        return root;
    }

    @Override
    public void onMonthChanged(MaterialCalendarView materialCalendarView, CalendarDay calendarDay) {
        if(calendarDay.getYear() != yearDisplay){
            calendarEventTask.cancel(true);
            calendarEventTask = null;
            yearDisplay = calendarDay.getYear();
            monthDisplay = calendarDay.getMonth();
            calendarEventTask = new CalendarEventTask();
            calendarEventTask.execute(yearDisplay);
        }else{
            monthDisplay = calendarDay.getMonth();
            String monthStr = (monthDisplay < 10)?"0"+monthDisplay:monthDisplay+"";
            refreshEvents(monthStr);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        yearDisplay = calendarView.getCurrentDate().getYear();
        monthDisplay = calendarView.getCurrentDate().getMonth();
        calendarEventTask = new CalendarEventTask();
        calendarEventTask.execute(yearDisplay);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void refreshEvents(String month){
        List<CalendarEvent> events = calendarEvents.get(month);
        calendarView.removeDecorators();
        if(events!=null && events.size() > 0){
            for(CalendarEvent calendarEvent: events) {
                if(monthEvents.get(calendarEvent.getColor()) == null)
                    monthEvents.put(calendarEvent.getColor(), new ArrayList<CalendarDay>());
                monthEvents.get(calendarEvent.getColor()).add(calendarEvent.date);
            }
            for(HashMap.Entry<String, List<CalendarDay>> entry : monthEvents.entrySet())
                calendarView.addDecorator(new CalendarEventDecorator(entry.getKey(), entry.getValue()));
        }
        calendarEventAdapter.refreshEvents(events);
        calendarEventView.invalidateItemDecorations();
    }

    public class CalendarEventTask extends AsyncTask<Integer, Void, HashMap<String, List<CalendarEvent>>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(calendarEvents != null){
                calendarLoadingBar.setVisibility(View.VISIBLE);
                calendarView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected HashMap<String, List<CalendarEvent>> doInBackground(Integer... integer) {
            int year = integer[0];
            HashMap<String, List<CalendarEvent>> sortedEvents = null;
            try{
                ApiStudo apiStudo = new ApiStudo();
                List<CalendarEvent> events = apiStudo.getAcademicCalendar(year);
                if(events!=null)
                    sortedEvents = new HashMap<String, List<CalendarEvent>>();
                for(CalendarEvent event:events){
                    if(sortedEvents.get(event.bulan) == null)
                        sortedEvents.put(event.bulan, new ArrayList<CalendarEvent>());
                    event.date = CalendarDay.from(year, Integer.parseInt(event.bulan), Integer.parseInt(event.tgl));
                    sortedEvents.get(event.bulan).add(event);
                }
                return sortedEvents;
            }catch (Exception e){
                Config.getInstance().addLogs("Error CalendarEventTask --> " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(HashMap<String, List<CalendarEvent>> sortedEvents) {
            super.onPostExecute(sortedEvents);
            if(calendarEvents!=null){
                calendarLoadingBar.setVisibility(View.GONE);
                calendarView.setVisibility(View.VISIBLE);
            }
            if(calendarEvents == null)
                calendarEvents = new HashMap<String, List<CalendarEvent>>();
            calendarEvents.clear();
            if(sortedEvents != null){
                calendarEvents.putAll(sortedEvents);
            }
            String monthStr = (monthDisplay < 10)?"0"+monthDisplay:monthDisplay+"";
            refreshEvents(monthStr);
            if(calendarEventView.getVisibility() == View.GONE){
                loadingBar.setVisibility(View.GONE);
                calendarEventView.setVisibility(View.VISIBLE);
            }
        }
    }
}
