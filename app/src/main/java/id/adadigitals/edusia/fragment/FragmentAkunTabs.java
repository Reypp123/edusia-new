package id.adadigitals.edusia.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.api.BankDKI;
import id.adadigitals.edusia.component.CipherComponent;
import id.adadigitals.edusia.component.ImageComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;
import id.adadigitals.edusia.data.wallet.request.AccountInformationRequest;
import id.adadigitals.edusia.data.wallet.response.AccountInformationResponse;
import id.adadigitals.edusia.dialog.InputDialogFragment;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;
import id.adadigitals.edusia.xui.PosInvoiceListActivity;
import id.adadigitals.edusia.xui.SavingHistoryActivity;
import id.adadigitals.edusia.xui.TopupInfoActivity;
import id.adadigitals.edusia.xui.TransactionHistoryActivity;

import java.util.List;

public class FragmentAkunTabs extends Fragment {

    final public String TAG_DIALOG_CHANGE_DAILYLIMIT = "DialogChangeDailyLimit";

    final public int DIALOG_CHANGE_DAILYLIMIT = 0;

    TextView balanceTxt;
    ImageView walletBtn;
    ImageView paymentBtn;
    ImageView historyBtn;

    LinearLayout loadingLayout;
    NestedScrollView dataLayout;
    LinearLayout studentDataLayout;

    ImageView guardianPhotoImg;
    TextView guardianNameTxt;
    TextView guardianPhoneTxt;


    WalletBankDKI mWalletDKI;
    List<Student> students;

    CardView mStudentView;

    AwesomeErrorDialog infoError;

    boolean isWaliIbu       = false;
    boolean isWaliFather    = false;

    public FragmentAkunTabs(){
    }

    public static FragmentAkunTabs newInstance() {
        FragmentAkunTabs fragment = new FragmentAkunTabs();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_akun, container, false);

        guardianPhotoImg    = (ImageView) root.findViewById(R.id.guardianPhotoImg);
        guardianNameTxt     = (TextView) root.findViewById(R.id.guardianNameTxt);
        guardianPhoneTxt    = (TextView) root.findViewById(R.id.guardianPhoneTxt);

        loadingLayout       = (LinearLayout) root.findViewById(R.id.loadingLayout);
        dataLayout          = (NestedScrollView) root.findViewById(R.id.dataLayout);
        studentDataLayout   = (LinearLayout) root.findViewById(R.id.studentDataLayout);
        balanceTxt          = (TextView) root.findViewById(R.id.balanceTxt);
        walletBtn           = (ImageView) root.findViewById(R.id.walletBtn);
        paymentBtn          = (ImageView) root.findViewById(R.id.paymentBtn);
        historyBtn          = (ImageView) root.findViewById(R.id.historyBtn);
        infoError           = new AwesomeErrorDialog(root.getContext());

        walletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent topupInfoActivity = TopupInfoActivity.createIntent(getActivity());
                startActivity(topupInfoActivity);
            }
        });

        paymentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Fitur ini akan segera hadir", Toast.LENGTH_SHORT).show();
            }
        });

        historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    CipherComponent ciu = new CipherComponent();
                    String secret = ciu.decrypt(Config.getInstance().getSessEncryptSecret(),students.get(0).getParentTemplateFile());
                    Intent intent = new Intent(getActivity(), TransactionHistoryActivity.class);
                    intent.putExtra(Config.KEY_PARAM_SECRET, secret);
                    intent.putExtra(Config.KEY_PARAM_USERNAME, mWalletDKI.getUsername());
                    intent.putExtra(Config.KEY_PARAM_ACCOUNT, mWalletDKI.getAccount_number());
                    startActivity(intent);
                }
                catch (Exception e){
                    Config.getInstance().addLogs("Error start activity Transaction history --> " + e.getMessage());
                }
            }
        });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LoadDataTask loadData = new LoadDataTask();
        loadData.execute();
    }

    private void refreshData(List<Student> students){
        Student student = students.get(0);
        isWaliIbu       = student.isWaliIbu(Config.getInstance().getSessionPhone());
        isWaliFather    = student.isWaliAyah(Config.getInstance().getSessionPhone());
        if( isWaliIbu ){
            if( !StringComponent.isNull(student.getIbuNama()) ){
                guardianNameTxt.setText(students.get(0).getIbuNama());
            }

            if( !StringComponent.isNull(student.getIbuNoHp()) ){
                guardianPhoneTxt.setText(student.getIbuNoHp());
            }

            if(!StringComponent.isNull(students.get(0).getIbuFoto())){
                ImageComponent.displayImageRound(getContext(), guardianPhotoImg, students.get(0).getIbuFotoUrl());
            }
        }
        else if( isWaliFather ){
            if( !StringComponent.isNull(student.getAyahNama()) ){
                guardianNameTxt.setText(StringComponent.upperCase(student.getAyahNama()));
            }

            if( !StringComponent.isNull(student.getAyahNoHp()) ){
                guardianPhoneTxt.setText(student.getAyahNoHp());
            }

            if(!StringComponent.isNull(students.get(0).getParentFaceTemplate())){
                ImageComponent.displayImageRound(getContext(), guardianPhotoImg, students.get(0).getParentTemplateUrl());
            }
        }

        if(mWalletDKI!=null) {
            balanceTxt.setText("Rp. " + StringComponent.formatThousands(mWalletDKI.balance));
            balanceTxt.setVisibility(View.VISIBLE);
        }else{
            balanceTxt.setVisibility(View.GONE);
        }

        if( student != null && students.size() > 0){
            for(int i=0; i<students.size(); i++){
                final Student studentis = students.get(i);
                final CardView studentCard = (CardView) createStudentView(studentDataLayout);
                studentCard.setTag(students.get(i));

                TextView titleText = (TextView) studentCard.findViewById(R.id.titleTxt);
                titleText.setText(students.get(i).getNama());

                final ImageButton toggleButton = (ImageButton) studentCard.findViewById(R.id.toggleBtn);
                toggleButton.setVisibility(View.GONE);

                if( !StringComponent.isNull(students.get(i).getStudentTemplateUrl()) ){
                    final ImageView imgMuridPhoto = (ImageView) studentCard.findViewById(R.id.muridPhotoImg);
                    ImageComponent.displayImageRound(getContext(), imgMuridPhoto, students.get(i).getStudentTemplateUrl());
                }

                EditText etMuridNis = (EditText) studentCard.findViewById(R.id.etMuridNis);
                etMuridNis.setText(students.get(i).getNIS());

                EditText etMuridSekolah   = (EditText) studentCard.findViewById(R.id.etMuridSekolah);
                etMuridSekolah.setText(students.get(i).getSekolah());

                EditText etMuridKelas   = (EditText) studentCard.findViewById(R.id.etMuridKelas);
                etMuridKelas.setText(students.get(i).getKelas());

                EditText etMuridTempatLahir   = (EditText) studentCard.findViewById(R.id.etMuridTempatLahir);
                etMuridTempatLahir.setText(students.get(i).getTempatLahir());

                EditText etTglLahir         = (EditText) studentCard.findViewById(R.id.etMuridTanggalLahir);
                etTglLahir.setText(students.get(i).getTanggalLahir());

                EditText studentAddressEdTxt = (EditText) studentCard.findViewById(R.id.studentAddressEdTxt);
                studentAddressEdTxt.setText(students.get(i).getAlamat());

                EditText studentPhoneEdTxt = (EditText) studentCard.findViewById(R.id.studentPhoneEdTxt);
                studentPhoneEdTxt.setText(students.get(i).getTlp());

                EditText studentMobileEdTxt = (EditText) studentCard.findViewById(R.id.studentMobileEdTxt);
                studentMobileEdTxt.setText(students.get(i).getHP());

                LinearLayout financeLayout = (LinearLayout) studentCard.findViewById(R.id.financeLayout);
                financeLayout.setVisibility(View.VISIBLE);

                EditText dailyLimitEdTxt = (EditText) studentCard.findViewById(R.id.dailyLimitEdTxt);
                if(!StringComponent.isNull(students.get(i).getDailyLimit())){
                    dailyLimitEdTxt.setText("Rp. "+StringComponent.formatThousands(students.get(i).getDailyLimit()));
                }
                else{
                    dailyLimitEdTxt.setText(" - ");
                }

                EditText savingsEdTxt = (EditText) studentCard.findViewById(R.id.savingsEdTxt);
                if(!StringComponent.isNull(students.get(i).getSaldoTabungan())){
                    savingsEdTxt.setText("Rp. "+StringComponent.formatThousands(students.get(i).getSaldoTabungan()));
                }
                else{
                    savingsEdTxt.setText("Rp. 0");
                }

                ImageView savingsHistoryBtn = (ImageView) studentCard.findViewById(R.id.savingsHistoryBtn);
                savingsHistoryBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showSavingsHistory(studentis.getNIS());
                    }
                });

                ImageView transactionHistoryBtn = (ImageView) studentCard.findViewById(R.id.transactionHistoryBtn);
                transactionHistoryBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPosInvoiceList(studentis.getNIS());
                    }
                });

                ImageView changeLimitBtn = (ImageView) studentCard.findViewById(R.id.changeLimitBtn);
                changeLimitBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mStudentView = studentCard;
                        InputDialogFragment changeDailyLimit = InputDialogFragment.newInstance(
                                getString(R.string.title_change_daily_limit),studentis.getDailyLimit());
                        changeDailyLimit.setOnInputConfirmedListener(new InputDialogFragment.OnInputConfirmedListener() {
                            @Override
                            public void onInputConfirmed(String newValue) {
                                SaveDailyLimitAsyncTask saveDailyLimitAsynctask = new SaveDailyLimitAsyncTask();
                                saveDailyLimitAsynctask.execute(studentis.getNIS(), newValue);
                            }
                        });
                        changeDailyLimit.show(getActivity().getFragmentManager(),
                                "CHANGE_DAILY_LIMIT");
                    }
                });

                studentDataLayout.addView(studentCard);

            }
        }

        this.students = students;
    }

    public class SaveDailyLimitAsyncTask extends AsyncTask<String, Void, BasicResponse>{

        String dailyLimit = null;
        String mLastErrorMsg = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            dataLayout.setVisibility(View.GONE);
        }

        @Override
        protected BasicResponse doInBackground(String... strings) {
            String nis = strings[0];
            String dailyLimit = strings[1];

            ApiStudo apiStudo = new ApiStudo();
            BasicResponse response = apiStudo.updateDailyLimit(nis, dailyLimit);
            if(response!=null && response.rc == 1){
                this.dailyLimit = dailyLimit;
                Student student = (Student) mStudentView.getTag();
                student.setDailyLimit(dailyLimit);
            }else if(response == null){
                mLastErrorMsg = getString(R.string.error_network_connection);
            }else if(response.rc != 1){
                mLastErrorMsg = response.msg;
            }
            return response;
        }

        @Override
        protected void onPostExecute(BasicResponse response) {
            super.onPostExecute(response);
            if(response!=null && response.rc == 1){
                ((EditText)mStudentView.findViewById(R.id.dailyLimitEdTxt)).setText("Rp. "+dailyLimit);
            }else{
                if(StringComponent.isNull(mLastErrorMsg)){
                    mLastErrorMsg = getString(R.string.error_gkjelas);
                }

                infoError
                        .setTitle(getString(R.string.title_change_daily_limit))
                        .setMessage(mLastErrorMsg)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {

                            }
                        })
                        .show();
            }
            loadingLayout.setVisibility(View.GONE);
            dataLayout.setVisibility(View.VISIBLE);
        }
    }

    public void showSavingsHistory(String nis){
        Intent intent = new Intent(getActivity(), SavingHistoryActivity.class);
        intent.putExtra(Config.KEY_PARAM_NIS, nis);
        startActivity(intent);
    }

    public void showPosInvoiceList(String nis){
        Intent intent = new Intent(getActivity(), PosInvoiceListActivity.class);
        intent.putExtra(Config.KEY_PARAM_NIS, nis);
        startActivity(intent);
    }

    private View createStudentView(ViewGroup rootView) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View studentItem = inflater.inflate(R.layout.expandable_card_student, rootView, false);
        return studentItem;
    }

    public class LoadDataTask extends AsyncTask<Void,Void, List<Student>> {

        ApiStudo apistudo = new ApiStudo();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            dataLayout.setVisibility(View.GONE);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            getListMuridResponse listMurid  = apistudo.getDaftarMurid();
            List<Student> students          = listMurid.data;

            if( listMurid != null && listMurid.rc == 1 ){
                BankDKI bankDKIApi = new BankDKI();
                AccountInformationRequest accountInformationRequest = new AccountInformationRequest();
                accountInformationRequest.setMsisdn(Config.getInstance().getSessionPhone());
                AccountInformationResponse accountInformationResponse = bankDKIApi.accInformation(accountInformationRequest);
                if( accountInformationResponse != null && accountInformationResponse.getWalletInfo() != null){
                    WalletBankDKI walletBankDKI = accountInformationResponse.getWalletInfo();
                    mWalletDKI = walletBankDKI;
                }
            }

            return students;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            if( students != null && students.size() > 0){
                refreshData(students);
            }

            loadingLayout.setVisibility(View.GONE);
            dataLayout.setVisibility(View.VISIBLE);
        }
    }
}
