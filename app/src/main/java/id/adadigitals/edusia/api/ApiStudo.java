package id.adadigitals.edusia.api;

import android.content.ContentValues;
import android.util.Base64;

import id.adadigitals.edusia.BuildConfig;
import id.adadigitals.edusia.R;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.CalendarEvent;
import id.adadigitals.edusia.data.KehadiranItem;
import id.adadigitals.edusia.data.NilaiDetail;
import id.adadigitals.edusia.data.NilaiSementara;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.CalendarEventResponse;
import id.adadigitals.edusia.response.json.GetDetailMuridResponse;
import id.adadigitals.edusia.response.json.GetDetailNilaiResponse;
import id.adadigitals.edusia.response.json.GetLaporanTabunganResponse;
import id.adadigitals.edusia.response.json.GetListTahunAkademikResponse;
import id.adadigitals.edusia.response.json.GetNilaiSementaraResponse;
import id.adadigitals.edusia.response.json.GetPosInvoiceResponse;
import id.adadigitals.edusia.response.json.GetTimeLineResponse;
import id.adadigitals.edusia.response.json.KehadiranResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;
import id.adadigitals.edusia.response.json.getUsernameResponse;
import id.adadigitals.edusia.response.json.getUserResponse;

public class ApiStudo{
    private static final String __LOG__           = "__APISTUDO__";
    public static final String URL_API            = Config.getInstance().checkUrlApiStatus() + "API/Multi";
    public static final String URL_OTP            = Config.URL_API_PRODUCTION + "API";
    public static final String TYPE_GET           = "GET";
    public static final String TYPE_POST          = "POST";
    public static final String TYPE_PUT           = "PUT";
    public static final String HEAD_USER_AGENT    = BuildConfig.FLAVOR+"/"+ BuildConfig.VERSION_NAME;
    public static final String HEAD_ACCEPT        = "application/json";

    private String username           = null;
    private String checksum           = null;
    private String secret             = null;
    private String BasicAuth          = null;
    String MsgError                   = null;
    boolean ConnFail                  = false;
    private Gson mGson;

    public ApiStudo(){
        this.username   = Config.getInstance().getSessionUsername();
        this.mGson      = new Gson();
        this.checksum   = StringComponent.hash(Config.TOKEN_SECRET);
        this.secret     = Config.getInstance().getSessionPasswordAuth();

        if( !StringComponent.isNull(username) && !StringComponent.isNull(secret) ){
            this.BasicAuth = Base64.encodeToString((username+":"+secret).getBytes(), Base64.DEFAULT);
        }
    }

    public getUsernameResponse getUsername(String phone, String nik){
        try{
            ContentValues contenValues = new ContentValues();
            contenValues.put("nohp",phone);

            if( !StringComponent.isNull(nik)){
                contenValues.put("nik", nik);
            }

            contenValues.put("checksum", checksum);

            String kirim = postRequest(URL_API + "/Login/get_username", getQuery(contenValues));
            getUsernameResponse responseUsername = mGson.fromJson(kirim, getUsernameResponse.class);
            return responseUsername;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error get username");
            return null;
        }
    }

    public getListMuridResponse getDaftarMurid(){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            contentValues.put("yayasan",Config.ID_YAYASAN+"");
            String kirim = postRequest( URL_API + "/Auth/Murid/getlistmurid", getQuery(contentValues));
            getListMuridResponse response = mGson.fromJson(kirim, getListMuridResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error get List Murid --> " + e.getMessage());
            return null;
        }
    }

    public GetListTahunAkademikResponse getTahunAkademik(){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            contentValues.put("yayasan",Config.ID_YAYASAN+"");
            String kirim = postRequest( URL_API + "/Auth/Murid/list_tahun_ajaran", getQuery(contentValues));
            GetListTahunAkademikResponse response = mGson.fromJson(kirim, GetListTahunAkademikResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error get getTahunAkademik --> " + e.getMessage());
            return null;
        }
    }

    public List<NilaiSementara> getNilaiSementara(String nis, String semester, String ajaran){
        Config.getInstance().addLogs("nis ==> " + nis);
        Config.getInstance().addLogs("semester ==> " + semester);
        Config.getInstance().addLogs("ajaran ==> " + ajaran);
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            contentValues.put("yayasan",Config.ID_YAYASAN+"");
            contentValues.put("nis",nis);
            contentValues.put("semester",semester);
            contentValues.put("ajaran",ajaran);
            String kirim = postRequest( URL_API + "/Auth/Penilaian/nilai_akhir", getQuery(contentValues));
            GetNilaiSementaraResponse response = mGson.fromJson(kirim, GetNilaiSementaraResponse.class);
            if( response != null && response.rc == 1 ){
                if( response.data != null ){
                    return response.data;
                }
                else {
                    return new ArrayList<NilaiSementara>();
                }
            }
        }
        catch (Exception e){
            Config.getInstance().addLogs("error get nilai sementara --> " + e.getMessage());
        }

        return null;
    }

    public List<NilaiDetail> getNilaiDetail(String nis, String semester, String ajaran, String idMatpel){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            contentValues.put("yayasan",Config.ID_YAYASAN+"");
            contentValues.put("nis",nis);
            contentValues.put("id_matpel",idMatpel);
            contentValues.put("semester",semester);
            contentValues.put("ajaran",ajaran);
            String kirim = postRequest( URL_API + "/Auth/Penilaian/nilai_smt_lalu", getQuery(contentValues));
            GetDetailNilaiResponse response = mGson.fromJson(kirim, GetDetailNilaiResponse.class);
            if( response != null && response.rc == 1 ){
                if( response.data != null ){
                    return response.data;
                }
                else {
                    return new ArrayList<NilaiDetail>();
                }
            }
        }
        catch (Exception e){
            Config.getInstance().addLogs("error get nilai detail --> " + e.getMessage());
        }

        return null;
    }

    public GetDetailMuridResponse getDetailMurid(String nis){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            contentValues.put("yayasan",Config.ID_YAYASAN+"");
            contentValues.put("nis",nis);
            String kirim = postRequest( URL_API + "/Auth/Murid/getdetailmurid", getQuery(contentValues));
            GetDetailMuridResponse response = mGson.fromJson(kirim, GetDetailMuridResponse.class);
            return response;
        }
        catch (Exception e){
            e.printStackTrace();
            Config.getInstance().addLogs("error get detail murid --> " + e.getMessage());
            return null;
        }
    }

    public GetTimeLineResponse getTimeLine(String nis, int start, int limit){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("start", start+"");
            contentValues.put("limit", limit+"");
            String kirim = postRequest(URL_API + "/Auth/Murid/get_timeline", getQuery(contentValues));
            GetTimeLineResponse response = mGson.fromJson(kirim, GetTimeLineResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error getTimeLine --> " + e.getMessage());
            return null;
        }
    }

    public GetLaporanTabunganResponse getLaporanTabungan(String nis, int start, int limit){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("start", start+"");
            contentValues.put("limit", limit+"");
            String result = postRequest(URL_API + "/Murid/getlaporantabungan", getQuery(contentValues));
            GetLaporanTabunganResponse response = mGson.fromJson(result, GetLaporanTabunganResponse.class);
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("error getLaporanTabungan --> " + e.getMessage());
            return null;
        }
    }

    public getUserResponse getUser(){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum",checksum);
            String kirim = postRequest(URL_API + "/Auth/Login/getuser", getQuery(contentValues));
            getUserResponse response = mGson.fromJson(kirim, getUserResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error get user --> " + e.getMessage());
            return null;
        }
    }

    public BasicResponse updateParentWallet(String parentWallet){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("parentwallet", parentWallet);
            String result = postRequest(URL_API+"/Auth/Login/update_parent_wallet", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("Error update parent wallet --> " + e.getMessage());
            return null;
        }
    }

    public List<CalendarEvent> getAcademicCalendar(int tahun){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("tahun", tahun+"");
            String result = postRequest(URL_API+"/Auth/Kalender/index", getQuery(contentValues));
            CalendarEventResponse calendarEventsList = mGson.fromJson(result, CalendarEventResponse.class);
            if(calendarEventsList!=null && calendarEventsList.rc == 1){
                if(calendarEventsList.data!=null)
                    return calendarEventsList.data;
                else
                    return new ArrayList<CalendarEvent>();
            }
        }catch(Exception e){
            Config.getInstance().addLogs("Error getAcademicCalendar --> " + e.getMessage());
        }

        return null;
    }

    public List<KehadiranItem> getKehadiran(String nis, int bulan, int start, int limit){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("bulan", bulan+"");
            contentValues.put("start", start+"");
            contentValues.put("limit", limit+"");
            String result   = postRequest( URL_API + "/Auth/Absensi/list_absensi", getQuery(contentValues));
            KehadiranResponse kehadiranResponse = mGson.fromJson(result, KehadiranResponse.class);
            if( kehadiranResponse != null && kehadiranResponse.rc == 1 ){
                if( kehadiranResponse.data != null ){
                    return kehadiranResponse.data;
                }
                else {
                    return new ArrayList<KehadiranItem>();
                }
            }
        }
        catch (Exception e){
            Config.getInstance().addLogs( "Error getKehadiran --> " + e.getMessage() );
        }

        return null;
    }

    public BasicResponse updateStudentWallet(String nis, String studentWallet){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("studentwallet", studentWallet);
            String result = postRequest(URL_API+"/Auth/Login/update_student_wallet", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("Error update student wallet --> " + e.getMessage());
            return null;
        }
    }

    public BasicResponse updateFCMbyOrtu(String nis, String nik, String token, String parent){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("nik", nik);
            contentValues.put("token", token);
            contentValues.put("parent", parent);
            String result           = postRequest(URL_API + "/Auth/Murid/update_token_parent", getQuery(contentValues));
            BasicResponse response  = mGson.fromJson(result, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("error updateFCMbyOrtu == " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    public BasicResponse updateStudent(Student student) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", student.getNIS());

            if (!StringComponent.isNull(student.getRiwayat())) {
                contentValues.put("riwayatkes", student.getRiwayat());
            }

            if (!StringComponent.isNull(student.getHP())) {
                contentValues.put("hp", student.getHP());
            }

            if (!StringComponent.isNull(student.getTlp())) {
                contentValues.put("tlp", student.getTlp());
            }

            if (!StringComponent.isNull(student.getAyahPekerjaan())) {
                contentValues.put("pekerjaanayah", student.getAyahPekerjaan());
            }

            if (!StringComponent.isNull(student.getAyahPenghasilan())){
                contentValues.put("penghasilanayah", student.getAyahPenghasilan());
            }

            if (!StringComponent.isNull(student.getAyahNoTelp()))
                contentValues.put("telponayah", student.getAyahNoTelp());

            if (!StringComponent.isNull(student.getAyahNoTelpPerusahaan()))
                contentValues.put("telponkantorayah", student.getAyahNoTelpPerusahaan());

            if (!StringComponent.isNull(student.getAyahAlamat()))
                contentValues.put("alamatayah", student.getAyahAlamat());

            if (!StringComponent.isNull(student.getAyahEmail()))
                contentValues.put("emailayah", student.getAyahEmail());

            if (!StringComponent.isNull(student.getAyahTmptLahir()))
                contentValues.put("tempatlahirayah", student.getAyahTmptLahir());

            if (!StringComponent.isNull(student.getAyahTglLahir()))
                contentValues.put("tanggallahirayah", student.getAyahTglLahir());

            if (!StringComponent.isNull(student.getIbuPekerjaan()))
                contentValues.put("pekerjaanibu", student.getIbuPekerjaan());

            if (!StringComponent.isNull(student.getIbuPenghasilan()))
                contentValues.put("penghasilanibu", student.getIbuPenghasilan());

            if (!StringComponent.isNull(student.getIbuNoTelp()))
                contentValues.put("telponibu", student.getIbuNoTelp());

            if (!StringComponent.isNull(student.getIbuNoTelpPerusahaan()))
                contentValues.put("telponkantoribu", student.getIbuNoTelpPerusahaan());

            if (!StringComponent.isNull(student.getIbuAlamat()))
                contentValues.put("alamatibu", student.getIbuAlamat());

            if (!StringComponent.isNull(student.getIbuEmail()))
                contentValues.put("emailibu", student.getIbuEmail());

            if (!StringComponent.isNull(student.getIbuTmptLahir()))
                contentValues.put("tempatlahiribu", student.getIbuTmptLahir());

            /*if (!StringComponent.isNull(student.getAlamat())) {
                contentValues.put("alamat", student.getAlamat());
            }

            if (!StringComponent.isNull(student.getAyahPerusahaan()))
                contentValues.put("perusahaanayah", student.getAyahPerusahaan());

            if (!StringComponent.isNull(student.getIbuPerusahaan()))
                contentValues.put("perusahaanibu", student.getIbuPerusahaan());


            if (!StringComponent.isNull(student.getIbuTglLahir()))
                contentValues.put("tanggallahiribu", student.getIbuTglLahir());*/

            String result = postRequest(URL_API + "/Auth/Murid/updatemurid", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;

        } catch (Exception e) {
            return null;
        }
    }

    public BasicResponse updateFotoIbu(String nik, File file){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nik", nik);

            HashMap<String,File> fileValues = new HashMap<>();
            fileValues.put("file", file);

            String kirim = postMultipartRequest(URL_API + "/Auth/Murid/update_foto_ibu", contentValues, fileValues );
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            return null;
        }
    }

    public BasicResponse updateFotoMurid(String nis, File file){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);

            HashMap<String,File> fileValues = new HashMap<>();
            fileValues.put("file", file);

            String kirim = postMultipartRequest(URL_API + "/Auth/Murid/update_foto_siswa", contentValues, fileValues );
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            return null;
        }
    }

    public BasicResponse updateOrtuTemplateFace(String nik, File file){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nik", nik);

            HashMap<String,File> fileValues = new HashMap<>();
            fileValues.put("file", file);
            String kirim = postMultipartRequest(URL_API + "/Auth/Login/update_parent_face_template", contentValues, fileValues );
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            return null;
        }
    }

    public BasicResponse updateMuridTemplateFace(String nis, File file){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);

            HashMap<String,File> fileValues = new HashMap<>();
            fileValues.put("file", file);

            String kirim = postMultipartRequest(URL_API + "/Auth/Login/update_student_face_template", contentValues, fileValues );
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            return null;
        }
    }

    public BasicResponse kirimOtp(String noHp){
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);

            if( !StringComponent.isNull(noHp) ){
                contentValues.put("no_hp", noHp);
            }

            String result = postRequest(URL_OTP + "/murid/kirim_sms", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("error kirim otp --> " + e.getMessage());
            return null;
        }
    }

    public BasicResponse checkOTP(String noHp, String kodeOTP){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);

            if( !StringComponent.isNull(noHp) ){
                contentValues.put("no_hp", noHp);
            }

            if( !StringComponent.isNull(kodeOTP) ){
                contentValues.put("otp", kodeOTP);
            }

            String result = postRequest(URL_OTP + "/murid/cek_otp", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("error check otp --> " + e.getMessage());
            return null;
        }
    }

    public BasicResponse updateFotoBapak(String nik, File file){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nik",nik);

            HashMap<String,File> fileValues = new HashMap<>();
            fileValues.put("file", file);

            String kirim = postMultipartRequest(URL_API + "/Auth/Murid/update_foto_ayah", contentValues, fileValues );
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            return response;
        }
        catch (Exception e){
            return null;
        }
    }

    public BasicResponse updateDailyLimit(String nis, String dailyLimit){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("nis", nis);
            contentValues.put("dailylimit", dailyLimit);
            String result = postRequest(URL_API + "/Murid/updatedailylimit", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(result, BasicResponse.class);
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("error check otp --> " + e.getMessage());
            return null;
        }
    }

    public GetPosInvoiceResponse getPosInvoice(String nis, int start, int limit){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("yayasan", Config.ID_YAYASAN+"");
            contentValues.put("nis", nis);
            contentValues.put("start", start+"");
            contentValues.put("limit", limit+"");
            String result = postRequest(URL_API+"/Auth/Pos/gettransaksimurid", getQuery(contentValues));
            GetPosInvoiceResponse response = mGson.fromJson(result, GetPosInvoiceResponse.class);
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("error getPosInvoice --> " + e.getMessage());
            return null;
        }
    }

    public BasicResponse updatePassword(String nik, String nohp, String password){
        try{
            ContentValues contentValues = new ContentValues();
            contentValues.put("checksum", checksum);
            contentValues.put("nik", nik);
            contentValues.put("nohp", nohp);
            contentValues.put("password", password);
            String kirim = postRequest( URL_API + "/Login/update_password", getQuery(contentValues));
            BasicResponse response = mGson.fromJson(kirim, BasicResponse.class);
            if( response != null && response.rc == 1){
                this.secret = password;
                this.BasicAuth = Base64.encodeToString((username+":"+secret).getBytes(), Base64.DEFAULT);
                Config.getInstance().setSessionLogin(username, password);
            }

            return response;
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error get List Murid");
            return null;
        }
    }

    public String getMsgError(){
        return MsgError;
    }

    public Boolean getConnFail(){
        return ConnFail;
    }

    public File downloadFileNya(String url, File fileTemplate){
        String kirim = getFile(url, fileTemplate);
        if( !StringComponent.isNull(kirim) ){
            return fileTemplate;
        }
        else {
            return null;
        }
    }

    public String getFile(String url, File templateFile){
        return httpRequest(TYPE_GET, url, null, null, true, templateFile);
    }

    public String postRequest(String url, String payload){
        return httpRequest(TYPE_POST, url, null, payload, true, null);
    }

    public String postRequest(String url, ContentValues contentValues, String payload){
        return httpRequest(TYPE_POST, url, contentValues, payload, true, null);
    }

    public String getRequest(String url, ContentValues contentValues){
        return httpRequest(TYPE_GET, url, contentValues, null, true , null);
    }

    public String getQuery(ContentValues contentValues) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator iterator = contentValues.valueSet().iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry) iterator.next();
            String key = me.getKey().toString();
            String value =  (String) me.getValue();
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value, "UTF-8"));
        }

        return result.toString();
    }

    public String httpRequest(String type, final String url, ContentValues contentValues, String payload, boolean https, File outputFile){
        try{
            String response = "", responseMsg = null;
            int responseCode = -1;
            String cUrl = (contentValues!=null && contentValues.size()>0)?url+"?&"+getQuery(contentValues):url;
            URL uri = new URL(cUrl);

            if(https) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uri.openConnection();
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        HostnameVerifier hv =
                                HttpsURLConnection.getDefaultHostnameVerifier();
                        return hv.verify(StringComponent.parseDomainName(url), session);

                    }
                });
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setRequestMethod(type);

                if (!StringComponent.isNull(BasicAuth))
                    Config.getInstance().addLogs("username  --> " + username);
                Config.getInstance().addLogs("secret --> " + secret);
                Config.getInstance().addLogs("basic auth --> " + BasicAuth);
                httpsURLConnection.setRequestProperty("Authorization", "Basic " + BasicAuth);
                httpsURLConnection.setRequestProperty("Accept", HEAD_ACCEPT);
                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setRequestProperty("User-Agent", HEAD_USER_AGENT);

                httpsURLConnection.setInstanceFollowRedirects(false);
                httpsURLConnection.setConnectTimeout(20 * 1000);
                httpsURLConnection.setReadTimeout(30 * 1000);
                httpsURLConnection.setUseCaches(false);
                if (!StringComponent.isNull(payload)) {
                    OutputStream os = httpsURLConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(payload);
                    writer.flush();
                    writer.close();
                    os.close();
                }

                InputStream is = null;
                try {
                    responseCode = httpsURLConnection.getResponseCode();
                    responseMsg =  httpsURLConnection.getResponseMessage();
                    is = httpsURLConnection.getInputStream();
                } catch(IOException e) {
                    is = httpsURLConnection.getErrorStream();
                }

                if (is != null) {
                    if(outputFile!=null){
                        if (outputFile.exists())
                            outputFile.delete();
                        RandomAccessFile raf = new RandomAccessFile(outputFile, "rw");
                        long fileSize = httpsURLConnection.getContentLength();
                        byte buf[] = new byte[4096];
                        int rd = -1;
                        long tot = 0;
                        while ((rd = is.read(buf)) != -1) {
                            raf.write(buf, 0, rd);
                            tot += rd;
                        }
                        raf.close();
                        if(fileSize == tot){
                            response = outputFile.getAbsolutePath();
                        }else{
                            if (outputFile.exists())
                                outputFile.delete();
                        }
                    }else {
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line = "";
                        while ((line = br.readLine()) != null)
                            response += line;
                        br.close();
                    }
                    is.close();
                }else{
                    Config.getInstance().addLogs("ERROR CONNECTION === "+type+" FAILED: "+url);
                    MsgError = Config.getInstance().getString(R.string.error_network_connection);
                    ConnFail = true;
                }
                httpsURLConnection.disconnect();
            }else{
                HttpURLConnection httpURLConnection = (HttpURLConnection) uri.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod(type);

                if (!StringComponent.isNull(BasicAuth))
                    httpURLConnection.setRequestProperty("Authorization", "Basic " + BasicAuth);
                httpURLConnection.setRequestProperty("Accept", HEAD_ACCEPT);
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setRequestProperty("User-Agent", HEAD_USER_AGENT);

                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setConnectTimeout(10 * 1000);
                httpURLConnection.setReadTimeout(20 * 1000);
                httpURLConnection.setUseCaches(false);
                if (!StringComponent.isNull(payload)) {
                    OutputStream os = httpURLConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(payload);
                    writer.flush();
                    writer.close();
                    os.close();
                }
                InputStream is = null;
                try {
                    responseCode = httpURLConnection.getResponseCode();
                    responseMsg =  httpURLConnection.getResponseMessage();
                    is = httpURLConnection.getInputStream();
                } catch(IOException e) {
                    is = httpURLConnection.getErrorStream();
                }
                if (is != null) {
                    if(outputFile!=null){
                        if (outputFile.exists())
                            outputFile.delete();
                        RandomAccessFile raf = new RandomAccessFile(outputFile, "rw");
                        long fileSize = httpURLConnection.getContentLength();
                        byte buf[] = new byte[4096];
                        int rd = -1;
                        long tot = 0;
                        while ((rd = is.read(buf)) != -1) {
                            raf.write(buf, 0, rd);
                            tot += rd;
                        }
                        raf.close();
                        if(fileSize == tot){
                            response = outputFile.getAbsolutePath();
                        }else{
                            if (outputFile.exists())
                                outputFile.delete();
                        }
                    }else {
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line = "";
                        while ((line = br.readLine()) != null)
                            response += line;
                        br.close();
                    }
                    is.close();
                }else{
                    Config.getInstance().addLogs("ERROR CONNECTION === "+type+" FAILED: "+url);
                    MsgError = Config.getInstance().getString(R.string.error_network_connection);
                    ConnFail = true;
                }
                httpURLConnection.disconnect();
            }

            if(StringComponent.isNull(response)){
                if(responseCode == HttpsURLConnection.HTTP_UNAUTHORIZED){
                    MsgError = Config.getInstance().getString(R.string.error_credential);
                }else{
                    MsgError = "FaultError "+responseCode;
                }
            }

            Config.getInstance().addLogs("=== "+type+" REQUEST: "+cUrl);
            if(!StringComponent.isNull(payload))
                Config.getInstance().addLogs("=== "+type+" PAYLOAD: "+payload);
            Config.getInstance().addLogs("=== SERVER RESPONSE: "+responseCode+":"+responseMsg+" --> "+response);
            return response;
        }catch(IOException e){
            Config.getInstance().addLogs("=== "+type+" FAILED: "+url);
            Config.getInstance().addLogs("=== FAILED PAYLOAD: "+payload);
            e.printStackTrace();
            Config.getInstance().addLogs("ERROR === " + e.getMessage() +  " === "+type+ " FAILED: " + url);
        }
        return null;
    }

    public String postMultipartRequest(String cUrl, ContentValues formValues, HashMap<String,File> fileValues){
        try {

            LinkedHashMap<String,String> headers = new LinkedHashMap<>();
            if (!StringComponent.isNull(BasicAuth))
                headers.put("Authorization", "Basic " + BasicAuth);
            headers.put("User-Agent", HEAD_USER_AGENT);
            headers.put("Accept", HEAD_ACCEPT);

            MultipartUtility multipart = new MultipartUtility(cUrl, headers,"UTF-8");

            Iterator iterator = formValues.valueSet().iterator();
            while(iterator.hasNext()) {
                Map.Entry me = (Map.Entry) iterator.next();
                String key = me.getKey().toString();
                String value =  (String) me.getValue();
                multipart.addFormField(key, value);
            }

            for(Map.Entry<String,File> item:fileValues.entrySet()){
                multipart.addFilePart(item.getKey(), item.getValue());
            }

            MultiPartResponse response = multipart.finish();

            if(response.getResponseCode() == HttpsURLConnection.HTTP_UNAUTHORIZED){
                MsgError = Config.getInstance().getString(R.string.error_credential);
            }else if(!StringComponent.isNull(response.getResponseMessage())){
                MsgError = response.getResponseMessage();
            }else{
                MsgError = "FaultError " + response.getResponseCode();
            }

            Config.getInstance().addLogs("=== POST REQUEST: "+cUrl);
            Config.getInstance().addLogs("=== SERVER RESPONSE: "+response.getResponseCode()+" --> "+response.getResponse());
            return response.getResponse();
        } catch (IOException ex) {
            ex.printStackTrace();
            Config.getInstance().addLogs("=== REQUEST: URL "+cUrl);
            Config.getInstance().addLogs("Error api ==> " + ex.getMessage());
        }

        return null;
    }
}
