package id.adadigitals.edusia.api;

import android.content.ContentValues;
import android.util.Base64;

import id.adadigitals.edusia.BuildConfig;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.request.AccountConfirmVerificationPinRequest;
import id.adadigitals.edusia.data.wallet.request.AccountInformationRequest;
import id.adadigitals.edusia.data.wallet.request.AccountVerificationPinRequest;
import id.adadigitals.edusia.data.wallet.request.CheckAccountRequest;
import id.adadigitals.edusia.data.wallet.request.ListTransactionRequest;
import id.adadigitals.edusia.data.wallet.request.RegisterAccountRequest;
import id.adadigitals.edusia.data.wallet.request.UbahPinRequest;
import id.adadigitals.edusia.data.wallet.response.AccountConfirmVerificationPinResponse;
import id.adadigitals.edusia.data.wallet.response.AccountInformationResponse;
import id.adadigitals.edusia.data.wallet.response.AccountVerificationPinResponse;
import id.adadigitals.edusia.data.wallet.response.CheckAccountResponse;
import id.adadigitals.edusia.data.wallet.response.ListTransactionResponse;
import id.adadigitals.edusia.data.wallet.response.RegisterAccountResponse;
import id.adadigitals.edusia.data.wallet.response.UbahPinResponse;
import id.adadigitals.edusia.response.json.GetAccessTokenResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class BankDKI {
    public static final String HEAD_USER_AGENT    = BuildConfig.FLAVOR+"/"+ BuildConfig.VERSION_NAME;

    final String URL_API                    = (Config.API_MANAGEMENT == true) ? Config.URL_API_DEV_BANKDKI : "https://portal.bankdki.co.id:8443/mobile-webconsole/apps/pocket/pbCustom/";
    final String PATH_REG                   = (Config.API_MANAGEMENT == true) ? "register" : "extRegisterNonCustomerJomi";
    final String PATH_CHECK                 = (Config.API_MANAGEMENT == true) ? "akun" : "extGetListPrimaryAccounts";
    final String HISTORY_PATH               = (Config.API_MANAGEMENT == true) ? "transaksi" : "extTransactionHistory";
    final String INFO_PATH                  = (Config.API_MANAGEMENT == true) ? "infochannel" : "extAccountInformationOtherChannel";
    final String FORGOT_PATH_VERIFY         = (Config.API_MANAGEMENT == true) ? "lpin" : "extForgotPasswordExternal";
    final String FORGOT_PATH_VERIFY_CONFIRM = (Config.API_MANAGEMENT == true) ? "verifikasi" : "extForgotPasswordExternalConfirm";
    final String EXECUTE_NEW_PIN            = (Config.API_MANAGEMENT == true) ? (Config.STATUS_DEVEL == "DEVEL") ? "konfirm" : "edit" : "extExecuteNewPasswordExternal";
    //    final String TRANSACTION_PATH = (Config.API_MANAGEMENT == true) ? "ochannel" : "extTransactionOtherChannel";

    final String TYPE_GET          = "GET";
    final String TYPE_PUT          = "PUT";
    final String TYPE_POST         = "POST";
    final String HEAD_ACCEPT       = "application/json";
    final String CONTENT_TYPE      = "application/json";

    String MsgError                = null;
    boolean ConnFail               = false;
    private Gson mGson;
    private String msgError        = null;
    private String access_token    = null;
    private String BasicAuth       = null;
    private String client_key      = (Config.STATUS_DEVEL == "DEVEL") ? "7wdh2h56pm3pdh2md3c4qauv" : "3prtjsqep9cjar96pbab5ebe";
    private String client_secret   = (Config.STATUS_DEVEL == "DEVEL") ? "5nqE2Rd9Nx" : "QJGP6VbSyQ";


    public BankDKI(){
        this.mGson = new GsonBuilder().disableHtmlEscaping().create();
        if( !StringComponent.isNull(client_key) && !StringComponent.isNull(client_secret) ){
            this.BasicAuth = Base64.encodeToString((client_key+":"+client_secret).getBytes(), Base64.DEFAULT);
        }

        reqGetAccessToken();
    }

    public GetAccessTokenResponse reqGetAccessToken(){
        try{
            this.mGson                  = new Gson();
            ContentValues contentValues = new ContentValues();
            contentValues.put("grant_type","client_credentials");
            String kirim                 = postRequestHttp( Config.URL_REFRESH_TOKEN_BANKDKI , getQuery(contentValues));
            GetAccessTokenResponse getAccessTokenResponse = mGson.fromJson(kirim, GetAccessTokenResponse.class);
            if( getAccessTokenResponse != null  ){
                this.access_token = getAccessTokenResponse.access_token;
            }

            return getAccessTokenResponse;
        }
        catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : reqGetAccessToken ==> "+ e.getMessage());
            return null;
        }
    }

    public UbahPinResponse accExecNewPin(UbahPinRequest ubahPinRequest){
        String request = mGson.toJson(ubahPinRequest);
        try{
            String response = postRequest(URL_API + EXECUTE_NEW_PIN, request);
            UbahPinResponse ubahPinResponse = mGson.fromJson(response, UbahPinResponse.class);
            return ubahPinResponse;
        }
        catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : accExecNewPin ==> "+ e.getMessage());
            return null;
        }
    }

    public AccountConfirmVerificationPinResponse accVerificationConfirmPin(AccountConfirmVerificationPinRequest accountConfirmVerificationPinRequest){
        String request = mGson.toJson(accountConfirmVerificationPinRequest);
        try{
            String response = postRequest(URL_API + FORGOT_PATH_VERIFY_CONFIRM, request);
            AccountConfirmVerificationPinResponse accountConfirmVerificationPinResponse = mGson.fromJson(response, AccountConfirmVerificationPinResponse.class);
            return accountConfirmVerificationPinResponse;
        }
        catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : accVerificationConfirmPin ==> "+ e.getMessage());
            return null;
        }
    }

    public AccountVerificationPinResponse accVerificationLupaPin( AccountVerificationPinRequest accountVerificationPinRequest ){
        String request = mGson.toJson(accountVerificationPinRequest);
        try{
            String response = postRequest(URL_API + FORGOT_PATH_VERIFY, request);
            AccountVerificationPinResponse accountVerificationPinResponse = mGson.fromJson(response, AccountVerificationPinResponse.class);
            return accountVerificationPinResponse;
        }
        catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : accVerificationLupaPin ==> "+ e.getMessage());
            return null;
        }
    }

    public AccountInformationResponse accInformation(AccountInformationRequest accountInformationRequest){
        String request = mGson.toJson(accountInformationRequest);
        try {
            String response = postRequest(URL_API + INFO_PATH, request);
            AccountInformationResponse accountInformationResponse = mGson.fromJson(response, AccountInformationResponse.class);
            return accountInformationResponse;
        }catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : accInformation ==> "+ e.getMessage());
            return null;
        }
    }

    public ListTransactionResponse accHistory(ListTransactionRequest listTransactionRequest){
        String request = mGson.toJson(listTransactionRequest);
        try {
            String response = postRequest(URL_API + HISTORY_PATH, request);
            ListTransactionResponse listTransactionResponse = mGson.fromJson(response, ListTransactionResponse.class);
            return listTransactionResponse;
        }catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API L : accHistory ==> "+ e.getMessage());
            return null;
        }
    }

    public RegisterAccountResponse accRegister(RegisterAccountRequest registerAccountRequest){
        String request = mGson.toJson(registerAccountRequest);
        try {
            String response = postRequest(URL_API + PATH_REG, request);
            RegisterAccountResponse registerAccountResponse = mGson.fromJson(response, RegisterAccountResponse.class);
            return registerAccountResponse;
        }catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API accRegister ==> "+ e.getMessage());
            return null;
        }
    }

    public CheckAccountResponse accCheck(CheckAccountRequest checkAccountRequest){
        Config.getInstance().addLogs(" =============================== DKI ACCOUNT USING "+checkAccountRequest.body.username+" : "+checkAccountRequest.body.password);
        String request = mGson.toJson(checkAccountRequest);
        try {
            String response = postRequest(URL_API + PATH_CHECK, request);
            CheckAccountResponse checkAccountResponse = mGson.fromJson(response, CheckAccountResponse.class);
            return checkAccountResponse;
        }catch (Exception e){
            Config.getInstance().addLogs("ERROR BANK DKI API accCheck ==> "+ e.getMessage());
            return null;
        }
    }

    public String getMsgError(){
        return MsgError;
    }

    public Boolean getConnFail(){
        return ConnFail;
    }

    public String getRequest(String url, ContentValues contentValues){
        return httpsRequest(TYPE_GET, url, contentValues, null, false );
    }

    public String postRequest(String url, String payload){

        boolean isHttps = true;
        if( Config.API_MANAGEMENT ){
            isHttps = false;
        }

        return httpsRequest(TYPE_POST, url, null, payload, isHttps);
    }

    public String postRequestHttp(String url, String payload){
        return httpsRequest(TYPE_POST, url, null, payload, false);
    }

    public String putRequest(String url, String payload){
        return httpsRequest(TYPE_PUT, url, null, payload, true);
    }

    public String getQuery(ContentValues contentValues) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator iterator = contentValues.valueSet().iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry) iterator.next();
            String key = me.getKey().toString();
            String value =  (String) me.getValue();
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value, "UTF-8"));
        }
        return result.toString();
    }

    public String httpsRequest(String type, final String url, ContentValues contentValues, String payload, boolean https){
        try{
            String response = "";
            int responseCode = -1;
            String cUrl = (contentValues!=null && contentValues.size()>0)?url+"?&"+getQuery(contentValues):url;
            URL uri = new URL(cUrl);

            if(https) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uri.openConnection();
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        HostnameVerifier hv =
                                HttpsURLConnection.getDefaultHostnameVerifier();
                        return hv.verify(StringComponent.parseDomainName(url), session);

                    }
                });
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setRequestMethod(type);


                httpsURLConnection.setRequestProperty("Accept", HEAD_ACCEPT);
                httpsURLConnection.setRequestProperty("Content-Type", CONTENT_TYPE);

                httpsURLConnection.setInstanceFollowRedirects(false);
                httpsURLConnection.setConnectTimeout(10 * 1000);
                httpsURLConnection.setReadTimeout(20 * 1000);
                httpsURLConnection.setUseCaches(false);
                if (!StringComponent.isNull(payload)) {
                    OutputStream os = httpsURLConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(payload);
                    writer.flush();
                    writer.close();
                    os.close();
                }

                InputStream is = null;
                try {
                    responseCode = httpsURLConnection.getResponseCode();
                    is = httpsURLConnection.getInputStream();
                } catch(IOException e) {
                    is = httpsURLConnection.getErrorStream();
                }

                if (is != null) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String line = "";
                    while ((line = br.readLine()) != null)
                        response += line;
                    br.close();
                    is.close();
                }else{
                    Config.getInstance().addLogs("=== "+type+" FAILED: "+url);
                    msgError = Config.getInstance().getString(R.string.error_network_connection);
                    ConnFail = true;
                }
                httpsURLConnection.disconnect();
            }
            else {
                HttpURLConnection httpURLConnection = (HttpURLConnection) uri.openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod(type);

                if( !StringComponent.isNull(BasicAuth) ){
                    if( !StringComponent.isNull(access_token) ){
                        Config.getInstance().addLogs("Token --> " + access_token);
                        httpURLConnection.setRequestProperty("Authorization", "Bearer " + access_token);
                        httpURLConnection.setRequestProperty("Accept", HEAD_ACCEPT);
                        httpURLConnection.setRequestProperty("Content-Type", CONTENT_TYPE);
                    }
                    else {
                        Config.getInstance().addLogs("Ada Basic Auth --> " + BasicAuth);
                        httpURLConnection.setRequestProperty("Authorization", "Basic " + BasicAuth);
                        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    }
                }

                httpURLConnection.setRequestProperty("User-Agent", HEAD_USER_AGENT);

                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setConnectTimeout(10 * 1000);
                httpURLConnection.setReadTimeout(20 * 1000);
                httpURLConnection.setUseCaches(false);

                if (!StringComponent.isNull(payload)) {
                    OutputStream os = httpURLConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(payload);
                    writer.flush();
                    writer.close();
                    os.close();
                }

                InputStream is = null;
                try {
                    responseCode = httpURLConnection.getResponseCode();
                    is = httpURLConnection.getInputStream();
                } catch(IOException e) {
                    is = httpURLConnection.getErrorStream();
                }
                if (is != null) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String line = "";
                    while ((line = br.readLine()) != null)
                        response += line;
                    br.close();
                    is.close();
                }else{
                    Config.getInstance().addLogs("=== "+type+" FAILED: "+url);
                    MsgError = Config.getInstance().getString(R.string.error_network_connection);
                    ConnFail = true;
                }
                httpURLConnection.disconnect();
            }

            if(StringComponent.isNull(response)){
                if((responseCode == HttpsURLConnection.HTTP_OK ||
                        responseCode == HttpsURLConnection.HTTP_CREATED))
                    response = "{\"result\":\"1\"}";
                else if(responseCode == HttpsURLConnection.HTTP_UNAUTHORIZED){
                    msgError = Config.getInstance().getString(R.string.error_credential);
                }else{
                    msgError = "FaultError "+responseCode;
                }
            }

            Config.getInstance().addLogs("=== "+type+" REQUEST: "+cUrl);
            if(!StringComponent.isNull(payload))
                Config.getInstance().addLogs("=== "+type+" PAYLOAD: "+payload);
            Config.getInstance().addLogs("=== SERVER RESPONSE: "+responseCode+" --> "+response.toString());
            return response;
        }catch(Exception e){
            Config.getInstance().addLogs("=== "+type+" FAILED: "+url);
            if(!StringComponent.isNull(payload))
                Config.getInstance().addLogs("=== "+type+" PAYLOAD: "+payload);
            Config.getInstance().addLogs("Error BANK DKI API LINE 168 --> " + e.getMessage());
        }
        return null;
    }
}
