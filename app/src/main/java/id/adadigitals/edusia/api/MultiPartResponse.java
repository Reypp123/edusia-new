package id.adadigitals.edusia.api;

import java.util.List;

public class MultiPartResponse {
    int responseCode = -1;
    String responseMessage = null;
    List<String> responses = null;

    public MultiPartResponse(int responseCode, String responseMessage, List<String> responses){
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.responses = responses;
    }

    public int getResponseCode(){
        return responseCode;
    }

    public String getResponseMessage(){
        return responseMessage;
    }

    public List<String> getResponseArray(){
        return responses;
    }

    public String getResponse(){
        if(responses != null && responses.size() > 0){
            String response = "";
            for(String line : responses){
                response = response + line;
            }
            return response;
        }
        return null;
    }
}
