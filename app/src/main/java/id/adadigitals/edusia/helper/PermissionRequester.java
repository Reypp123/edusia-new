package id.adadigitals.edusia.helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import id.adadigitals.edusia.config.Config;

import id.adadigitals.edusia.config.Config;

public class PermissionRequester {
    public static boolean requestReadPhoneStatePermissionIfNeeded(AppCompatActivity appCompatActivity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.READ_PHONE_STATE, appCompatActivity, requestCode);
    }

    public static boolean requestSendSMSIfNeeded(AppCompatActivity appCompatActivity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.SEND_SMS, appCompatActivity, requestCode);
    }

    public static boolean requestReceiveSMSIfNeeded(AppCompatActivity appCompatActivity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.RECEIVE_SMS, appCompatActivity, requestCode);
    }

    public static boolean requestCameraPermissionIfNeeded(AppCompatActivity appCompatActivity, int requestCode) {
        return checkAndRequestPermission(Manifest.permission.CAMERA, appCompatActivity, requestCode);
    }

    private static boolean checkAndRequestPermission(String permission, AppCompatActivity appCompatActivity, int requestCode) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (checkPermission(permission)) {
            return true;
        } else {
            appCompatActivity.requestPermissions(new String[]{permission}, requestCode);
        }
        return false;
    }

    private static boolean checkPermission(String permission) {
        final int permissionCheck = ContextCompat.checkSelfPermission(Config.getInstance(), permission);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isPermissionGranted(int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }
}
