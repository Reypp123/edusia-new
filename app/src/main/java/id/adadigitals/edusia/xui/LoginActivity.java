package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;

import java.io.File;
import java.util.List;

import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.response.json.getUserResponse;

public class LoginActivity extends AppCompatActivity {

    public static final int MODE_SETUP = 0;
    public static final int MODE_VERIFIKASI = 1;
    public static final String PARAMS_LOGIN_MODE = "LoginParamsMode";

    private int loginMode = MODE_SETUP;

    private Toolbar toolbar;
    private RelativeLayout login_layout;
    private TextView tvInstruction;
    private EditText passwordEdTxt, confirmEdTxt;
    private Button btnLogin;
    private AwesomeErrorDialog infoError;
    private AwesomeProgressDialog infoProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_password_setup);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public void declare(){
        login_layout  = (RelativeLayout) findViewById(R.id.loginLayout);
        tvInstruction = (TextView) findViewById(R.id.tvInstruction);
        passwordEdTxt = (EditText) findViewById(R.id.passwordEdTxt);
        confirmEdTxt  = (EditText) findViewById(R.id.confirmEdTxt);
        btnLogin      = (Button) findViewById(R.id.btnContinue);
        infoError     = new AwesomeErrorDialog(this);
        infoProgress  = new AwesomeProgressDialog(this);
    }

    public void run(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loginMode = getIntent().getIntExtra(PARAMS_LOGIN_MODE, MODE_SETUP);

        if( loginMode == MODE_SETUP ){
            toolbar.setTitle(R.string.title_password_configuration);
        }
        else {
            toolbar.setTitle(R.string.title_password_verification);
        }

        if( loginMode == MODE_SETUP ){
            tvInstruction.setText(R.string.desc_password_setup);
            confirmEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        login();
                        return true;
                    }
                    return false;
                }
            });
        }
        else {
            tvInstruction.setText(R.string.desc_password_verification);
            confirmEdTxt.setVisibility(View.GONE);
            passwordEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        login();
                        return true;
                    }
                    return false;
                }
            });
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    public void login(){
        passwordEdTxt.setError(null);
        confirmEdTxt.setError(null);

        String password = passwordEdTxt.getText().toString();
        String confirm  = confirmEdTxt.getText().toString();
        boolean cancel  = false;
        View focusView  = null;

        if(StringComponent.isNull(password)){
            passwordEdTxt.setError(getString(R.string.error_required_password));
            focusView = passwordEdTxt;
            cancel    = true;
        }
        else if( StringComponent.isNull(confirm) && loginMode == MODE_SETUP ){
            confirmEdTxt.setError(getString(R.string.error_required_confirm_password));
            focusView  = confirmEdTxt;
            cancel     = true;
        }

        if( loginMode == MODE_SETUP ){
            if( !StringComponent.isNull(password) && !isPasswordValid(password) ){
                passwordEdTxt.setError(getString(R.string.error_sandi_singkat));
                focusView   = passwordEdTxt;
                cancel      = true;
            }
            else if( !StringComponent.isNull(password) && !StringComponent.isValidPassword(password) ){
                passwordEdTxt.setError(getString(R.string.error_sandi_nonalfanumerik));
                focusView   = passwordEdTxt;
                cancel      = true;
            }
            else if( !StringComponent.isNull(confirm) && !password.equals(confirm) ){
                confirmEdTxt.setError(getString(R.string.error_sandi_tidaksama));
                focusView   = confirmEdTxt;
                cancel      = true;
            }
        }

        if( cancel ){
            focusView.requestFocus();
        }
        else {
            String username = Config.getInstance().getSessionUsername();
            String mobile   = Config.getInstance().getSessionPhone();
            String nik      = Config.getInstance().getSessionNIK();

            Config.getInstance().setSessionLogin(username, password);

            UserLoginTask userloginTask = new UserLoginTask();
            userloginTask.execute(username, password, mobile, nik);
        }
    }

    private boolean isPasswordValid(String password){
        return password.length() >= 8;
    }

    public class UserLoginTask extends AsyncTask<String, Void, List<Student>>{

        String username;
        String password;
        String nohp;
        String nik;

        String msgError     = null;
        Boolean ConnFail    = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            String message = getString(R.string.text_configuration_process);
            if( loginMode == MODE_VERIFIKASI ){
                message = getString(R.string.text_verification_process);
            }

            infoProgress.setTitle(getString(R.string.title_menunggu))
                    .setMessage(message)
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();

            passwordEdTxt.setEnabled(false);
            confirmEdTxt.setEnabled(false);
        }

        @Override
        protected List<Student> doInBackground(String... strings) {
            username    = strings[0];
            password    = strings[1];
            nohp        = strings[2];
            nik         = strings[3];

            boolean success     = false;
            ApiStudo apistudo = new ApiStudo();

            if( loginMode == MODE_SETUP ){
                BasicResponse bResponse = apistudo.updatePassword(nik,nohp,password);
                if( bResponse != null && bResponse.rc == 1 ){
                    success = true;
                }
                else if( bResponse != null ){
                    msgError = bResponse.msg;
                }
                else {
                    ConnFail = apistudo.getConnFail();
                    msgError = apistudo.getMsgError();
                }
            }

            if( loginMode == MODE_VERIFIKASI ){
                getUserResponse response = apistudo.getUser();
                if( response != null && response.rc == 1 ){
                    success = true;
                }
                else if( response != null && response.rc == 0 ){
                    msgError = getString(R.string.error_login_userNotFound);
                }
                else {
                    ConnFail = apistudo.getConnFail();
                    msgError = apistudo.getMsgError();
                }
            }

            if( (loginMode == MODE_SETUP && success) || (loginMode == MODE_VERIFIKASI && success) ){
                getListMuridResponse listMurid = apistudo.getDaftarMurid();
                if( listMurid != null ){
                    if( listMurid.rc == 1 ){
                        if( listMurid.data.size() <= 0 ){
                            msgError = getString(R.string.text_data_student_notFound);
                        }
                        else {
                            List<Student> students  = listMurid.data;
                            boolean isWaliMother    = students.get(0).isWaliIbu(nohp);
                            boolean isWaliFather    = students.get(0).isWaliAyah(nohp);

                            if( isWaliMother && students.get(0).isParentTemplateExists(students.get(0).getIbuNIK()) )
                            {
                                File resultFile = apistudo.downloadFileNya(students.get(0).getParentTemplateUrl(), students.get(0).getParentTemplateFile());
                                if( resultFile == null || !resultFile.exists() ){
                                    ConnFail = apistudo.getConnFail();
                                    msgError = apistudo.getMsgError();
                                    return null;
                                }
                            }

                            if( isWaliFather && students.get(0).isParentTemplateExists(students.get(0).getAyahNIK()) ){
                                File resultFile = apistudo.downloadFileNya(students.get(0).getParentTemplateUrl(), students.get(0).getParentTemplateFile());
                                if( resultFile == null || !resultFile.exists() ){
                                    ConnFail = apistudo.getConnFail();
                                    msgError = apistudo.getMsgError();
                                    return null;
                                }
                            }

                            for (Student student:students){
                                try {
                                    if( student.isStudentTemplateExists()){
                                        File resultFile = apistudo.downloadFileNya(student.getStudentTemplateUrl(), student.getStudentTemplateFile());
                                        if( resultFile == null || !resultFile.exists() ){
                                            ConnFail = apistudo.getConnFail();
                                            msgError = apistudo.getMsgError();
                                            return null;
                                        }
                                    }

                                    if( loginMode == MODE_VERIFIKASI ){
                                        if(  !StringComponent.isNull(student.getNIS()) ){
                                            if( isWaliFather && !StringComponent.isNull(Config.getInstance().getFcmToken()) ){
                                                BasicResponse response = apistudo.updateFCMbyOrtu(student.getNIS(), student.getAyahNIK(), Config.getInstance().getFcmToken(), "ayah");
                                                if( response == null || response.rc != 1 ){
                                                    ConnFail = apistudo.getConnFail();
                                                    msgError = apistudo.getMsgError();
                                                    return null;
                                                }
                                            }
                                            else if( isWaliMother && !StringComponent.isNull(Config.getInstance().getFcmToken())){
                                                BasicResponse response = apistudo.updateFCMbyOrtu(student.getNIS(), student.getIbuNIK(), Config.getInstance().getFcmToken(), "ibu");
                                                if( response == null || response.rc != 1 ){
                                                    ConnFail = apistudo.getConnFail();
                                                    msgError = apistudo.getMsgError();
                                                    return null;
                                                }
                                            }
                                        }
                                    }

                                    Thread.sleep(500);
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            return listMurid.data;
                        }
                    }
                    else {
                        msgError = listMurid.msg;
                    }
                }
                else {
                    ConnFail = apistudo.getConnFail();
                    msgError = apistudo.getMsgError();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);

            if( infoProgress != null ){
                infoProgress.hide();
            }

            passwordEdTxt.setEnabled(true);
            confirmEdTxt.setEnabled(true);

            if( StringComponent.isNull(msgError) ){
                msgError = getString(R.string.error_gkjelas);
            }

            if( students != null && students.size() > 0){
                Config.getInstance().setSessionStatusVerified(true);
                if( loginMode == MODE_VERIFIKASI ){
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_SETUP_WALLET);
                    showSetupWallet();
                }
                else {
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFIKASI_DATA_MURID);
                    showValidationDataStudent();
                }

                finish();
            }
            else {
                Config.getInstance().addLogs("Conn Fail --> " + ConnFail);
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(msgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {
                                if( ConnFail ){
                                    reset();
                                }
                            }
                        })
                        .show();
            }
        }
    }

    private void showValidationDataStudent(){
        Intent validationData = ValidationDataStudent.createIntent(getApplicationContext());
        startActivity(validationData);
    }

    private void showSetupWallet(){
        Intent setupWallet = SetupWallet.createIntent(getApplicationContext());
        setupWallet.putExtra(SetupWallet.PARAMS_WALLET_MODE, SetupWallet.MODE_NEW_PIN);
        startActivity(setupWallet);
    }

    public void reset(){
        Config.getInstance().resetUserVerifikasi();
        Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFICATION);
        Intent verificationActivity = UserVerificationActivity.createIntent(getApplicationContext());
        startActivity(verificationActivity);
        finish();
    }
}
