package id.adadigitals.edusia.xui;

import android.app.DatePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.TransactionHistoryAdapter;
import id.adadigitals.edusia.api.BankDKI;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.request.ListTransactionRequest;
import id.adadigitals.edusia.data.wallet.response.ListTransactionResponse;
import id.adadigitals.edusia.widget.LineItemDecoration;

import java.util.Calendar;

import id.adadigitals.edusia.api.BankDKI;

public class TransactionHistoryActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    TextView resultTxt;
    EditText dateFromEdTxt;
    EditText dateToEdTxt;

    AppCompatButton getHistoryBtn;

    RelativeLayout rootLayout;
    RelativeLayout progressLayout;
    RecyclerView historyList;

    int whichDateField = 0;

    String mUsername = "";
    String mPin = "";
    String mAkun = "";

    String mDateFrom = "";
    String mDateTo = "";

    TransactionHistoryAdapter listTransactionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_transaction_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            mUsername = getIntent().getStringExtra(Config.KEY_PARAM_USERNAME);
            mAkun = getIntent().getStringExtra(Config.KEY_PARAM_ACCOUNT);
            mPin = getIntent().getStringExtra(Config.KEY_PARAM_SECRET);
        }else{
            finish();
        }

        setupUserInterface();
    }

    public void setupUserInterface(){
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        historyList = (RecyclerView) findViewById(R.id.historyList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        historyList.setLayoutManager(linearLayoutManager);
        historyList.addItemDecoration(new LineItemDecoration(this, LinearLayout.VERTICAL));
        listTransactionAdapter = new TransactionHistoryAdapter();
        historyList.setAdapter(listTransactionAdapter);
        getHistoryBtn = (AppCompatButton) findViewById(R.id.getHistoryBtn);
        getHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFields()) {
                    ListTransactionTask listTransactionTask = new ListTransactionTask();
                    listTransactionTask.execute(mUsername, mPin, mAkun, mDateFrom, mDateTo);
                }
            }
        });

        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);

        resultTxt = (TextView) findViewById(R.id.resultTxt);
        dateFromEdTxt = (EditText) findViewById(R.id.dateFromEdTxt);
        dateFromEdTxt.setFocusable(false);
        dateFromEdTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whichDateField = 0;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        TransactionHistoryActivity.this, TransactionHistoryActivity.this, year, month, day);
                datePickerDialog.show();
            }
        });
        dateToEdTxt = (EditText) findViewById(R.id.dateToEdTxt);
        dateToEdTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                whichDateField = 1;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        TransactionHistoryActivity.this, TransactionHistoryActivity.this, year, month, day);
                datePickerDialog.show();
            }
        });
        dateToEdTxt.setFocusable(false);
    }

    public boolean checkFields(){
        if(StringComponent.isNull(dateFromEdTxt.getText().toString())){
            dateFromEdTxt.setError(getString(R.string.error_required_dateFrom));
            return false;
        }else if(StringComponent.isNull(dateToEdTxt.getText().toString())){
            dateToEdTxt.setError(getString(R.string.error_required_dateLast));
            return false;
        }else
            return true;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String day = (dayOfMonth < 10)?"0"+dayOfMonth:dayOfMonth+"";
        month++;
        String monthCtr = (month < 10)?"0"+month:month+"";
        if(whichDateField == 0){
            dateFromEdTxt.setError(null);
            mDateFrom = year+""+monthCtr+""+day;
            dateFromEdTxt.setText(day+"/"+monthCtr+"/"+year);
        }else if(whichDateField == 1){
            dateToEdTxt.setError(null);
            mDateTo = year+""+monthCtr+""+day;
            dateToEdTxt.setText(day+"/"+monthCtr+"/"+year);
        }
    }

    public class ListTransactionTask extends AsyncTask<String, Void, ListTransactionResponse> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            resultTxt.setVisibility(View.GONE);
            dateFromEdTxt.setEnabled(false);
            dateToEdTxt.setEnabled(false);
            getHistoryBtn.setEnabled(false);
            historyList.setVisibility(View.GONE);
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected ListTransactionResponse doInBackground(String... strings) {
            BankDKI dkiApi = new BankDKI();
            String username = strings[0];
            String password = strings[1];
            String account = strings[2];
            String dateFrom = strings[3];
            String dateTo = strings[4];
            ListTransactionRequest listTransactionRequest = new ListTransactionRequest();
            listTransactionRequest.setUsername(username);
            listTransactionRequest.setAccount(account);
            listTransactionRequest.setDateFrom(dateFrom);
            listTransactionRequest.setDateTo(dateTo);
            listTransactionRequest.setPassword(password);
            return dkiApi.accHistory(listTransactionRequest);
        }

        @Override
        protected void onPostExecute(ListTransactionResponse listTransactionResponse) {
            super.onPostExecute(listTransactionResponse);
            progressLayout.setVisibility(View.GONE);
            dateFromEdTxt.setEnabled(true);
            dateToEdTxt.setEnabled(true);
            getHistoryBtn.setEnabled(true);
            if(listTransactionResponse !=null){
                if(listTransactionResponse.responseStatus()==1){
                    if(listTransactionResponse.isNotEmpty()){
                        historyList.setVisibility(View.VISIBLE);
                        resultTxt.setVisibility(View.GONE);
                        listTransactionAdapter.refreshAdapter(listTransactionResponse.getTrxHistory());
                    }else{
                        historyList.setVisibility(View.GONE);
                        resultTxt.setVisibility(View.VISIBLE);
                        resultTxt.setText(getString(R.string.desc_transaction_history_empty));
                    }

                }else if(listTransactionResponse.responseStatus()==0) {
                    historyList.setVisibility(View.GONE);
                    resultTxt.setVisibility(View.VISIBLE);
                    resultTxt.setText(listTransactionResponse.getError().getFaultMessage());
                }
            }
            else{
                String errorMsg = Config.getInstance().getString(R.string.error_network_connection);
                historyList.setVisibility(View.GONE);
                resultTxt.setVisibility(View.VISIBLE);
                resultTxt.setText(errorMsg);
            }
        }
    }
}
