package id.adadigitals.edusia.xui;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;

import java.util.Arrays;
import java.util.Calendar;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.BankDKI;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;
import id.adadigitals.edusia.data.wallet.WalletPinBankDKI;
import id.adadigitals.edusia.data.wallet.request.AccountConfirmVerificationPinRequest;
import id.adadigitals.edusia.data.wallet.request.AccountInformationRequest;
import id.adadigitals.edusia.data.wallet.request.AccountVerificationPinRequest;
import id.adadigitals.edusia.data.wallet.response.AccountConfirmVerificationPinResponse;
import id.adadigitals.edusia.data.wallet.response.AccountVerificationPinResponse;
import id.adadigitals.edusia.data.wallet.response.FaultError;
import id.adadigitals.edusia.view.PinEntryEditText;

public class LupaPinActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final String PARAMS_ID_REFERENCE = "ReferenceParamsMode";

    RelativeLayout rootLayout;
    NestedScrollView dataLayout;
    EditText etTanggalLahirIdentitas;
    EditText etEmailIdentitas;
    Button btnReqOtp;
    AwesomeProgressDialog infoProcess;
    AwesomeErrorDialog infoError;
    WalletPinBankDKI mWalletPin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_informasi_identitas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        declare();
        run();
    }

    public static Intent CreateIntent(Context context){
        return new Intent(context, LupaPinActivity.class);
    }

    private void declare(){
        rootLayout  = (RelativeLayout) findViewById(R.id.rootLayout);
        dataLayout  = (NestedScrollView) findViewById(R.id.dataLayout);
        etTanggalLahirIdentitas = (EditText) findViewById(R.id.etTanggalLahirIdentitas);
        etEmailIdentitas = (EditText) findViewById(R.id.etEmailIdentitas);
        btnReqOtp       = (Button) findViewById(R.id.btnReqOtp);
        infoProcess     = new AwesomeProgressDialog(this);
        infoError       = new AwesomeErrorDialog(this);
    }

    private void run(){
        Calendar c = Calendar.getInstance();
        final int year  = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day   = c.get(Calendar.DAY_OF_MONTH);

        etTanggalLahirIdentitas.setFocusable(false);
        etTanggalLahirIdentitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(LupaPinActivity.this,LupaPinActivity.this, year, month, day);
                datePickerDialog.show();
            }
        });

        btnReqOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runVerifikasiAkun();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String day = (dayOfMonth < 10)?"0"+dayOfMonth:dayOfMonth+"";
        month++;
        String monthCtr = (month < 10)?"0"+month:month+"";
        etTanggalLahirIdentitas.setText(year + "-" + monthCtr + "-" + day);
    }

    private void runVerifikasiAkun(){
        boolean InputError = false;
        View focusView     = null;

        String etTanggalLahir = etTanggalLahirIdentitas.getText().toString();
        String etEmail        = etEmailIdentitas.getText().toString();

        if( StringComponent.isNull(etTanggalLahir) ){
            etTanggalLahirIdentitas.setError(getString(R.string.error_required_tgl_lahir_wali));
            InputError = true;
        }

        if( StringComponent.isNull(etEmail) ){
            etEmailIdentitas.setError(getString(R.string.error_required_email_wali));
            InputError = true;
            focusView  = etEmailIdentitas;
        }

        if( InputError ){
            focusView.requestFocus();
        }
        else {
            VerifikasiAkunLupaPinTask verifiAkunLupaPin = new VerifikasiAkunLupaPinTask();
            verifiAkunLupaPin.execute(etTanggalLahir, etEmail);
        }
    }

    public void showOtpNewPin(){
        Intent otpVerificationPin = OtpVerificationNewPin.createIntent(getApplicationContext());
        if( !StringComponent.isNull(mWalletPin.getReference()) ){
            otpVerificationPin.putExtra(LupaPinActivity.PARAMS_ID_REFERENCE, mWalletPin.getReference());
        }
        startActivity(otpVerificationPin);
        finish();
    }

    public class VerifikasiAkunLupaPinTask extends AsyncTask<String, Void, WalletPinBankDKI>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(getString(R.string.text_section_verification))
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
        }

        @Override
        protected WalletPinBankDKI doInBackground(String... strings) {
            String tglLahir = strings[0];
            String email    = strings[1];

            BankDKI bankDKI    = new BankDKI();
            AccountVerificationPinRequest accountVerificationPinRequest = new AccountVerificationPinRequest();
            accountVerificationPinRequest.setDateOfBirth(tglLahir);
            accountVerificationPinRequest.setEmail(email);
            AccountVerificationPinResponse accountVerificationPinResponse = bankDKI.accVerificationLupaPin(accountVerificationPinRequest);
            if( accountVerificationPinResponse == null ){
                msgError = bankDKI.getMsgError();
                if( StringComponent.isNull(msgError) ){
                    msgError = getString(R.string.error_gkjelas);
                }
            }
            else if( accountVerificationPinResponse != null && accountVerificationPinResponse.responseStatus() == 0 ){
                if (accountVerificationPinResponse.getError().getCode() == -1) {
                    msgError = accountVerificationPinResponse.getError().getFaultMessage();
                } else {
                    msgError = accountVerificationPinResponse.getError().getMessage();
                }
            }
            else if (accountVerificationPinResponse != null && accountVerificationPinResponse.responseStatus() == 1) {
                WalletPinBankDKI walletPinBankDKI = new WalletPinBankDKI();
                walletPinBankDKI.setReference(accountVerificationPinResponse.getReference());
                walletPinBankDKI.setId(accountVerificationPinResponse.getId());
                Config.getInstance().setSessionAccountIDWallet(accountVerificationPinResponse.getId());
                return walletPinBankDKI;
            }

            return null;
        }

        @Override
        protected void onPostExecute(WalletPinBankDKI walletPinBankDKI) {
            super.onPostExecute(walletPinBankDKI);
            if( walletPinBankDKI != null ){
                if( infoProcess != null ){
                    infoProcess.hide();
                }

                mWalletPin = walletPinBankDKI;
                showOtpNewPin();
            }
            else {
                if (StringComponent.isNull(msgError)) {
                    msgError = getString(R.string.error_gkjelas);
                }

                if( !StringComponent.isNull(msgError) ){

                    if( infoProcess != null ){
                        infoProcess.hide();
                    }

                    infoError
                            .setTitle(getString(R.string.verification_failed))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
        }
    }
}
