package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.ListKIAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.NilaiDetail;
import id.adadigitals.edusia.widget.LineItemDecoration;

public class NilaiDetailAkademikActivity extends AppCompatActivity {

    LinearLayout loadingLayout;
    RecyclerView recyclerView;
    TextView resultTxt;
    ListKIAdapter listKIAdapter;

    TextView siswaNameToolbar;

    String nis              = "";
    String nama_siswa       = "";
    String semester         = "";
    String ajaran           = "";
    String idmatpel         = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_detail_nilai);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            nis         = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
            nama_siswa  = getIntent().getStringExtra(Config.KEY_PARAM_NAMA_SISWA);
            semester    = getIntent().getStringExtra(Config.KEY_PARAM_SEMESTER);
            ajaran      = getIntent().getStringExtra(Config.KEY_PARAM_AJARAN);
            idmatpel    = getIntent().getStringExtra(Config.KEY_PARAM_IDMATPEL);
        }
        else {
            finish();
        }

        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, NilaiDetailAkademikActivity.class);
        return intent;
    }

    private void declare(){
        siswaNameToolbar  = (TextView) findViewById(R.id.siswaNameToolbar);
        loadingLayout     = (LinearLayout) findViewById(R.id.loadingLayout);
        recyclerView      = (RecyclerView) findViewById(R.id.recyclerView);
        resultTxt         = (TextView) findViewById(R.id.resultTxt);
        listKIAdapter     = new ListKIAdapter(getApplicationContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new LineItemDecoration(getApplicationContext(),LinearLayout.VERTICAL));
        recyclerView.setAdapter(listKIAdapter);
    }

    private void run(){

        if( !StringComponent.isNull(nama_siswa) ){
            siswaNameToolbar.setText(nama_siswa);
        }

        if( !StringComponent.isNull(idmatpel) ){
            LoadDataKI loadDataKI = new LoadDataKI();
            loadDataKI.execute(nis, semester, ajaran, idmatpel);
        }
    }

    public class LoadDataKI extends AsyncTask<String, Void, List<NilaiDetail>>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        @Override
        protected List<NilaiDetail> doInBackground(String... strings) {
            String nis                              = strings[0];
            String semester                         = strings[1];
            String tahun_ajaran                     = strings[2];
            String id_matpel                        = strings[3];
            List<NilaiDetail> nilaiDetails          = new ArrayList<NilaiDetail>();

            ApiStudo apiStudo                       = new ApiStudo();
            List<NilaiDetail> response              = apiStudo.getNilaiDetail(nis, semester, tahun_ajaran, id_matpel);
            if( response != null ){
                if( response.size() > 0 ){
                    try{
                        int i = 0;
                        for(NilaiDetail nilaiDetail: response){
                            nilaiDetail.setId_ki(response.get(i).getId_ki());
                            nilaiDetail.setNis(nis);
                            nilaiDetail.setNama(nama_siswa);
                            nilaiDetail.setId_matpel(id_matpel);
                            nilaiDetail.setSemester(semester);
                            nilaiDetail.setAjaran(tahun_ajaran);
                            nilaiDetails.add(nilaiDetail);
                            i++;
                        }

                        return nilaiDetails;
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        Config.getInstance().addLogs("Error loop get nilai detail --> " + e.getMessage());
                    }
                }
                else {
                    msgError    = getString(R.string.text_data_notFound);
                }
            }
            else {
                msgError    = apiStudo.getMsgError();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<NilaiDetail> nilaiDetails) {
            super.onPostExecute(nilaiDetails);

            if( nilaiDetails != null && nilaiDetails.size() > 0 ){
                loadingLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                listKIAdapter.refreshAdapter(nilaiDetails);
            }
            else {

                if( !StringComponent.isNull(msgError) ){
                    Config.getInstance().addLogs("msg error get nilai sementara ==> " + msgError);
                }

                loadingLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                resultTxt.setVisibility(View.VISIBLE);
            }
        }
    }
}
