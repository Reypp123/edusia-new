package id.adadigitals.edusia.xui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.ImageComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.component.ViewAnimationComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import id.adadigitals.edusia.api.ApiStudo;

public class ValidationDataStudent extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private RelativeLayout rootLayout;
    private LinearLayout loadingLayout, studentDataLayout;
    private NestedScrollView dataLayout;

    private EditText etNamaWaliMurid;
    private EditText etTempatLahirWaliMurid;
    private EditText etTanggalLahirWaliMurid;
    private EditText etAlamatWaliMurid;
    private EditText etNoTelpWaliMurid;
    private EditText etNohpWaliMurid;
    private EditText etNikWaliMurid;
    private EditText etEmailWaliMurid;

    private Button btnNext;

    private boolean isWaliIbu       = false;
    private boolean isWaliFather    = false;

    private AwesomeProgressDialog infoProcess;
    private AwesomeErrorDialog infoError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_validation_data);
        initToolbar();
        declare();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, ValidationDataStudent.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private void initToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void declare(){
        rootLayout                  = (RelativeLayout) findViewById(R.id.rootLayout);
        loadingLayout               = (LinearLayout) findViewById(R.id.loadingLayout);
        studentDataLayout           = (LinearLayout) findViewById(R.id.studentDataLayout);
        dataLayout                  = (NestedScrollView) findViewById(R.id.dataLayout);
        etNamaWaliMurid             = (EditText) findViewById(R.id.etNamaWaliMurid);
        etTempatLahirWaliMurid      = (EditText) findViewById(R.id.etTempatLahirWaliMurid);
        etTanggalLahirWaliMurid     = (EditText) findViewById(R.id.etTanggalLahirWaliMurid);
        etAlamatWaliMurid           = (EditText) findViewById(R.id.etAlamatWaliMurid);
        etNoTelpWaliMurid           = (EditText) findViewById(R.id.etNoTelpWaliMurid);
        etNohpWaliMurid             = (EditText) findViewById(R.id.etNohpWaliMurid);
        etNikWaliMurid              = (EditText) findViewById(R.id.etNikWaliMurid);
        etEmailWaliMurid            = (EditText) findViewById(R.id.etEmailWaliMurid);
        infoProcess                 = new AwesomeProgressDialog(this);
        infoError                   = new AwesomeErrorDialog(this);
        btnNext                     = (Button) findViewById(R.id.btnContinue);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreExecuteSaveDataMurid();
            }
        });

        LoadDataTask loadDataTask = new LoadDataTask();
        loadDataTask.execute();
    }

    private void PreExecuteSaveDataMurid(){
        boolean InputError = false;
        View focusView     = null;
        List<Student> students = new ArrayList<Student>();

        String waliAlamat      = etAlamatWaliMurid.getText().toString();
        String waliTempatLahir = etTempatLahirWaliMurid.getText().toString();
        String waliTglLahir    = etTanggalLahirWaliMurid.getText().toString();
        String waliNoTelp      = etNoTelpWaliMurid.getText().toString();
        String waliEmail       = etEmailWaliMurid.getText().toString();

        if( StringComponent.isNull(waliAlamat) ){
            etAlamatWaliMurid.setError(getString(R.string.error_required_address_wali));
            InputError = true;
            focusView  = etAlamatWaliMurid;
        }


        if( StringComponent.isNull(waliTempatLahir) ){
            etTempatLahirWaliMurid.setError(getString(R.string.error_required_tempat_lahir_wali));
            InputError = true;
            focusView  = etTempatLahirWaliMurid;
        }

        if( StringComponent.isNull(waliTglLahir) ){
            etTanggalLahirWaliMurid.setError(getString(R.string.error_required_tgl_lahir_wali));
            InputError = true;
            focusView  = etTanggalLahirWaliMurid;
        }

        if( StringComponent.isNull(waliNoTelp) ){
            etNoTelpWaliMurid.setError(getString(R.string.error_required_no_telp_wali));
            InputError = true;
            focusView  = etNoTelpWaliMurid;
        }

        if( StringComponent.isNull(waliEmail) ){
            etEmailWaliMurid.setError(getString(R.string.error_required_email_wali));
            InputError = true;
            focusView  = etEmailWaliMurid;
        }

        if( InputError ){
            focusView.requestFocus();
        }
        else {

            for(int i=0; i < studentDataLayout.getChildCount(); i++){
                CardView studentCard = (CardView) studentDataLayout.getChildAt(i);
                Student student = (Student) studentCard.getTag();

                if( isWaliFather ){
                    student.setAyahAlamat(waliAlamat);
                    student.setAyahNoTelp(waliNoTelp);
                    student.setAyahTmptLahir(waliTempatLahir);
                    student.setAyahEmail(waliEmail);
                    student.setAyahTglLahir(waliTglLahir);
                }
                else if( isWaliIbu ){
                    student.setIbuAlamat(waliAlamat);
                    student.setIbuNoTelp(waliNoTelp);
                    student.setIbuTmptLahir(waliTempatLahir);
                    student.setIbuEmail(waliEmail);
                    student.setIbuTglLahir(waliTglLahir);
                }

                student.setAlamat(waliAlamat);
                student.setTlp(waliNoTelp);
                students.add(student);
            }

            SaveDataAsyncTask saveData = new SaveDataAsyncTask();
            saveData.execute(students);
        }
    }

    private void refreshData(List<Student> students){
        Student student = students.get(0);
        isWaliIbu       = student.isWaliIbu(Config.getInstance().getSessionPhone());
        isWaliFather    = student.isWaliAyah(Config.getInstance().getSessionPhone());

        if( isWaliIbu ){
            if( !StringComponent.isNull(student.getIbuNama()) ){
                etNamaWaliMurid.setText(StringComponent.upperCase(student.getIbuNama()));
            }

            if( !StringComponent.isNull(student.getIbuTmptLahir())){
                etTempatLahirWaliMurid.setText(StringComponent.upperCase(student.getIbuTmptLahir()));
            }

            if( !StringComponent.isNull(student.getIbuTglLahir()) ){
                etTanggalLahirWaliMurid.setText(student.getIbuTglLahir());
            }

            if( !StringComponent.isNull(student.getIbuAlamat()) ){
                etAlamatWaliMurid.setText(StringComponent.upperCase(student.getIbuAlamat()));
            }

            if( !StringComponent.isNull(student.getIbuNoTelp()) ){
                etNoTelpWaliMurid.setText(student.getIbuNoTelp());
            }

            if( !StringComponent.isNull(student.getIbuNoHp()) ){
                etNohpWaliMurid.setText(student.getIbuNoHp());
            }

            if( !StringComponent.isNull(student.getIbuNIK())){
                etNikWaliMurid.setText(student.getIbuNIK());
            }

            if( !StringComponent.isNull(student.getIbuEmail()) ){
                etEmailWaliMurid.setText(student.getIbuEmail());
            }
        }
        else if( isWaliFather ){
            if( !StringComponent.isNull(student.getAyahNama()) ){
                etNamaWaliMurid.setText(StringComponent.upperCase(student.getAyahNama()));
            }

            if( !StringComponent.isNull(student.getAyahTmptLahir()) ){
                etTempatLahirWaliMurid.setText(StringComponent.upperCase(student.getAyahTmptLahir()));
            }

            if( !StringComponent.isNull(student.getAyahTglLahir()) ){
                etTanggalLahirWaliMurid.setText(student.getAyahTglLahir());
            }

            if( !StringComponent.isNull(student.getAyahAlamat()) ){
                etAlamatWaliMurid.setText(StringComponent.upperCase(student.getAyahAlamat()));
            }

            if( !StringComponent.isNull(student.getAyahNoTelp()) ){
                etNoTelpWaliMurid.setText(student.getAyahNoTelp());
            }

            if( !StringComponent.isNull(student.getAyahNoHp()) ){
                etNohpWaliMurid.setText(student.getAyahNoHp());
            }

            if( !StringComponent.isNull(student.getAyahNIK())){
                etNikWaliMurid.setText(student.getAyahNIK());
            }

            if( !StringComponent.isNull(student.getAyahEmail()) ){
                etEmailWaliMurid.setText(student.getAyahEmail());
            }
        }

        Calendar c = Calendar.getInstance();
        final int year  = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day   = c.get(Calendar.DAY_OF_MONTH);
        etTanggalLahirWaliMurid.setFocusable(false);
        etTanggalLahirWaliMurid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(ValidationDataStudent.this,ValidationDataStudent.this, year, month, day);
                datePickerDialog.show();
            }
        });

        if( student != null && students.size() > 0){
            for (int i=0; i < students.size(); i++){
                CardView muridCard = (CardView) createCardMurid(studentDataLayout);
                muridCard.setTag(students.get(i));

                TextView titleText = (TextView) muridCard.findViewById(R.id.titleTxt);
                titleText.setText(students.get(i).getNama());

                final ImageButton toggleButton = (ImageButton) muridCard.findViewById(R.id.toggleBtn);
                final LinearLayout contentLayout = (LinearLayout) muridCard.findViewById(R.id.contentLayout);
                contentLayout.setVisibility(View.GONE);
                toggleButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toggleSectionText(contentLayout, toggleButton);
                    }
                });

                if( !StringComponent.isNull(students.get(i).getSiswaFotoUrl()) ){
                    final ImageView imgMuridPhoto = (ImageView) muridCard.findViewById(R.id.muridPhotoImg);
                    ImageComponent.displayImageRound(ValidationDataStudent.this, imgMuridPhoto, students.get(i).getSiswaFotoUrl());
                }

                EditText etMuridNis = (EditText) muridCard.findViewById(R.id.etMuridNis);
                etMuridNis.setText(students.get(i).getNIS());

                EditText etMuridKelas   = (EditText) muridCard.findViewById(R.id.etMuridKelas);
                etMuridKelas.setText(students.get(i).getKelas());

                EditText etMuridSekolah   = (EditText) muridCard.findViewById(R.id.etMuridSekolah);
                etMuridSekolah.setText(students.get(i).getSekolah());

                EditText etMuridTempatLahir   = (EditText) muridCard.findViewById(R.id.etMuridTempatLahir);
                etMuridTempatLahir.setText(students.get(i).getTempatLahir());

                EditText etTglLahir         = (EditText) muridCard.findViewById(R.id.etMuridTanggalLahir);
                etTglLahir.setText(students.get(i).getTanggalLahir());

                studentDataLayout.addView(muridCard);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String day = (dayOfMonth < 10)?"0"+dayOfMonth:dayOfMonth+"";
        month++;
        String monthCtr = (month < 10)?"0"+month:month+"";
        etTanggalLahirWaliMurid.setText(year + "-" + month + "-" + day);
    }

    private View createCardMurid(ViewGroup rootView){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View studentItem        = inflater.inflate(R.layout.expandable_card_student, rootView, false);
        return studentItem;
    }

    private void toggleSectionText(final LinearLayout contentLayout, ImageButton toggleBtn) {
        boolean show = toggleArrow(toggleBtn);
        if (show) {
            ViewAnimationComponent.expand(contentLayout, new ViewAnimationComponent.AnimListener() {
                @Override
                public void onFinish() {
                    StylingComponent.nestedScrollTo(dataLayout, contentLayout);
                }
            });
        } else {
            ViewAnimationComponent.collapse(contentLayout);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    public class SaveDataAsyncTask extends AsyncTask<List<Student>, Void, BasicResponse>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(R.string.text_waiting_saveData)
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();

            etAlamatWaliMurid.setEnabled(false);
            etEmailWaliMurid.setEnabled(false);
            etTempatLahirWaliMurid.setEnabled(false);
            etTempatLahirWaliMurid.setEnabled(false);
            etNoTelpWaliMurid.setEnabled(false);
        }

        @Override
        protected BasicResponse doInBackground(List<Student>... lists) {
            List<Student> students = lists[0];
            BasicResponse response = null;
            ApiStudo apiStudo      = new ApiStudo();

            if( (isWaliIbu && students.get(0).isParentTemplateExists(students.get(0).getIbuNIK())) ||
                    ( isWaliFather && students.get(0).isParentTemplateExists(students.get(0).getAyahNIK()) ) ){

                File resultFile = apiStudo.downloadFileNya(students.get(0).getParentTemplateUrl(), students.get(0).getParentTemplateFile());
                if( resultFile == null ){
                    msgError = apiStudo.getMsgError();
                    return null;
                }
            }

            Config.getInstance().addLogs("jumlah student ==> " + students.size());
            for (Student student:students){
               try{
                   if( student.isStudentTemplateExists() ){
                       File resultFile = apiStudo.downloadFileNya( student.getStudentTemplateUrl(), student.getStudentTemplateFile());
                       if( resultFile == null ){
                           msgError = apiStudo.getMsgError();
                           return null;
                       }
                   }

                   response = apiStudo.updateStudent(student);
                   if( response == null && response.rc == 0 ){
                       if( response == null ){
                           msgError = apiStudo.getMsgError();
                       }
                       else if( response.rc == 0 )
                       {
                           msgError = response.msg;
                       }
                   }

                   if( isWaliFather && !StringComponent.isNull(Config.getInstance().getFcmToken()) ){
                       response = apiStudo.updateFCMbyOrtu(student.getNIS(), student.getAyahNIK(), Config.getInstance().getFcmToken(), "ayah");
                       if( response == null || response.rc != 1 ){
                           msgError = apiStudo.getMsgError();
                           return null;
                       }
                   }
                   else if( isWaliIbu && !StringComponent.isNull(Config.getInstance().getFcmToken()) ){
                       response = apiStudo.updateFCMbyOrtu(student.getNIS(), student.getIbuNIK(), Config.getInstance().getFcmToken(), "ibu");
                       if( response == null || response.rc != 1 ){
                           msgError = apiStudo.getMsgError();
                           return null;
                       }
                   }

                   Thread.sleep(500);
               }
               catch (Exception e){
                   e.printStackTrace();
               }
            }

            return response;
        }

        @Override
        protected void onPostExecute(BasicResponse basicResponse) {
            super.onPostExecute(basicResponse);

            if( infoProcess != null ){
                infoProcess.hide();
            }

            if( basicResponse != null ){
                if( basicResponse.rc == 1 ){
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_FACE_ENROLLMENT);
                    showFaceActivity();
                    finish();
                }
                else {
                    infoError
                            .setTitle(getString(R.string.text_error))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
            else {
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(msgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {

                            }
                        })
                        .show();
            }
        }
    }

    public class LoadDataTask extends AsyncTask<Void,Void, List<Student>>{

        ApiStudo apistudo = new ApiStudo();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            dataLayout.setVisibility(View.GONE);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            getListMuridResponse listMurid  = apistudo.getDaftarMurid();
            List<Student> students          = listMurid.data;
            return students;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            if( students != null && students.size() > 0){
                refreshData(students);
            }

            loadingLayout.setVisibility(View.GONE);
            dataLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showFaceActivity(){
        Intent faceAct = FaceCaptureActivity.createIntent(getApplicationContext());
        startActivity(faceAct);
    }
}
