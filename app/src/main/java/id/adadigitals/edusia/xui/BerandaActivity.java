package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.fragment.FragmentAkademikTabs;
import id.adadigitals.edusia.fragment.FragmentAkunTabs;
import id.adadigitals.edusia.fragment.FragmentDashboardTabs;
import id.adadigitals.edusia.fragment.FragmentEkosistemTabs;
import id.adadigitals.edusia.fragment.FragmentKehadiranTabs;

import java.util.ArrayList;
import java.util.List;

public class BerandaActivity extends AppCompatActivity {
    private ViewPager view_pager;
    private TabLayout tab_layout;
    private SectionsPagerAdapter viewPagerAdapter;

    int toolbarTitle[] = {
            R.string.label_beranda,
            R.string.label_kehadiran,
            R.string.label_akademik,
            R.string.label_ekosistem,
            R.string.label_akun,
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_beranda);
        initToolbar();
        initComponent();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, BerandaActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP|intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_action_bar_beranda));
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void initComponent(){
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        tab_layout = (TabLayout) findViewById(R.id.tab_layout);
        setupViewPager(view_pager);

        tab_layout.setupWithViewPager(view_pager);

        tab_layout.getTabAt(0).setIcon(getResources().getDrawable(R.drawable.icon_beranda));
        tab_layout.getTabAt(1).setIcon(getResources().getDrawable(R.drawable.icon_kehadiran));
        tab_layout.getTabAt(2).setIcon(getResources().getDrawable(R.drawable.icon_akademik));
        tab_layout.getTabAt(3).setIcon(getResources().getDrawable(R.drawable.icon_ecosystem));
        tab_layout.getTabAt(4).setIcon(getResources().getDrawable(R.drawable.icon_akunprofile));

        /*// set icon color pre-selected
        tab_layout.getTabAt(0).getIcon().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(1).getIcon().setColorFilter(getResources().getColor(R.color.color_disable_primary), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(2).getIcon().setColorFilter(getResources().getColor(R.color.color_disable_primary), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(3).getIcon().setColorFilter(getResources().getColor(R.color.color_disable_primary), PorterDuff.Mode.SRC_IN);
        tab_layout.getTabAt(4).getIcon().setColorFilter(getResources().getColor(R.color.color_disable_primary), PorterDuff.Mode.SRC_IN);*/

        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getSupportActionBar().setTitle(toolbarTitle[tab.getPosition()]);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(FragmentDashboardTabs.newInstance());
        viewPagerAdapter.addFragment(FragmentKehadiranTabs.newInstance());
        viewPagerAdapter.addFragment(FragmentAkademikTabs.newInstance());
        viewPagerAdapter.addFragment(FragmentEkosistemTabs.newInstance());
        viewPagerAdapter.addFragment(FragmentAkunTabs.newInstance());
        viewPager.setAdapter(viewPagerAdapter);
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logout(){
        Config.getInstance().resetUserVerifikasi();
        Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFICATION);
        Intent verificationActivity = UserVerificationActivity.createIntent(getApplicationContext());
        startActivity(verificationActivity);
    }
}
