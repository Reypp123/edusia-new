package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.DataKd;
import id.adadigitals.edusia.data.DetailKD;
import id.adadigitals.edusia.data.NilaiDetail;
import id.adadigitals.edusia.data.NilaiSementara;

public class NilaiKDAkademikActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    LineChart chart;
    TextView siswaNameToolbar;
    TextView resultTxt;

    LinearLayout loadingLayout;
    LinearLayout mainLayout;
    NestedScrollView scrollView;
    TableLayout table;

    String nis              = "";
    String nama_siswa       = "";
    String semester         = "";
    String ajaran           = "";
    String idmatpel         = "";
    String IDKompetensi     = "";

    public HashMap<Integer, String> label;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_nilai_kd);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            nis             = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
            nama_siswa      = getIntent().getStringExtra(Config.KEY_PARAM_NAMA_SISWA);
            semester        = getIntent().getStringExtra(Config.KEY_PARAM_SEMESTER);
            ajaran          = getIntent().getStringExtra(Config.KEY_PARAM_AJARAN);
            idmatpel        = getIntent().getStringExtra(Config.KEY_PARAM_IDMATPEL);
            IDKompetensi    = getIntent().getStringExtra(Config.KEY_PARAM_KI);
        }
        else {
            finish();
        }

        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, NilaiKDAkademikActivity.class);
        return intent;
    }

    private void declare(){
        siswaNameToolbar  = (TextView) findViewById(R.id.siswaNameToolbar);
        chart             = (LineChart) findViewById(R.id.chart1);
        loadingLayout     = (LinearLayout) findViewById(R.id.loadingLayout);
        mainLayout        = (LinearLayout) findViewById(R.id.mainLayout);
        scrollView        = (NestedScrollView) findViewById(R.id.scrollView);
        resultTxt         = (TextView) findViewById(R.id.resultTxt);
        table             = new TableLayout(this);
    }

    private void initChart( List<NilaiDetail> nilaiDetil ){
        if( nilaiDetil != null && nilaiDetil.size() > 0 ) {

            for (int i = 0; i < nilaiDetil.size(); i++) {
                if( nilaiDetil.get(i).getId_ki().equals(IDKompetensi) ){
                    List<DataKd> kdData     = nilaiDetil.get(i).getKd();
                    label                   = new HashMap<>();
                    ArrayList<Entry> values = new ArrayList<>();
                    if( kdData.size() > 1 ){
                        for( int z = 0; z<kdData.size(); z++ ){
                            label.put(z,"KD " + kdData.get(z).getNama());

                            float val    = Float.valueOf(kdData.get(z).getNilai_nh());
                            if( kdData.get(z).getNilai_nh().equals("") ){
                                val = 0;
                            }

                            values.add(new Entry(z, val));
                        }

                        LineDataSet set1;

                        if (chart.getData() != null && chart.getData().getDataSetCount() > 0) {
                            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
                            set1.setValues(values);
                            set1.notifyDataSetChanged();
                            chart.getData().notifyDataChanged();
                            chart.notifyDataSetChanged();
                        } else {
                            // create a dataset and give it a type
                            set1 = new LineDataSet(values, "Kompetensi Dasar");
                            set1.setDrawIcons(false);
                            set1.enableDashedLine(10f, 5f, 0f);
                            set1.setColor(Color.BLACK);
                            set1.setCircleColor(Color.BLACK);
                            set1.setLineWidth(1f);
                            set1.setCircleRadius(3f);
                            set1.setDrawCircleHole(false);
                            set1.setFormLineWidth(1f);
                            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
                            set1.setFormSize(15.f);
                            set1.setValueTextSize(9f);
                            set1.enableDashedHighlightLine(10f, 5f, 0f);
                            set1.setDrawFilled(true);
                            set1.setFillFormatter(new IFillFormatter() {
                                @Override
                                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                                    return chart.getAxisLeft().getAxisMinimum();
                                }
                            });

                            // set color of filled area
                            if (Utils.getSDKInt() >= 18) {
                                // drawables only supported on api level 18 and above
                                Drawable drawable = ContextCompat.getDrawable(this, R.drawable.fade_red);
                                set1.setFillDrawable(drawable);
                            } else {
                                set1.setFillColor(Color.BLACK);
                            }

                            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                            dataSets.add(set1);
                            LineData data = new LineData(dataSets);
                            chart.setData(data);
                            chart.invalidate();
                        }

                        XAxis xAxis;
                        xAxis = chart.getXAxis();
                        xAxis.enableGridDashedLine(10f, 10f, 0f);
                        xAxis.setValueFormatter(new ValueFormatter() {
                            @Override
                            public String getFormattedValue(float value) {
                                return label.get((int) value);
                            }
                        });
                        xAxis.setLabelCount(label.size(), true);

                        YAxis yAxis;
                        yAxis = chart.getAxisLeft();
                        yAxis.enableGridDashedLine(10f, 10f, 0f);
                        yAxis.setAxisMaximum(100f);
                        yAxis.setAxisMinimum(0f);

                        chart.setBackgroundColor(Color.WHITE);
                        chart.getDescription().setEnabled(false);
                        chart.setTouchEnabled(true);
                        chart.setOnChartValueSelectedListener(this);
                        chart.setDrawGridBackground(false);
                        chart.setDragEnabled(true);
                        chart.setScaleEnabled(true);
                        chart.setPinchZoom(true);
                        chart.getAxisRight().setEnabled(false);
                        chart.animateX(2000);

                        Legend l = chart.getLegend();
                        l.setForm(Legend.LegendForm.LINE);
                    }
                    else {
                        chart.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    private void run(){
        table.setLayoutParams(new TableLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        table.setShrinkAllColumns(true);
        table.setStretchAllColumns(true);

        if( !StringComponent.isNull(nama_siswa) ){
            siswaNameToolbar.setText(nama_siswa);
        }

        if( !StringComponent.isNull(IDKompetensi) ){
            LoadDataFinalNilai loadDataFinalNilai = new LoadDataFinalNilai();
            loadDataFinalNilai.execute(nis, semester, ajaran, idmatpel);
        }
    }

    public void parseData(List<NilaiDetail> nilaiDetil){
        if( nilaiDetil != null && nilaiDetil.size() > 0 ){
            for(int i=0; i<nilaiDetil.size(); i++){
                if( nilaiDetil.get(i).getId_ki().equals(IDKompetensi) ){
                    switch (IDKompetensi){
                        case "1":
                            List<DataKd> kdData = nilaiDetil.get(i).getKd();
                            for( int z = 0; z<kdData.size(); z++ ){

                                //BARIS 1
                                TableRow rowTitle = new TableRow(NilaiKDAkademikActivity.this);
                                rowTitle.setGravity(Gravity.CENTER_HORIZONTAL);

                                // title column/row
                                TextView title = new TextView(NilaiKDAkademikActivity.this);
                                title.setText("KD " + kdData.get(z).getNama());

                                title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                                title.setGravity(Gravity.CENTER);
                                title.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                title.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                TableRow.LayoutParams params = new TableRow.LayoutParams();
                                params.topMargin = 30;
                                params.span = 4;

                                rowTitle.addView(title,params);

                                table.addView(rowTitle);

                                //BARIS 2

                                TableRow rows01 = new TableRow(NilaiKDAkademikActivity.this);
                                rows01.setGravity(Gravity.CENTER_HORIZONTAL);

                                TextView tv01 = new TextView(NilaiKDAkademikActivity.this);
                                tv01.setGravity(Gravity.CENTER);
                                tv01.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv01.setText("SKP");
                                tv01.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv01);

                                TextView tv05 = new TextView(NilaiKDAkademikActivity.this);
                                tv05.setGravity(Gravity.CENTER);
                                tv05.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv05.setText("RD");
                                tv05.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv05);

                                TextView tv06 = new TextView(NilaiKDAkademikActivity.this);
                                tv06.setGravity(Gravity.CENTER);
                                tv06.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv06.setText("PY");
                                tv06.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv06);

                                TextView tv07 = new TextView(NilaiKDAkademikActivity.this);
                                tv07.setGravity(Gravity.CENTER);
                                tv07.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv07.setText("NH");
                                tv07.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv07);

                                table.addView(rows01);

                                //BARIS 3
                                TableRow row = new TableRow(NilaiKDAkademikActivity.this);
                                row.setGravity(Gravity.CENTER_HORIZONTAL);

                                List<DetailKD> detail = kdData.get(z).getDetail();

                                for( int xx = 0; xx < detail.size(); xx++){
                                    TextView br1 = new TextView(NilaiKDAkademikActivity.this);
                                    br1.setGravity(Gravity.CENTER);
                                    br1.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                    br1.setText(detail.get(xx).getNilai());
                                    br1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                    row.addView(br1);
                                }

                                TextView br5 = new TextView(NilaiKDAkademikActivity.this);
                                br5.setGravity(Gravity.CENTER);
                                br5.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br5.setText(kdData.get(z).getNilai_rd());
                                br5.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br5);

                                TextView br6 = new TextView(NilaiKDAkademikActivity.this);
                                br6.setGravity(Gravity.CENTER);
                                br6.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br6.setText(kdData.get(z).getNilai_py());
                                br6.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br6);

                                TextView br7 = new TextView(NilaiKDAkademikActivity.this);
                                br7.setGravity(Gravity.CENTER);
                                br7.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br7.setText(kdData.get(z).getNilai_nh());
                                br7.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br7);

                                table.addView(row);
                            }

                            mainLayout.addView(table);
                        break;
                        case "2":
                            List<DataKd> KD = nilaiDetil.get(i).getKd();
                            for( int z = 0; z<KD.size(); z++ ){

                                //BARIS 1
                                TableRow rowTitle = new TableRow(NilaiKDAkademikActivity.this);
                                rowTitle.setGravity(Gravity.CENTER_HORIZONTAL);

                                // title column/row
                                TextView title = new TextView(NilaiKDAkademikActivity.this);
                                title.setText("KD " + KD.get(z).getNama());

                                title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                                title.setGravity(Gravity.CENTER);
                                title.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                title.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                TableRow.LayoutParams params = new TableRow.LayoutParams();
                                params.topMargin = 30;
                                params.span = 4;

                                rowTitle.addView(title,params);

                                table.addView(rowTitle);

                                //BARIS 2

                                TableRow rows01 = new TableRow(NilaiKDAkademikActivity.this);
                                rows01.setGravity(Gravity.CENTER_HORIZONTAL);

                                TextView tv01 = new TextView(NilaiKDAkademikActivity.this);
                                tv01.setGravity(Gravity.CENTER);
                                tv01.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv01.setText("SKP");
                                tv01.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv01);

                                TextView tv05 = new TextView(NilaiKDAkademikActivity.this);
                                tv05.setGravity(Gravity.CENTER);
                                tv05.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv05.setText("RD");
                                tv05.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv05);

                                TextView tv06 = new TextView(NilaiKDAkademikActivity.this);
                                tv06.setGravity(Gravity.CENTER);
                                tv06.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv06.setText("PY");
                                tv06.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv06);

                                TextView tv07 = new TextView(NilaiKDAkademikActivity.this);
                                tv07.setGravity(Gravity.CENTER);
                                tv07.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv07.setText("NH");
                                tv07.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv07);

                                table.addView(rows01);

                                //BARIS 3
                                TableRow row = new TableRow(NilaiKDAkademikActivity.this);
                                row.setGravity(Gravity.CENTER_HORIZONTAL);

                                List<DetailKD> detail = KD.get(z).getDetail();

                                for( int xx = 0; xx < detail.size(); xx++){
                                    TextView br1 = new TextView(NilaiKDAkademikActivity.this);
                                    br1.setGravity(Gravity.CENTER);
                                    br1.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                    br1.setText(detail.get(xx).getNilai());
                                    br1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                    row.addView(br1);
                                }

                                TextView br5 = new TextView(NilaiKDAkademikActivity.this);
                                br5.setGravity(Gravity.CENTER);
                                br5.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br5.setText(KD.get(z).getNilai_rd());
                                br5.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br5);

                                TextView br6 = new TextView(NilaiKDAkademikActivity.this);
                                br6.setGravity(Gravity.CENTER);
                                br6.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br6.setText(KD.get(z).getNilai_py());
                                br6.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br6);

                                TextView br7 = new TextView(NilaiKDAkademikActivity.this);
                                br7.setGravity(Gravity.CENTER);
                                br7.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br7.setText(KD.get(z).getNilai_nh());
                                br7.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br7);

                                table.addView(row);
                            }

                            mainLayout.addView(table);
                        break;
                        case "3":
                            List<DataKd> dataKD = nilaiDetil.get(i).getKd();
                            for( int z = 0; z<dataKD.size(); z++ ){

                                //BARIS 1
                                TableRow rowTitle = new TableRow(NilaiKDAkademikActivity.this);
                                rowTitle.setGravity(Gravity.CENTER_HORIZONTAL);

                                // title column/row
                                TextView title = new TextView(NilaiKDAkademikActivity.this);
                                title.setText("KD " + dataKD.get(z).getNama());

                                title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                                title.setGravity(Gravity.CENTER);
                                title.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                title.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                TableRow.LayoutParams params = new TableRow.LayoutParams();
                                params.topMargin = 30;
                                params.span = 6;

                                rowTitle.addView(title,params);

                                table.addView(rowTitle);

                                //BARIS 2

                                TableRow rows01 = new TableRow(NilaiKDAkademikActivity.this);
                                rows01.setGravity(Gravity.CENTER_HORIZONTAL);

                                TextView tv01 = new TextView(NilaiKDAkademikActivity.this);
                                tv01.setGravity(Gravity.CENTER);
                                tv01.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv01.setText("TT");
                                tv01.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv01);

                                TextView tv02 = new TextView(NilaiKDAkademikActivity.this);
                                tv02.setGravity(Gravity.CENTER);
                                tv02.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv02.setText("TL");
                                tv02.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv02);

                                TextView tv03 = new TextView(NilaiKDAkademikActivity.this);
                                tv03.setGravity(Gravity.CENTER);
                                tv03.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv03.setText("NP");
                                tv03.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv03);

                                TextView tv05 = new TextView(NilaiKDAkademikActivity.this);
                                tv05.setGravity(Gravity.CENTER);
                                tv05.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv05.setText("RD");
                                tv05.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv05);

                                TextView tv06 = new TextView(NilaiKDAkademikActivity.this);
                                tv06.setGravity(Gravity.CENTER);
                                tv06.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv06.setText("PY");
                                tv06.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv06);

                                TextView tv07 = new TextView(NilaiKDAkademikActivity.this);
                                tv07.setGravity(Gravity.CENTER);
                                tv07.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv07.setText("NH");
                                tv07.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv07);

                                table.addView(rows01);

                                //BARIS 3
                                TableRow row = new TableRow(NilaiKDAkademikActivity.this);
                                row.setGravity(Gravity.CENTER_HORIZONTAL);

                                List<DetailKD> detail = dataKD.get(z).getDetail();

                                for( int xx = 0; xx < detail.size(); xx++){
                                    TextView br1 = new TextView(NilaiKDAkademikActivity.this);
                                    br1.setGravity(Gravity.CENTER);
                                    br1.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                    br1.setText(detail.get(xx).getNilai());
                                    br1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                    row.addView(br1);
                                }

                                TextView br5 = new TextView(NilaiKDAkademikActivity.this);
                                br5.setGravity(Gravity.CENTER);
                                br5.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br5.setText(dataKD.get(z).getNilai_rd());
                                br5.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br5);

                                TextView br6 = new TextView(NilaiKDAkademikActivity.this);
                                br6.setGravity(Gravity.CENTER);
                                br6.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br6.setText(dataKD.get(z).getNilai_py());
                                br6.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br6);

                                TextView br7 = new TextView(NilaiKDAkademikActivity.this);
                                br7.setGravity(Gravity.CENTER);
                                br7.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br7.setText(dataKD.get(z).getNilai_nh());
                                br7.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br7);

                                table.addView(row);
                            }

                            mainLayout.addView(table);
                        break;
                        case "4":
                            List<DataKd> datakd = nilaiDetil.get(i).getKd();
                            for( int z = 0; z<datakd.size(); z++ ){

                                //BARIS 1
                                TableRow rowTitle = new TableRow(NilaiKDAkademikActivity.this);
                                rowTitle.setGravity(Gravity.CENTER_HORIZONTAL);

                                // title column/row
                                TextView title = new TextView(NilaiKDAkademikActivity.this);
                                title.setText("KD " + datakd.get(z).getNama());

                                title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                                title.setGravity(Gravity.CENTER);
                                title.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                title.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                TableRow.LayoutParams params = new TableRow.LayoutParams();
                                params.topMargin = 30;
                                params.span = 5;

                                rowTitle.addView(title,params);

                                table.addView(rowTitle);

                                //BARIS 2

                                TableRow rows01 = new TableRow(NilaiKDAkademikActivity.this);
                                rows01.setGravity(Gravity.CENTER_HORIZONTAL);

                                TextView tv01 = new TextView(NilaiKDAkademikActivity.this);
                                tv01.setGravity(Gravity.CENTER);
                                tv01.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv01.setText("PRY");
                                tv01.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv01);

                                TextView tv02 = new TextView(NilaiKDAkademikActivity.this);
                                tv02.setGravity(Gravity.CENTER);
                                tv02.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv02.setText("PRK");
                                tv02.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv02);

                                TextView tv05 = new TextView(NilaiKDAkademikActivity.this);
                                tv05.setGravity(Gravity.CENTER);
                                tv05.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv05.setText("RD");
                                tv05.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv05);

                                TextView tv06 = new TextView(NilaiKDAkademikActivity.this);
                                tv06.setGravity(Gravity.CENTER);
                                tv06.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv06.setText("PY");
                                tv06.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv06);

                                TextView tv07 = new TextView(NilaiKDAkademikActivity.this);
                                tv07.setGravity(Gravity.CENTER);
                                tv07.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                tv07.setText("NH");
                                tv07.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                rows01.addView(tv07);

                                table.addView(rows01);

                                //BARIS 3
                                TableRow row = new TableRow(NilaiKDAkademikActivity.this);
                                row.setGravity(Gravity.CENTER_HORIZONTAL);

                                List<DetailKD> detail = datakd.get(z).getDetail();

                                for( int xx = 0; xx < detail.size(); xx++){
                                    TextView br1 = new TextView(NilaiKDAkademikActivity.this);
                                    br1.setGravity(Gravity.CENTER);
                                    br1.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                    br1.setText(detail.get(xx).getNilai());
                                    br1.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                    row.addView(br1);
                                }

                                TextView br5 = new TextView(NilaiKDAkademikActivity.this);
                                br5.setGravity(Gravity.CENTER);
                                br5.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br5.setText(datakd.get(z).getNilai_rd());
                                br5.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br5);

                                TextView br6 = new TextView(NilaiKDAkademikActivity.this);
                                br6.setGravity(Gravity.CENTER);
                                br6.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br6.setText(datakd.get(z).getNilai_py());
                                br6.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br6);

                                TextView br7 = new TextView(NilaiKDAkademikActivity.this);
                                br7.setGravity(Gravity.CENTER);
                                br7.setTypeface(Typeface.SERIF, Typeface.BOLD);
                                br7.setText(datakd.get(z).getNilai_nh());
                                br7.setBackground(getResources().getDrawable(R.drawable.cell_shape));
                                row.addView(br7);

                                table.addView(row);
                            }

                            mainLayout.addView(table);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    public class LoadDataFinalNilai extends AsyncTask<String, Void, List<NilaiDetail>>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }

        @Override
        protected List<NilaiDetail> doInBackground(String... strings) {
            String nis                              = strings[0];
            String semester                         = strings[1];
            String tahun_ajaran                     = strings[2];
            String id_matpel                        = strings[3];
            List<NilaiDetail> nilaiDetails          = new ArrayList<NilaiDetail>();

            ApiStudo apiStudo                       = new ApiStudo();
            List<NilaiDetail> response              = apiStudo.getNilaiDetail(nis, semester, tahun_ajaran, id_matpel);
            if( response != null ){
                if( response.size() > 0 ){
                    return response;
                }
                else {
                    msgError    = getString(R.string.text_data_notFound);
                }
            }
            else {
                msgError    = apiStudo.getMsgError();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<NilaiDetail> nilaiDetails) {
            super.onPostExecute(nilaiDetails);

            if( nilaiDetails != null && nilaiDetails.size() > 0 ){
                loadingLayout.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
                parseData(nilaiDetails);
                initChart(nilaiDetails);
            }
            else {
                if( !StringComponent.isNull(msgError) ){
                    Config.getInstance().addLogs("msg error get nilai sementara ==> " + msgError);
                }

                loadingLayout.setVisibility(View.GONE);
                scrollView.setVisibility(View.GONE);
                resultTxt.setVisibility(View.VISIBLE);
            }
        }
    }
}
