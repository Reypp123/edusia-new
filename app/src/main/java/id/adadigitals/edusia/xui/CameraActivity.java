package id.adadigitals.edusia.xui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import id.adadigitals.cameraview.CameraView;
import id.adadigitals.cameraview.camera.IPhoto;
import id.adadigitals.cameraview.image.AspectRatio;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.BitmapComponent;
import id.adadigitals.edusia.component.FileComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.helper.PermissionRequester;
import id.adadigitals.edusia.view.OverlayView;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class CameraActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final float TRACKING_MASK_WIDTH_RATIO = 0.6f;
    private static final float TRACKING_MASK_HEIGHT_RATIO = 0.6f;

    private CameraView mCameraView;
    private OverlayView mOverlayview;

    private Handler mBackgroundHandler;

    int faceIndex = 0;
    int faceGroup = 0;
    String faceUid = null;

    private View.OnClickListener mOnclickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.take_picture:
                    if( mCameraView != null ){
                        mCameraView.takePicture();
                    }
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_camera);

        faceIndex = getIntent().getIntExtra(Config.KEY_PARAM_FACE_INDEX, -1);
        faceGroup = getIntent().getIntExtra(Config.KEY_PARAM_FACE_GROUP, -1);
        faceUid   = getIntent().getStringExtra(Config.KEY_PARAM_FACE_UID);

        if(StringComponent.isNull(faceUid) || faceIndex == -1 || faceGroup == -1 ){
            finish();
        }

        mCameraView = (CameraView) findViewById(R.id.camera);
        mCameraView.setFacing(CameraView.FACING_FRONT);
        mCameraView.setAspectRatio(AspectRatio.parse("4:3"));
        mOverlayview = (OverlayView) findViewById(R.id.overlayView);
        if( mCameraView != null){
            mCameraView.addCallback(mCallback);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.take_picture);
        if( fab != null ){
            fab.setOnClickListener(mOnclickListener);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if( actionBar != null ){
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(PermissionRequester.requestCameraPermissionIfNeeded(this,REQUEST_CAMERA_PERMISSION)){
            mCameraView.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if( mBackgroundHandler != null ){
            mBackgroundHandler.getLooper().quitSafely();
            mBackgroundHandler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (!PermissionRequester.isPermissionGranted(grantResults)){
                    Toast.makeText(this, R.string.toast_permission_to_access_camera,
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
        }
    }

    private CameraView.Callback mCallback = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            Config.getInstance().addLogs("onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            Config.getInstance().addLogs("onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] bytes) {
            Config.getInstance().addLogs("onPictureTaken " + bytes.length);
            getBackgroundHandler().post(new Runnable() {
                @Override
                public void run() {
                    final Uri photoUri = Uri.fromFile(FileComponent.getPhotoFile(faceGroup,faceUid));
                    final Uri templateUri = Uri.fromFile(FileComponent.getTemplateFile(faceGroup,faceUid));
                    File templateFile = new File(templateUri.getPath());

                    FileOutputStream out = null;
                    try{
                        Matrix matrix = checkRotate(bytes);
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                        out = new FileOutputStream(photoUri.getPath());
                        if(out!=null) {
                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                            out.close();
                            new BitmapComponent().save(rotatedBitmap, templateFile.getAbsolutePath());
                            Intent intent = new Intent();
                            intent.putExtra(Config.KEY_PARAM_FACE_INDEX, faceIndex);
                            intent.putExtra(Config.KEY_PARAM_FACE_GROUP, faceGroup);
                            intent.putExtra(Config.KEY_PARAM_FACE_UID, faceUid);
                            intent.putExtra(Config.KEY_PARAM_PHOTO_PATH, photoUri);
                            intent.putExtra(Config.KEY_PARAM_TEMPLATE_PATH, templateUri);
                            setResult(Config.FACE_CAPTURE_SUCCESS, intent);
                            finish();
                        }
                    }catch(Exception e){
                        Config.getInstance().addLogs("error save gambar");
                    }
                    setResult(Config.FACE_CAPTURE_FAIL);
                    finish();
                }
            });
        }

        @Override
        public void onPreviewFrame(CameraView cameraView, final IPhoto photo) {
            super.onPreviewFrame(cameraView, photo);
        }

        @Override
        public void onLayoutChanged(int width, int height) {
            super.onLayoutChanged(width, height);
            mOverlayview.setLayoutParams(new RelativeLayout.LayoutParams(width, height));

            Paint whitePaint = new Paint();
            whitePaint.setStyle(Paint.Style.STROKE);
            whitePaint.setColor(Color.WHITE);

            Rect whiteRect = new Rect(
                    (int) (width * (1 - TRACKING_MASK_WIDTH_RATIO)) / 2,
                    (int) (height * (1 - TRACKING_MASK_HEIGHT_RATIO)) / 2,
                    (int) (width * (1 + TRACKING_MASK_WIDTH_RATIO)) / 2,
                    (int) (height * (1 + TRACKING_MASK_HEIGHT_RATIO)) / 2);

            mOverlayview.drawRectangles(new Rect[]{whiteRect}, new Paint[]{whitePaint});

            System.out.println(" onDraw === LAYOUT SIZE: "+width+" x "+height);
        }
    };

    public Matrix checkRotate(byte[] bytes){
        Matrix matrix = new Matrix();
        try {
            InputStream is = new ByteArrayInputStream(bytes);
            ExifInterface exif = new ExifInterface(is);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            if(rotation == 0)
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_ROTATE_90+"");
            switch (rotation) {
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    matrix.setScale(-1, 1);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;

                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;

                case ExifInterface.ORIENTATION_TRANSPOSE:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;

                case ExifInterface.ORIENTATION_TRANSVERSE:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(-90);
                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                default:
                    break;
            }
        }catch (Exception e){
            Config.getInstance().addLogs("Error Camera activity LINE : 209");
        }
        return matrix;
    }

    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }
}
