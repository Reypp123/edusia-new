package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.ListMataPelajaranAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.NilaiSementara;
import id.adadigitals.edusia.data.TahunAkademik;
import id.adadigitals.edusia.response.json.GetListTahunAkademikResponse;
import id.adadigitals.edusia.view.RadarMarkerView;
import id.adadigitals.edusia.widget.LineItemDecoration;

public class AkademikDetailActivity extends AppCompatActivity {

    LinearLayout layoutContent, filterLayout;
    RecyclerView listMataPelajaran;
    TextView resultTxt;

    TextView siswaNameToolbar;
    ProgressBar loadingBar;
    AwesomeErrorDialog infoError;
    RadarChart chart;

    Spinner semester;
    Spinner tahunAjaran;

    NestedScrollView scroll_view;

    ListMataPelajaranAdapter listMataPelajaranAdapter;

    List<StringWithTag> itemList = new ArrayList<StringWithTag>();

    String nis              = "";
    String nama_siswa       = "";

    public String[] DATA_NAME;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_akademik_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            nis         = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
            nama_siswa  = getIntent().getStringExtra(Config.KEY_PARAM_NAMA_SISWA);
        }
        else {
            finish();
        }

        declare();
        run();
    }

    public static Intent createIntent(Context context){
        return new Intent(context,AkademikDetailActivity.class);
    }

    private void declare(){
        scroll_view                             = (NestedScrollView) findViewById(R.id.scrollView);
        layoutContent                           = (LinearLayout) findViewById(R.id.layoutContent);
        filterLayout                           = (LinearLayout) findViewById(R.id.filterLayout);
        listMataPelajaran                       = (RecyclerView) findViewById(R.id.recyclerMataPelajaran) ;
        siswaNameToolbar                        = (TextView) findViewById(R.id.siswaNameToolbar);
        resultTxt                               = (TextView) findViewById(R.id.resultTxt);
        loadingBar                              = (ProgressBar) findViewById(R.id.loadingBar);
        semester                                = (Spinner) findViewById(R.id.semester);
        tahunAjaran                             = (Spinner) findViewById(R.id.tahunAjaran);
        infoError                               = new AwesomeErrorDialog(this);
        chart                                   = (RadarChart) findViewById(R.id.chart1);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listMataPelajaran.setLayoutManager(linearLayoutManager);
        listMataPelajaran.addItemDecoration(new LineItemDecoration(this, LinearLayout.VERTICAL));
        listMataPelajaranAdapter                = new ListMataPelajaranAdapter(getApplicationContext());
        listMataPelajaran.setAdapter(listMataPelajaranAdapter);
    }

    private void run(){


        if( !StringComponent.isNull(nama_siswa) ){
            siswaNameToolbar.setText(nama_siswa);
        }

        semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(position);
                Object tag      = s.tag;
                Config.getInstance().setSessSelectedSemester(String.valueOf(tag));
                execLoadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tahunAjaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag s = (StringWithTag) parent.getItemAtPosition(position);
                Object tag      = s.tag;
                Config.getInstance().setSessSelectedTahun(String.valueOf(tag));
                execLoadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        itemList.add(new StringWithTag("1","1"));
        itemList.add(new StringWithTag("2","2"));
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, itemList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        semester.setAdapter(dataAdapter);

        LoadTahunAkademik loadTahunAkademik = new LoadTahunAkademik();
        loadTahunAkademik.execute();
    }

    private void execLoadData(){
        Config.getInstance().addLogs("semester ==> " + Config.getInstance().getSessionSemester());
        Config.getInstance().addLogs("tahun akademik ==> " + Config.getInstance().getSessionTahunAkademik());
        LoadNilaiSementara loadNilaiSementara = new LoadNilaiSementara();
        loadNilaiSementara.execute(nis,Config.getInstance().getSessionSemester(),Config.getInstance().getSessionTahunAkademik());
    }

    public void setData(List<NilaiSementara> nilaiSementara) {

        ArrayList<RadarEntry> entries1 = new ArrayList<>();
//        ArrayList<RadarEntry> entries2 = new ArrayList<>();

        if( nilaiSementara != null && nilaiSementara.size() > 2 ){

            DATA_NAME   = new String[nilaiSementara.size()];

            int i = 0;
            for(NilaiSementara nilai:nilaiSementara){
                entries1.add(new RadarEntry(Float.valueOf(nilai.nilai)));
                DATA_NAME[i] = nilai.matapelajaran;
                i++;
            }

            chart.getDescription().setEnabled(false);
            chart.setWebLineWidth(1f);
            chart.setWebColor(Color.BLACK);
            chart.setWebLineWidthInner(1f);
            chart.setWebColorInner(Color.BLACK);
            chart.setWebAlpha(100);

            // create a custom MarkerView (extend MarkerView) and spectvContentfy the layout
            // to use for it
            MarkerView mv = new RadarMarkerView(this, R.layout.radar_markerview);
            mv.setChartView(chart); // For bounds control
            chart.setMarker(mv); // Set the marker to the chart

            chart.animateXY(1400, 1400, Easing.EaseInOutQuad);

            XAxis xAxis = chart.getXAxis();
            xAxis.setTextSize(9f);
            xAxis.setYOffset(0f);
            xAxis.setXOffset(0f);
            xAxis.setValueFormatter(new ValueFormatter() {

//                private final String[] mActivities = new String[]{"Burger", "Steak", "asdasd", "asdadsad"};

                @Override
                public String getFormattedValue(float value) {
                    return DATA_NAME[(int) value % DATA_NAME.length];
                }
            });

            xAxis.setTextColor(Color.BLACK);

            YAxis yAxis = chart.getYAxis();
            yAxis.setLabelCount(2, false);
            yAxis.setTextSize(9f);
            yAxis.setAxisMinimum(0f);
            yAxis.setAxisMaximum(80f);
            yAxis.setDrawLabels(false);

            Legend l = chart.getLegend();
            l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
            l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
            l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
            l.setDrawInside(false);
            l.setXEntrySpace(7f);
            l.setYEntrySpace(5f);
            l.setTextColor(Color.BLACK);
            l.setEnabled(false);

            RadarDataSet set1 = new RadarDataSet(entries1, nama_siswa);
            set1.setColor(Color.rgb(57, 172, 152));
            set1.setFillColor(Color.rgb(42, 217, 186));
            set1.setDrawFilled(true);
            set1.setFillAlpha(180);
            set1.setLineWidth(2f);
            set1.setDrawHighlightCircleEnabled(true);
            set1.setDrawHighlightIndicators(false);

            ArrayList<IRadarDataSet> sets = new ArrayList<>();
            sets.add(set1);

            RadarData data = new RadarData(sets);
            data.setValueTextSize(8f);
            data.setDrawValues(false);
            data.setValueTextColor(Color.BLACK);

            chart.setData(data);
            chart.setRotationEnabled(false);

            for (IDataSet<?> set : chart.getData().getDataSets()){
                set.setDrawValues(!set.isDrawValuesEnabled());
            }

            chart.invalidate();
        }
        else {
            chart.setVisibility(View.GONE);
        }

        /*float mul = 80;
        float min = 20;


        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < cnt; i++) {
            float val1 = (float) (Math.random() * mul) + min;
            entries1.add(new RadarEntry(val1));

            float val2 = (float) (Math.random() * mul) + min;
            entries2.add(new RadarEntry(val2));
        }

        RadarDataSet set2 = new RadarDataSet(entries2, "Muhammad Raihan");
        set2.setColor(Color.rgb(238, 123, 191));
        set2.setFillColor(Color.rgb(230, 148, 191));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);


        sets.add(set2);*/
    }

    public void refreshTahunAkademik(List<TahunAkademik> tahun){
        if( tahun != null && tahun.size() > 0 ){
            List<StringWithTag> itemList = new ArrayList<StringWithTag>();
            for(TahunAkademik tahunakademik:tahun){
                itemList.add(new StringWithTag(tahunakademik.tahunakademik_namatahun, tahunakademik.tahunakademik_namatahun));
            }

            ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, itemList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tahunAjaran.setAdapter(dataAdapter);
        }
    }

    class LoadTahunAkademik extends AsyncTask<Void, Void, List<TahunAkademik>>{

        String msgError     = null;
        Boolean ConnFail    = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<TahunAkademik> doInBackground(Void... voids) {
            ApiStudo apiStudo = new ApiStudo();
            GetListTahunAkademikResponse response = apiStudo.getTahunAkademik();
            if( response == null || response.rc == 0 ){
                if( response == null ){
                    msgError    = getString(R.string.error_gkjelas);
                    ConnFail    = apiStudo.getConnFail();
                }
                else if( response.rc == 0 ){
                    msgError    = getString(R.string.error_kesalahan);
                    ConnFail    = true;
                }
            }
            else {
                return response.data;
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<TahunAkademik> tahunAkademiks) {
            super.onPostExecute(tahunAkademiks);

            if( tahunAkademiks != null && tahunAkademiks.size() > 0 ){
                refreshTahunAkademik(tahunAkademiks);
            }
            else
            {
                if( !StringComponent.isNull(msgError) ){
                    infoError
                            .setTitle(getString(R.string.title_mohon_maaf))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {
                                    if( ConnFail ){
                                        finish();
                                    }
                                }
                            })
                            .show();
                }
            }
        }
    }

    class LoadNilaiSementara extends AsyncTask<String, Void, List<NilaiSementara>>{

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingBar.setVisibility(View.VISIBLE);
            scroll_view.setVisibility(View.GONE);
            resultTxt.setVisibility(View.GONE);
            filterLayout.setVisibility(View.GONE);
        }

        @Override
        protected List<NilaiSementara> doInBackground(String... strings) {
            String nis                              = strings[0];
            String semester                         = strings[1];
            String tahun_ajaran                     = strings[2];
            List<NilaiSementara> nilaiSementaras    = new ArrayList<NilaiSementara>();

            ApiStudo apiStudo                       = new ApiStudo();
            List<NilaiSementara> dataNilai          = apiStudo.getNilaiSementara(nis, semester, tahun_ajaran);
            if( dataNilai != null && dataNilai.size() > 0 ){
                int i = 0;
                for( NilaiSementara nilaiSementara:dataNilai ){
                    nilaiSementara.setNomor(i+1);
                    nilaiSementara.setMatapelajaran(dataNilai.get(i).matapelajaran);
                    nilaiSementara.setNilai(dataNilai.get(i).nilai);
                    nilaiSementara.setId_matpel(dataNilai.get(i).id_matpel);
                    nilaiSementara.setNis(nis);
                    nilaiSementara.setNama(nama_siswa);
                    nilaiSementara.setSemester(semester);
                    nilaiSementara.setAjaran(tahun_ajaran);
                    nilaiSementaras.add(nilaiSementara);
                    i++;
                }

                return nilaiSementaras;
            }
            else {
                msgError    = apiStudo.getMsgError();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<NilaiSementara> nilaiSementaras) {
            super.onPostExecute(nilaiSementaras);

            if( nilaiSementaras != null && nilaiSementaras.size() > 0 ){
                loadingBar.setVisibility(View.GONE);
                scroll_view.setVisibility(View.VISIBLE);
                filterLayout.setVisibility(View.VISIBLE);
                listMataPelajaranAdapter.refreshAdapter(nilaiSementaras);
                setData(nilaiSementaras);
            }
            else {
                if( !StringComponent.isNull(msgError) ){
                    Config.getInstance().addLogs("msg error get nilai sementara ==> " + msgError);
                }
                else{
                    loadingBar.setVisibility(View.GONE);
                    scroll_view.setVisibility(View.GONE);
                    filterLayout.setVisibility(View.GONE);
                    resultTxt.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public static class StringWithTag {
        public String string;
        public Object tag;

        public StringWithTag(String string, Object tag) {
            this.string = string;
            this.tag = tag;
        }

        @Override
        public String toString() {
            return string;
        }
    }
}
