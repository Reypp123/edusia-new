package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.component.ViewAnimationComponent;

public class TopupInfoActivity extends AppCompatActivity {

    private ImageButton atmToggleBtn, jakToggleBtn, otherToggleBtn;
    private View atmExpandLayout, jakExpandLayout, otherExpandLayout;
    private NestedScrollView scrollView;

    public static Intent createIntent(Context context){
        return new Intent(context, TopupInfoActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_topup_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        initComponent();
    }

    private void initComponent() {
        // nested scrollview
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);

        atmToggleBtn = (ImageButton) findViewById(R.id.atmToggleBtn);
        atmExpandLayout = (View) findViewById(R.id.atmExpandLayout);
        atmToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, atmExpandLayout);
            }
        });

        jakToggleBtn = (ImageButton) findViewById(R.id.jakToggleBtn);
        jakExpandLayout = (View) findViewById(R.id.jakExpandLayout);
        jakToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, jakExpandLayout);
            }
        });

        otherToggleBtn = (ImageButton) findViewById(R.id.otherToggleBtn);
        otherExpandLayout = (View) findViewById(R.id.otherExpandLayout);
        otherToggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleSection(view, otherExpandLayout);
            }
        });

    }


    private void toggleSection(View bt, final View lyt) {
        boolean show = toggleArrow(bt);
        if (show) {
            ViewAnimationComponent.expand(lyt, new ViewAnimationComponent.AnimListener() {
                @Override
                public void onFinish() {
                    scrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollView.scrollTo(500, lyt.getBottom());
                        }
                    });
                }
            });
        } else {
            ViewAnimationComponent.collapse(lyt);
        }
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }
}
