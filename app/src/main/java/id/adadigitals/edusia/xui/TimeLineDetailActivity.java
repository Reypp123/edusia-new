package id.adadigitals.edusia.xui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.ImageComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.data.TimeLine;
import id.adadigitals.edusia.response.json.GetDetailMuridResponse;
import id.adadigitals.edusia.response.json.GetTimeLineResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;

public class TimeLineDetailActivity extends AppCompatActivity {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    LinearLayout rootLayout;
    LinearLayout loadingLayout;
    NestedScrollView scroll_view;
    LinearLayout layoutDataTimeLine;
    AwesomeErrorDialog infoError;
    AwesomeProgressDialog infoProcess;
    TextView resultTxt;
    TextView siswaNameToolbar;
    Student mSiswa;

    String nis              = "";
    String nama_siswa       = "";
    int mPage               = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_timeline_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            nis         = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
            nama_siswa  = getIntent().getStringExtra(Config.KEY_PARAM_NAMA_SISWA);
        }
        else {
            finish();
        }

        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, TimeLineDetailActivity.class);
        return intent;
    }

    private void declare(){
        rootLayout          = (LinearLayout) findViewById(R.id.rootLayout);
        loadingLayout       =  (LinearLayout) findViewById(R.id.loadingLayout);
        scroll_view         = (NestedScrollView) findViewById(R.id.scroll_view);
        resultTxt           = (TextView) findViewById(R.id.resultTxt);
        siswaNameToolbar    = (TextView) findViewById(R.id.siswaNameToolbar);
        layoutDataTimeLine  = (LinearLayout) findViewById(R.id.layoutDataTimeLine);
        infoError           = new AwesomeErrorDialog(TimeLineDetailActivity.this);
        infoProcess         = new AwesomeProgressDialog(TimeLineDetailActivity.this);
    }

    private void run(){
        if( !StringComponent.isNull(nama_siswa) ){
            siswaNameToolbar.setText(nama_siswa);
        }

        if( !StringComponent.isNull(nis) ){
            LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
            loadDataAsyncTask.execute(nis);
        }
    }

    private View createTimeLineView(ViewGroup rootView) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View timeLineItem       = inflater.inflate(R.layout.expandable_card_timeline, rootView, false);
        return timeLineItem;
    }

    public class LoadDataAsyncTask extends AsyncTask<String, Void, List<TimeLine>>{

        String msgError         = null;
        int start               = mPage * Config.DEFAULT_LIMIT;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            scroll_view.setVisibility(View.GONE);
            resultTxt.setVisibility(View.GONE);
        }

        @Override
        protected List<TimeLine> doInBackground(String... strings) {
            String nis                              = strings[0];
            ApiStudo apiStudo                       = new ApiStudo();
            GetDetailMuridResponse getMuridByNis    = apiStudo.getDetailMurid(nis);

            if( getMuridByNis.data != null && getMuridByNis.rc == 1 ){
                Student siswa                               = getMuridByNis.data;
                siswa.setSiswaNama(siswa.getNama());
                siswa.setFoto(siswa.getFoto());
                mSiswa = siswa;
                GetTimeLineResponse getTimeLineResponse     = apiStudo.getTimeLine(siswa.getNIS(), start, Config.DEFAULT_LIMIT);
                if( getTimeLineResponse != null ){
                    if( getTimeLineResponse.rc == 1 ){
                        if( getTimeLineResponse.data.size() <= 0 ){
                            msgError = getString(R.string.text_data_timeline_notFound);
                        }
                        else {
                            return getTimeLineResponse.data;
                        }
                    }
                    else {
                        msgError = getTimeLineResponse.msg;
                    }
                }
                else {
                    msgError = apiStudo.getMsgError();
                }
            }
            else {
                msgError = apiStudo.getMsgError();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<TimeLine> timeLines) {
            super.onPostExecute(timeLines);
            loadingLayout.setVisibility(View.GONE);
            if( timeLines != null && timeLines.size() > 0 ){
                scroll_view.setVisibility(View.VISIBLE);
                refreshData(timeLines);
            }
            else {
                resultTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    private void refreshData(List<TimeLine> timeLine){
        for(int i=0; i< timeLine.size(); i++){
            final CardView timelineCard = (CardView) createTimeLineView(layoutDataTimeLine);
            timelineCard.setTag(timeLine.get(i));

            TextView tvKeterangan = (TextView) timelineCard.findViewById(R.id.tvKeterangan);
            tvKeterangan.setText(timeLine.get(i).msg_timeline);

            TextView tvTanggal    = (TextView) timelineCard.findViewById(R.id.tvTanggal);
            tvTanggal.setText(convertTimeAgo(timeLine.get(i).cdate));

            if( !StringComponent.isNull(mSiswa.getNama()) ){
                TextView tvNameSiswa = (TextView) timelineCard.findViewById(R.id.tvNameSiswa);
                tvNameSiswa.setText(mSiswa.getNama());
            }

            if( !StringComponent.isNull(mSiswa.getSiswaFotoUrl()) ){
                final ImageView imgMuridPhoto = (ImageView) timelineCard.findViewById(R.id.imgSiswa);
                ImageComponent.displayImageRound(getApplicationContext(), imgMuridPhoto, mSiswa.getSiswaFotoUrl());
            }

            layoutDataTimeLine.addView(timelineCard);
        }
    }

    private String convertTimeAgo(String date){
        try{
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date past = format.parse(date);
            long time = past.getTime();

            if (time < 1000000000000L) {
                time *= 1000;
            }

            long now = System.currentTimeMillis();
            if (time > now || time <= 0) {
                return null;
            }


            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 50 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + " minutes ago";
            } else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + " hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return diff / DAY_MILLIS + " days ago";
            }
        }
        catch (Exception e){
            e.printStackTrace();
            Config.getInstance().addLogs("Error convertTimeAgo --> " + e.getMessage());
            return null;
        }
    }

}
