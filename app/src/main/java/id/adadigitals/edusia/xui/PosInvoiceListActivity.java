package id.adadigitals.edusia.xui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.adadigitals.edusia.adapter.PosInvoiceAdapter;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.response.json.GetPosInvoiceResponse;
import id.adadigitals.edusia.widget.LineItemDecoration;

import id.adadigitals.edusia.api.ApiStudo;

public class PosInvoiceListActivity extends AppCompatActivity  {
    RecyclerView posInvoiceList;
    PosInvoiceAdapter posInvoiceAdapter;

    RelativeLayout progressLayout;

    LinearLayout infoLayout;
    TextView infoTxt;
    AppCompatButton reloadBtn;

    String mNis = null;

    int mPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos_invoice_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        if (getIntent().getExtras()!=null){
            mNis = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
        }else
            finish();
        setupUserInterface();
    }

    public void setupUserInterface(){
        infoLayout = (LinearLayout) findViewById(R.id.infoLayout);
        infoTxt = (TextView) findViewById(R.id.infoTxt);

        posInvoiceList = (RecyclerView) findViewById(R.id.posInvoiceList);
        progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        posInvoiceList.setLayoutManager(linearLayoutManager);
        posInvoiceList.addItemDecoration(new LineItemDecoration(this, LinearLayout.VERTICAL));
        posInvoiceAdapter = new PosInvoiceAdapter();
        posInvoiceList.setAdapter(posInvoiceAdapter);

        reloadBtn = (AppCompatButton) findViewById(R.id.reloadBtn);
        reloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LoadSavingsTask loadSavingsTask = new LoadSavingsTask();
        loadSavingsTask.execute(mNis);
    }

    public class LoadSavingsTask extends AsyncTask<String, Void, GetPosInvoiceResponse> {

        String mLastErrorMsg;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoLayout.setVisibility(View.GONE);
            posInvoiceList.setVisibility(View.GONE);
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected GetPosInvoiceResponse doInBackground(String... strings) {
            String nis = strings[0];
            ApiStudo webApi = new ApiStudo();
            int start = mPage * Config.DEFAULT_LIMIT;
            GetPosInvoiceResponse getPosInvoiceResponse = webApi.getPosInvoice(nis, start, Config.DEFAULT_LIMIT);
            if(getPosInvoiceResponse!=null && getPosInvoiceResponse.rc == 1){

            }else if(getPosInvoiceResponse == null){
                mLastErrorMsg = getString(R.string.error_network_connection);
            }else if(getPosInvoiceResponse.rc != 1){
                mLastErrorMsg = getPosInvoiceResponse.msg;
            }
            return getPosInvoiceResponse;
        }

        @Override
        protected void onPostExecute(GetPosInvoiceResponse getPosInvoiceResponse) {
            super.onPostExecute(getPosInvoiceResponse);
            progressLayout.setVisibility(View.GONE);

            if(getPosInvoiceResponse !=null){
                if(getPosInvoiceResponse.rc==1){
                    if(getPosInvoiceResponse.data!=null && getPosInvoiceResponse.data.size() > 0){
                        mPage++;
                        posInvoiceAdapter.addData(getPosInvoiceResponse.data);
                        infoLayout.setVisibility(View.GONE);
                        posInvoiceList.setVisibility(View.VISIBLE);
                    }else{
                        infoLayout.setVisibility(View.VISIBLE);
                        posInvoiceList.setVisibility(View.GONE);
                        infoTxt.setText(getString(R.string.desc_invoices_not_available));
                        reloadBtn.setVisibility(View.GONE);
                    }
                }else if(getPosInvoiceResponse.rc !=1) {
                    if(StringComponent.isNull(mLastErrorMsg))
                        mLastErrorMsg = Config.getInstance().getString(R.string.error_gkjelas);
                    infoLayout.setVisibility(View.VISIBLE);
                    posInvoiceList.setVisibility(View.GONE);
                }
            }else{
                if(StringComponent.isNull(mLastErrorMsg))
                    mLastErrorMsg = Config.getInstance().getString(R.string.error_gkjelas);
                infoLayout.setVisibility(View.VISIBLE);
                posInvoiceList.setVisibility(View.GONE);
            }
        }
    }
}
