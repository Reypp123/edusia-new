package id.adadigitals.edusia.xui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.BankDKI;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.WalletPinBankDKI;
import id.adadigitals.edusia.data.wallet.request.AccountConfirmVerificationPinRequest;
import id.adadigitals.edusia.data.wallet.response.AccountConfirmVerificationPinResponse;
import id.adadigitals.edusia.data.wallet.response.FaultError;
import id.adadigitals.edusia.service.VerifikasiNewPinService;
import id.adadigitals.edusia.view.PinEntryEditText;

public class OtpVerificationNewPin extends AppCompatActivity {

    LinearLayout rootLayout;

    RelativeLayout timerLayout;

    CircularProgressBar timerBar;

    TextView timerTxt;
    TextView resendTxt;

    PinEntryEditText pinOtpCodeEdTxt;

    Button cancelBtn;
    Button nextBtn;

    AwesomeProgressDialog infoProcess;
    AwesomeErrorDialog infoError;

    Handler mHandler;
    TextWatcher textWatcher;
    Intent verificationNewPinService;

    WalletPinBankDKI mWalletPin;
    String reference;

    VerificationReceiver mReceiver;

    boolean timerRun = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_show_otp_newpin);
        reference = getIntent().getStringExtra(LupaPinActivity.PARAMS_ID_REFERENCE);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        declare();
        run();
        startService(verificationNewPinService);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if( mReceiver == null ){
            mReceiver = new VerificationReceiver();
            mReceiver.register();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if( mReceiver != null ){
            mReceiver.unregister();
        }
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, OtpVerificationNewPin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private void declare(){
        verificationNewPinService   = new Intent(getApplicationContext(), VerifikasiNewPinService.class);
        rootLayout                  = (LinearLayout) findViewById(R.id.rootLayout);
        timerLayout                 = (RelativeLayout) findViewById(R.id.timerLayout);
        timerBar                    = (CircularProgressBar) findViewById(R.id.timerBar);

        timerTxt                    = (TextView) findViewById(R.id.timerTxt);
        resendTxt                   = (TextView) findViewById(R.id.resendTxt);

        pinOtpCodeEdTxt             = (PinEntryEditText) findViewById(R.id.pinOtpCodeEdTxt);

        cancelBtn                   = (Button) findViewById(R.id.cancelBtn);
        nextBtn                     = (Button) findViewById(R.id.nextBtn);

        infoProcess                 = new AwesomeProgressDialog(this);
        infoError                   = new AwesomeErrorDialog(this);

        textWatcher                 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if( s.length() == 6 ){
                    prepareTaskExecute();
                    nextBtn.setEnabled(true);
                }else{
                    nextBtn.setEnabled(false);
                }
            }
        };
    }

    private void run(){
        pinOtpCodeEdTxt.addTextChangedListener(textWatcher);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareTaskExecute();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SetupWallet.createIntent(getApplicationContext()));
                finish();
            }
        });

        resendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendTxt.setVisibility(View.GONE);
            }
        });
    }

    private void prepareTaskExecute(){
        String otp = pinOtpCodeEdTxt.getText().toString();
        KonfirmasiHasilVerifikasiTask konfirmasiHasilVerifikasiTask = new KonfirmasiHasilVerifikasiTask();
        konfirmasiHasilVerifikasiTask.execute(otp);
    }

    private void runKonfirmasiHasilVerifikasi(){
        Intent walletResetPin = SetupWallet.createIntent(getApplicationContext());
        walletResetPin.putExtra(SetupWallet.PARAMS_WALLET_MODE, SetupWallet.MODE_RESET_PIN);
        startActivity(walletResetPin);
        finish();
    }

    public class VerificationReceiver extends BroadcastReceiver {

        public void register(){
            IntentFilter filters = new IntentFilter();
            filters.addAction(Config.INTENT_VERIFICATION_TIMER);
            filters.addAction(Config.INTENT_VERIFICATION_SUCCESS);
            filters.addAction(Config.INTENT_VERIFICATION_FAILED);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_IN_PROGRESS);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_SENT);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_UNSENT);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_RECEIVED);
            try {
                registerReceiver(this, filters);
            } catch (Exception e) {
                Config.getInstance().addLogs("error register verification receiver --> " + e.getMessage());
            }
        }

        public void unregister(){
            try {
                unregisterReceiver(this);
            }catch (Exception e) {
                Config.getInstance().addLogs("error unregister verification receiver --> " + e.getMessage());
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Config.INTENT_VERIFICATION_TIMER)) {
                timerRun = true;
                int remaining = intent.getIntExtra("TIMER",0);
                boolean start = intent.getBooleanExtra("START",false);
                if( timerBar.getProgress() == 0 ){
                    timerBar.setProgressWithAnimation(remaining,900);
                }
                else {
                    timerBar.setProgress(remaining);
                }

                int minutes = remaining / 60;
                int seconds = remaining % 60;
                timerTxt.setText(String.format("%02d:%02d", minutes, seconds));
                if(remaining < 60 && resendTxt.getVisibility() == View.GONE){
                    resendTxt.setVisibility(View.VISIBLE);
                }

                if( remaining == 0 ){
                    timerRun = false;
                }
            }
            else if(intent.getAction().equals(Config.INTENT_VERIFICATION_FAILED)){
                timerRun = false;
                timerTxt.setTextColor(ContextCompat.getColor(OtpVerificationNewPin.this, R.color.red_500));
            }
        }
    }

    public class KonfirmasiHasilVerifikasiTask extends AsyncTask<String, Void, String> {

        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            stopService(verificationNewPinService);
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(getString(R.string.text_section_verification))
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String codeOtp = strings[0];

            if( codeOtp != null && reference != null ){
                AccountConfirmVerificationPinRequest accountConfirmVerificationPinRequest = new AccountConfirmVerificationPinRequest();
                accountConfirmVerificationPinRequest.setOtp(codeOtp);
                accountConfirmVerificationPinRequest.setReference(reference);
                BankDKI apiBankDKI = new BankDKI();
                AccountConfirmVerificationPinResponse accountConfirmVerificationPinResponse = apiBankDKI.accVerificationConfirmPin(accountConfirmVerificationPinRequest);
                if (accountConfirmVerificationPinResponse == null) {
                    msgError = apiBankDKI.getMsgError();
                } else if (accountConfirmVerificationPinResponse != null && accountConfirmVerificationPinResponse.responseStatus() == 0) {
                    if (accountConfirmVerificationPinResponse.getError().getCode() == FaultError.BAD_CREDENTIAL) {
                        msgError = getString(R.string.error_wallet_bad_credentials);
                    } else {
                        msgError = accountConfirmVerificationPinResponse.getError().getFaultMessage();
                    }
                } else if (accountConfirmVerificationPinResponse != null && accountConfirmVerificationPinResponse.responseStatus() == 1) {
                    return reference;
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String reference) {
            super.onPostExecute(reference);

            if( reference != null ){
                if( infoProcess != null ){
                    infoProcess.hide();
                }

                runKonfirmasiHasilVerifikasi();
            }
            else {
                if (StringComponent.isNull(msgError)) {
                    msgError = getString(R.string.error_gkjelas);
                }

                if( !StringComponent.isNull(msgError) ){

                    if( infoProcess != null ){
                        infoProcess.hide();
                    }

                    infoError
                            .setTitle(getString(R.string.verification_failed))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
        }
    }
}
