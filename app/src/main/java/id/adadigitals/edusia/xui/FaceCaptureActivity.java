package id.adadigitals.edusia.xui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.FileComponent;
import id.adadigitals.edusia.component.ImageComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.component.ViewAnimationComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.api.ApiStudo;

public class FaceCaptureActivity extends AppCompatActivity {
    private static final int REQUEST_FACE_CAPTURE = 0;

    private Toolbar toolbar;
    private LinearLayout rootLayout, loadingLayout, stepperLayout, OrtuStepper, ContentLayout, studentStepper;
    private NestedScrollView faceSetupLayout;
    private Button btnContinue;
    private LayoutInflater inflater;
    private TextView parentTitleTxt;
    private TextView studentTitleTxt;
    private TextView stepTxt;
    private AwesomeProgressDialog infoProcess;
    private AwesomeErrorDialog infoError;

    private String mUid = null;
    private int mFaceGroup = -1;
    private int mFaceIndex = -1;

    private List<View> LayoutContents = new ArrayList<>();
    private List<RelativeLayout> stepContent = new ArrayList<>();
    private int success_step = 0;
    private int current_step = -1;
    private int enrollment_required = 0;

    boolean isWaliFather = false;
    boolean isWaliIbu    = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_face_setup);
        initToolbar();
        declare();
        run();
    }

    public static Intent createIntent(Context context){
        return new Intent(context, FaceCaptureActivity.class);
    }

    private void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void declare(){
        rootLayout      = (LinearLayout) findViewById(R.id.rootLayout);
        loadingLayout   = (LinearLayout) findViewById(R.id.loadingLayout);
        faceSetupLayout = (NestedScrollView) findViewById(R.id.faceSetupLayout);
        stepperLayout   = (LinearLayout) findViewById(R.id.stepperLayout);
        btnContinue     = (Button) findViewById(R.id.btnContinue);
        infoProcess     = new AwesomeProgressDialog(this);
        infoError       = new AwesomeErrorDialog(this);
    }

    private void run(){
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.getInstance().setOnboardingStatus(Config.ONBOARDING_SETUP_WALLET);
                Intent walletSetup = SetupWallet.createIntent(FaceCaptureActivity.this);
                walletSetup.putExtra(SetupWallet.PARAMS_WALLET_MODE, SetupWallet.MODE_NEW_PIN);
                startActivity(walletSetup);
                finish();
            }
        });

        LoadDataAsyncTask loadData = new LoadDataAsyncTask();
        loadData.execute();
    }

    private void loadRefreshData(List<Student> students){
        boolean collapse = false;
        boolean visible = false;
        final Student firstStudents = students.get(0);
        isWaliFather                = firstStudents.isWaliAyah(Config.getInstance().getSessionPhone());
        isWaliIbu                   = firstStudents.isWaliIbu(Config.getInstance().getSessionPhone());
        OrtuStepper                 = (LinearLayout) createViewStep(stepperLayout);
        OrtuStepper.findViewById(R.id.contentLayout).setTag(firstStudents);
        LayoutContents.add(OrtuStepper.findViewById(R.id.contentLayout));
        stepContent.add((RelativeLayout) OrtuStepper.findViewById(R.id.stepLayout));
        ((TextView) OrtuStepper.findViewById(R.id.stepTxt)).setText("1");
        parentTitleTxt = (TextView) OrtuStepper.findViewById(R.id.tvTitle);

        if( isWaliFather ){
            if( firstStudents.isParentTemplateExists(firstStudents.getAyahNIK()) ){
                ImageComponent.displayImageOriginal(FaceCaptureActivity.this, ((ImageView) OrtuStepper.findViewById(R.id.photoImg)), firstStudents.getAyahFotoUrl() );
                OrtuStepper.findViewById(R.id.photoImg).setVisibility(View.VISIBLE);
                ((Button) OrtuStepper.findViewById(R.id.btnTake)).setText(getString(R.string.label_btn_ulangi_take_face));
                collapse = true;
            }

            parentTitleTxt.setText(StringComponent.upperCase(firstStudents.getAyahNama()));
        }
        else if( isWaliIbu ){
            if( firstStudents.isParentTemplateExists(firstStudents.getIbuNIK())){
                ImageComponent.displayImageOriginal(FaceCaptureActivity.this, ((ImageView) OrtuStepper.findViewById(R.id.photoImg)), firstStudents.getIbuFotoUrl() );
                OrtuStepper.findViewById(R.id.photoImg).setVisibility(View.VISIBLE);
                ((Button) OrtuStepper.findViewById(R.id.btnTake)).setText(getString(R.string.label_btn_ulangi_take_face));
                collapse = true;
            }

            parentTitleTxt.setText(StringComponent.upperCase(firstStudents.getIbuNama()));
        }

        parentTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recollapse(0);
            }
        });

        ((Button) OrtuStepper.findViewById(R.id.btnTake)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.getInstance().addLogs("get session NIK " + Config.getInstance().getSessionNIK());
                showFaceCaptureActivity(Config.getInstance().getSessionNIK(), Config.ORTU_FACE_GROUP, 0);
            }
        });



        stepperLayout.addView(OrtuStepper);
        enrollment_required++;
        if(collapse){
            ViewAnimationComponent.collapse(LayoutContents.get(0));
            setCheckedStep(0);
            success_step++;
        } else{
            current_step = 0;
            visible = true;
            OrtuStepper.setVisibility(View.VISIBLE);
        }

        for( int i = 0; i < students.size(); i++ ){
            collapse = false;
            final Student student = students.get(i);
            Config.getInstance().addLogs("student --> " + student.getNama());
            studentStepper = (LinearLayout) createViewStep(stepperLayout);
            studentStepper.findViewById(R.id.contentLayout).setTag(student);
            LayoutContents.add(studentStepper.findViewById(R.id.contentLayout));
            stepContent.add((RelativeLayout) studentStepper.findViewById(R.id.stepLayout));
            studentTitleTxt     = (TextView) studentStepper.findViewById(R.id.tvTitle);
            stepTxt             = (TextView) studentStepper.findViewById(R.id.stepTxt);
            stepTxt.setText(i+2+"");
            studentTitleTxt.setText(StringComponent.upperCase(student.getNama()));
            final int index = i+1;
            studentTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    recollapse(index);
                }
            });

            ((Button) studentStepper.findViewById(R.id.btnTake)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFaceCaptureActivity(student.getNIS(), Config.MURID_FACE_GROUP, index);
                }
            });

            if(student.isStudentTemplateExists()) {
                ImageComponent.displayImageOriginal(FaceCaptureActivity.this, ((ImageView)
                        studentStepper.findViewById(R.id.photoImg)), student.getStudentTemplateUrl());
                studentStepper.findViewById(R.id.photoImg).setVisibility(View.VISIBLE);
                ((Button)studentStepper.findViewById(R.id.btnTake)).setText(R.string.label_btn_ulangi_take_face);
                collapse = true;
            }

            stepperLayout.addView(studentStepper);
            enrollment_required++;
            if(collapse){
                ViewAnimationComponent.collapse(LayoutContents.get(index));
                setCheckedStep(index);
                success_step++;
            } else if(!visible){
                if(current_step == -1)
                    current_step = index;
                visible = true;
                OrtuStepper.setVisibility(View.VISIBLE);
            }
        }

        if(success_step >= enrollment_required)
            btnContinue.setEnabled(true);
        else{
            LayoutContents.get(current_step).setVisibility(View.VISIBLE);
        }
    }

    private void collapseAndContinue(int index) {
        try{
            ViewAnimationComponent.collapse(LayoutContents.get(index));
            setCheckedStep(index);
            if( LayoutContents.size() > index ){
                index++;
                current_step = index;
                success_step = index > success_step ? index : success_step;
                ViewAnimationComponent.expand(LayoutContents.get(index));
            }
        }
        catch (Exception e){
            Config.getInstance().addLogs("Error Face Capture Activity Line 244 --> " + e.getMessage());
        }

        Config.getInstance().addLogs("index " + index);
        Config.getInstance().addLogs("layoutcontent jumlah " + LayoutContents.size());
        Config.getInstance().addLogs("success_step " + success_step);
        Config.getInstance().addLogs("enrollment_required " + enrollment_required);

        if(success_step >= enrollment_required){
            btnContinue.setEnabled(true);
        }

    }

    private void setCheckedStep(int index) {
        RelativeLayout relative = stepContent.get(index);
        relative.removeAllViews();
        ImageButton img = new ImageButton(this);
        img.setImageResource(R.drawable.ic_done);
        img.setBackgroundColor(Color.TRANSPARENT);
        img.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        relative.addView(img);
    }

    private void recollapse(int index){
        if (success_step >= index && current_step != index) {
            current_step = index;
            collapseAll();
            ViewAnimationComponent.expand(LayoutContents.get(index));
        }
    }

    private void collapseAll() {
        for (View v : LayoutContents) {
            ViewAnimationComponent.collapse(v);
        }
    }

    private View createViewStep(ViewGroup layout){
        inflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View studentItem = inflater.inflate(R.layout.expandable_step_face, layout, false);
        return studentItem;
    }

    public class LoadDataAsyncTask extends AsyncTask<Void, Void, List<Student>>{

        ApiStudo apistudo = new ApiStudo();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            faceSetupLayout.setVisibility(View.GONE);
        }

        @Override
        protected List<Student> doInBackground(Void... voids) {
            getListMuridResponse listMurid = apistudo.getDaftarMurid();
            List<Student> students = listMurid.data;
            return students;
        }

        @Override
        protected void onPostExecute(List<Student> students) {
            super.onPostExecute(students);
            if( students != null && students.size() > 0){
                loadRefreshData(students);
            }

            loadingLayout.setVisibility(View.GONE);
            faceSetupLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showFaceCaptureActivity(String uid, int faceGroup, int faceIndex){
        this.mUid = uid;
        this.mFaceGroup = faceGroup;
        this.mFaceIndex = faceIndex;

        Intent intent = null;
        if( Config.FACE_RECOGNITION ){
            //intent = new Intent(FaceCaptureActivity.this,FaceDetectorActivity.class);
        }
        else {
            intent = new Intent(FaceCaptureActivity.this, CameraActivity.class);
        }

        intent.putExtra(Config.KEY_PARAM_FACE_INDEX, faceIndex);
        intent.putExtra(Config.KEY_PARAM_FACE_GROUP, faceGroup);
        intent.putExtra(Config.KEY_PARAM_FACE_UID, uid);
        startActivityForResult(intent, REQUEST_FACE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case REQUEST_FACE_CAPTURE:
                switch (resultCode){
                    case Config.FACE_CAPTURE_CAMERA_INIT_FAILED:
                        Toast.makeText(this, R.string.toast_error_init_camera, Toast.LENGTH_SHORT).show();
                        break;
                    case Config.FACE_CAPTURE_NO_CAMERA_PERMISSION:
                        Toast.makeText(this, R.string.toast_permission_to_access_camera, Toast.LENGTH_SHORT).show();
                        break;
                    case Config.FACE_CAPTURE_SUCCESS:
                        int faceIndex = data.getIntExtra(Config.KEY_PARAM_FACE_INDEX,-1);
                        int faceGroup = data.getIntExtra(Config.KEY_PARAM_FACE_GROUP,-1);

                        Config.getInstance().addLogs("faceIndex ==> " + faceIndex);
                        Config.getInstance().addLogs("face group ==> " + faceGroup);

                        String faceUid = data.getStringExtra(Config.KEY_PARAM_FACE_UID);
                        Uri photoUri = data.getParcelableExtra(Config.KEY_PARAM_PHOTO_PATH);
                        Uri templateUri = data.getParcelableExtra(Config.KEY_PARAM_TEMPLATE_PATH);
                        Student student = (Student) LayoutContents.get(faceIndex).getTag();
                        if(faceGroup == Config.ORTU_FACE_GROUP) {

                            if(isWaliFather) {
                                student.setAyahFoto(FileComponent.getPhotoFile(faceGroup, faceUid).getName());
                            }else if(isWaliIbu){
                                student.setIbuFoto(FileComponent.getPhotoFile(faceGroup, faceUid).getName());
                            }

                            Config.getInstance().addLogs(" get template file --> " + FileComponent.getTemplateFile(faceGroup, faceUid).getName());
                            student.setParentFaceTemplate(FileComponent.getTemplateFile(faceGroup, faceUid).getName());

                        }else if(faceGroup == Config.MURID_FACE_GROUP) {
                            student.setFoto(FileComponent.getPhotoFile(faceGroup, faceUid).getName());
                            student.setFaceTemplate(FileComponent.getTemplateFile(faceGroup, faceUid).getName());
                        }

                        ((Button)LayoutContents.get(faceIndex).findViewById(R.id.btnTake)).setText(R.string.label_btn_ulangi_take_face);
                        LayoutContents.get(faceIndex).findViewById(R.id.photoImg).setVisibility(View.VISIBLE);
                        ImageComponent.displayImageOriginal(FaceCaptureActivity.this,
                                ((ImageView)LayoutContents.get(faceIndex).findViewById(R.id.photoImg)),
                                photoUri.toString());

                        saveDataTask saveData = new saveDataTask(faceGroup);
                        saveData.execute(student);

                        break;

                    case Config.FACE_CAPTURE_FAIL:
                        Config.getInstance().addLogs(" ============================== FACE_CAPTURE_FAIL");
                        //TODO HERE
                        break;
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

    }

    public class saveDataTask extends AsyncTask<Student, Void, BasicResponse>{
        String msgError = null;
        int saveData = Config.ORTU_FACE_GROUP;

        public saveDataTask(int saveData){
            this.saveData = saveData;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(getString(R.string.text_waiting_saveData))
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();

        }

        @Override
        protected BasicResponse doInBackground(Student... students) {
            Student student = students[0];
            BasicResponse basicResponse = null;
            ApiStudo apistudo = new ApiStudo();

            try{
                if( saveData == Config.ORTU_FACE_GROUP ){
                    if( isWaliFather ){
                        basicResponse = apistudo.updateFotoBapak(student.getAyahNIK(), student.getAyahPhotoFile());
                        if( basicResponse != null && basicResponse.rc == 1){
                            basicResponse = apistudo.updateOrtuTemplateFace(student.getAyahNIK(),student.getParentTemplateFile());
                        }
                    }
                    else if( isWaliIbu ){
                        basicResponse = apistudo.updateFotoIbu(student.getIbuNIK(), student.getIbuPhotoFile());
                        if( basicResponse != null && basicResponse.rc == 1 ){
                            basicResponse = apistudo.updateOrtuTemplateFace(student.getIbuNIK(),student.getParentTemplateFile());
                        }
                    }
                }
                else if( saveData == Config.MURID_FACE_GROUP ){
                    basicResponse = apistudo.updateFotoMurid(student.getNIS(), student.getStudentPhotoFile());
                    if( student.isStudentTemplateExists() ){
                        if( basicResponse != null && basicResponse.rc == 1 ){
                            basicResponse = apistudo.updateMuridTemplateFace(student.getNIS(), student.getStudentTemplateFile());
                        }
                    }
                    else {
                        Config.getInstance().addLogs("Gk ada template murid file");
                    }
                }

                if( basicResponse == null || basicResponse.rc == 0 ){
                    if( basicResponse == null ){
                        msgError = apistudo.getMsgError();
                    }
                    else if( basicResponse.rc == 0 ){
                        msgError = basicResponse.msg;
                    }

                    if( StringComponent.isNull(msgError) ){
                        msgError = getString(R.string.error_gkjelas);
                    }
                }

                Thread.sleep(500);
            }
            catch (Exception e){
                Config.getInstance().addLogs("Error Face Capture Activity Line 456 --> " + e.getMessage());
            }

            return basicResponse;
        }

        @Override
        protected void onPostExecute(BasicResponse basicResponse) {
            super.onPostExecute(basicResponse);

            if( infoProcess != null ){
                infoProcess.hide();
            }

            if( basicResponse != null ){
                if( basicResponse.rc == 1 ){
                    collapseAndContinue(mFaceIndex);
                }
                else {
                    Config.getInstance().addLogs("kesini --> 459");
                    infoError
                            .setTitle(getString(R.string.text_error))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
            else {
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(msgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {

                            }
                        })
                        .show();
            }
        }
    }
}
