package id.adadigitals.edusia.xui;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;

public class SplashActivity extends AppCompatActivity {

    private String _LOG = "__SPLASH__";
    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_splash);
        StylingComponent.setSystemBarColor(this, R.color.bg_splash);
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                splashDismiss();
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunnable, Config.SPLASH_DURATION);
    }

    private void splashDismiss(){
        checkMauKemana();
    }

    private void checkMauKemana(){
        showIntro();
        finish();
    }

    private void showIntro(){
        Intent intent = new Intent(SplashActivity.this, IntroActivity.class);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.an_i_show_in, R.anim.an_i_show_out);
    }
}
