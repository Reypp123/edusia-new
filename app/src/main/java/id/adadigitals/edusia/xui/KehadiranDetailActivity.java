package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;
import com.prolificinteractive.materialcalendarview.format.WeekDayFormatter;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.TextStyle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.CalendarKehadiranAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.KehadiranItem;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.response.json.GetDetailMuridResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;
import id.adadigitals.edusia.view.CalendarKehadiranDecorator;

public class KehadiranDetailActivity extends AppCompatActivity {

    CalendarKehadiranAdapter calendarKehadiranAdapter;
    AwesomeErrorDialog infoError;
    MaterialCalendarView calendarView;
    ProgressBar calendarLoadingBar;
    CalendarKehadiranTask calendarKehadiranTask;

    ProgressBar loadingBar;
    RecyclerView KehadiranView;
    TextView siswaNameToolbar;

    public int yearDisplay = 2018;
    public int monthDisplay = 1;

    HashMap<String, List<CalendarDay>> monthKehadiran;
    HashMap<String, List<KehadiranItem>> calendarKehadiran;

    int mPage = 0;
    int DEFAULT_LIMIT = 30;
    int def_hadir     = 0;
    int def_izin      = 0;
    int def_sakit     = 0;
    int def_absen     = 0;

    String nis              = "";
    String nama_siswa       = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_kehadiran_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
            getWindow().setNavigationBarColor(getResources().getColor(R.color.colorTextCustom));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);

        if (getIntent().getExtras()!=null){
            nis         = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
            nama_siswa  = getIntent().getStringExtra(Config.KEY_PARAM_NAMA_SISWA);
        }
        else {
            finish();
        }

        declare();
        run();
    }

    public static Intent createIntent(Context context){
        return new Intent(context,KehadiranDetailActivity.class);
    }

    private void declare(){
        siswaNameToolbar                        = (TextView) findViewById(R.id.siswaNameToolbar);
        monthKehadiran                          = new HashMap<String, List<CalendarDay>>();
        infoError                               = new AwesomeErrorDialog(KehadiranDetailActivity.this);
        loadingBar                              = (ProgressBar) findViewById(R.id.loadingBar);
        KehadiranView                           = (RecyclerView) findViewById(R.id.kehadiranView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        KehadiranView.setLayoutManager(linearLayoutManager);

        View calendarHeader = LayoutInflater.from(KehadiranView.getContext())
                .inflate(R.layout.list_calendar_header_kehadiran, KehadiranView, false);

        calendarView                            = (MaterialCalendarView) calendarHeader.findViewById(R.id.calendarView);
        calendarLoadingBar                      = (ProgressBar) calendarHeader.findViewById(R.id.calendarLoadingBar);

        calendarView.setWeekDayFormatter(new WeekDayFormatter() {
            @Override
            public CharSequence format(DayOfWeek dayOfWeek) {
                return dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault());
            }
        });
        calendarView.setTitleFormatter(new TitleFormatter() {
            @Override
            public CharSequence format(CalendarDay calendarDay) {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("LLLL yyyy", Locale.getDefault());
                return calendarDay.getDate().format(formatter);
            }
        });

        calendarView.setPagingEnabled(false);
        calendarView.setTopbarVisible(false);
        calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_NONE);

        calendarKehadiranAdapter = new CalendarKehadiranAdapter(calendarHeader);
        KehadiranView.setAdapter(calendarKehadiranAdapter);
    }

    private void run(){

        if( !StringComponent.isNull(nama_siswa) ){
            siswaNameToolbar.setText(nama_siswa);
        }

        if( !StringComponent.isNull(nis) ){
            yearDisplay = calendarView.getCurrentDate().getYear();
            monthDisplay = calendarView.getCurrentDate().getMonth();
            calendarKehadiranTask = new CalendarKehadiranTask();
            calendarKehadiranTask.execute(String.valueOf(monthDisplay),nis);
        }
    }

    public void refreshKehadiran(String month){
        List<KehadiranItem> kehadirans = calendarKehadiran.get(month);
        calendarView.removeDecorators();

        if( kehadirans != null && kehadirans.size() > 0){
            def_hadir = 0;
            def_izin  = 0;
            def_sakit = 0;
            def_absen = 0;

            for(KehadiranItem kehadiran:kehadirans){

                if( kehadiran.getStatus_kehadiran().equals("H")  ){
                    def_hadir++;
                }

                if( kehadiran.getStatus_kehadiran().equals("I") ){
                    def_izin++;
                }

                if( kehadiran.getStatus_kehadiran().equals("S") ){
                    def_sakit++;
                }

                if( kehadiran.getStatus_kehadiran().equals("A") ){
                    def_absen++;
                }
                if( monthKehadiran.get(kehadiran.getColor()) == null ){
                    monthKehadiran.put(kehadiran.getColor(), new ArrayList<CalendarDay>());
                }

                monthKehadiran.get(kehadiran.getColor()).add(kehadiran.date);
            }

            for (HashMap.Entry<String, List<CalendarDay>> entry : monthKehadiran.entrySet()){
                calendarView.addDecorator(new CalendarKehadiranDecorator(entry.getKey(), entry.getValue()));
            }
        }

        calendarKehadiranAdapter.setSumKehadiran(def_hadir,def_izin,def_sakit,def_absen);
        KehadiranView.invalidateItemDecorations();
    }

    public class CalendarKehadiranTask extends AsyncTask<String, Void, HashMap<String, List<KehadiranItem>>>{

        String msgError         = null;
        int start               = mPage * Config.DEFAULT_LIMIT;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if( calendarKehadiran != null ){
                calendarLoadingBar.setVisibility(View.VISIBLE);
                calendarView.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected HashMap<String, List<KehadiranItem>> doInBackground(String... strings) {
            String month = strings[0];
            String nis   = strings[1];
            HashMap<String, List<KehadiranItem>> sortedKehadiran = null;
            try{
                ApiStudo apiStudo                       = new ApiStudo();
                GetDetailMuridResponse getMuridByNis    = apiStudo.getDetailMurid(nis);
                if( getMuridByNis.data != null && getMuridByNis.rc == 1 ) {
                    Student siswa                               = getMuridByNis.data;
                    List<KehadiranItem> kehadiranItems          = apiStudo.getKehadiran(siswa.getNIS(), Integer.parseInt(month), start, DEFAULT_LIMIT);
                    if( kehadiranItems != null && kehadiranItems.size() > 0 ){
                        sortedKehadiran = new HashMap<String, List<KehadiranItem>>();
                        for (KehadiranItem kehadiran:kehadiranItems){
                            if( sortedKehadiran.get(kehadiran.bulan) == null ){
                                sortedKehadiran.put(kehadiran.bulan, new ArrayList<KehadiranItem>());
                            }

                            kehadiran.date = CalendarDay.from(Integer.parseInt(kehadiran.tahun), Integer.parseInt(kehadiran.bulan), Integer.parseInt(kehadiran.tgl));
                            sortedKehadiran.get(kehadiran.bulan).add(kehadiran);
                        }

                        return sortedKehadiran;
                    }
                }
                else {
                    msgError = apiStudo.getMsgError();
                }
            }
            catch (Exception e){
                Config.getInstance().addLogs("Error CalendarKehadiranTask --> " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(HashMap<String, List<KehadiranItem>> sortedKehadiran) {
            super.onPostExecute(sortedKehadiran);
            if( calendarKehadiran != null ){
                calendarLoadingBar.setVisibility(View.GONE);
                calendarView.setVisibility(View.VISIBLE);
            }

            if( calendarKehadiran == null ){
                calendarKehadiran = new HashMap<String, List<KehadiranItem>>();
            }

            calendarKehadiran.clear();
            if( sortedKehadiran != null ){
                calendarKehadiran.putAll(sortedKehadiran);
            }


            String monthStr = (monthDisplay < 10)? "0"+monthDisplay:monthDisplay+"";
            refreshKehadiran(monthStr);
            if(KehadiranView.getVisibility() == View.GONE){
                loadingBar.setVisibility(View.GONE);
                KehadiranView.setVisibility(View.VISIBLE);
            }
        }
    }
}
