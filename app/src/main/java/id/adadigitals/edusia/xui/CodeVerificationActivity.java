package id.adadigitals.edusia.xui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.dialog.OnDialogSelectionListener;
import id.adadigitals.edusia.dialog.SingleSelectionDialog;
import id.adadigitals.edusia.helper.PermissionRequester;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.service.VerifikasiUserService;
import id.adadigitals.edusia.view.PinEntryEditText;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

public class CodeVerificationActivity extends AppCompatActivity implements OnDialogSelectionListener {

    final public String TAG_DIALOG_SELECT_SIM = "DialogSelectSim";
    final public String TAG_DIALOG_FAILED_SENTSMS = "DialogFailedSentSms";

    final public int DIALOG_SELECT_SIMCARRIER = 0;
    final public int DIALOG_INFO_SENDSMS_FAILED = 1;
    final public int DIALOG_INFO_VCODE_INVALID = 2;
    final public int DIALOG_INFO_USER_VERIFICATION_FAILED = 2;
    final public int DIALOG_INFO_USER_VERIFICATION_MISMATCH = 3;
    final public int DIALOG_INFO_USER_VERIFICATION_FACE_NOT_VERIFIED = 4;
    final public int DIALOG_INFO_NO_STUDENTS = 5;

    LinearLayout rootLayout;
    Toolbar toolbar;
    RelativeLayout timerLayout;
    //CircularProgressIndicator timerBar;
    CircularProgressBar timerBar;

    TextView timerTxt;
    TextView resendTxt;
    PinEntryEditText pinOtpCodeEdTxt;

    Button cancelBtn;
    Button nextBtn;

    AwesomeProgressDialog infoProcess;
    AwesomeErrorDialog infoError;

    Handler mHandler;
    TextWatcher textWatcher;

    VerificationReceiver mReceiver;
    Intent verificationService;

    private String dialogTag = "";

    boolean showDialog = false;
    SingleSelectionDialog subsDialog;

    int selectedSimInfo = -1;
    List<SubscriberInfo> subscribers = new ArrayList<SubscriberInfo>();

    public class SubscriberInfo {
        int subsId;
        String carrierName;
    }

    boolean timerRun = false;

    Runnable mSendingChecker = new Runnable() {
        @Override
        public void run() {
            if (infoProcess != null) {
                infoProcess.hide();
                infoProcess = null;
                showFragmentDialog(DIALOG_INFO_SENDSMS_FAILED);
                stopService(verificationService);
            }
        }
    };

    Runnable mTimerChecker = new Runnable() {
        @Override
        public void run() {
            if( !timerRun && Config.getInstance().getVerificationStatus() != Config.VERIFICATION_SUCCESS ){
                timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.red_500));
                timerTxt.setText("00:00");
                timerTxt.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if( mReceiver == null ){
            mReceiver = new VerificationReceiver();
            mReceiver.register();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if( mReceiver != null ){
           unregisterReceiver(mReceiver);
           mReceiver = null;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_show_code_verification);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        declare();
        run();
        if( Config.FEATURE_SMS_OTP_BANKDKI ){
            startService(verificationService);
        }
        else {
            sendSms();
        }
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, CodeVerificationActivity.class);
        return intent;
    }

    private void declare(){
        mHandler            = new Handler();
        verificationService = new Intent(getApplicationContext(), VerifikasiUserService.class);
        rootLayout  = (LinearLayout) findViewById(R.id.rootLayout);
        toolbar     = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        timerLayout = (RelativeLayout) findViewById(R.id.timerLayout);
        //timerBar    = (CircularProgressIndicator) findViewById(R.id.timerBar);
        timerBar    = (CircularProgressBar) findViewById(R.id.timerBar);

        timerTxt    = (TextView) findViewById(R.id.timerTxt);
        resendTxt   = (TextView) findViewById(R.id.resendTxt);

        pinOtpCodeEdTxt     = (PinEntryEditText) findViewById(R.id.pinOtpCodeEdTxt);

        cancelBtn           = (Button) findViewById(R.id.cancelBtn);
        nextBtn             = (Button) findViewById(R.id.nextBtn);

        infoProcess         = new AwesomeProgressDialog(this);
        infoError           = new AwesomeErrorDialog(this);

        textWatcher         = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if( s.length() == 6 ){
                    runVerificationSMSGoToMain();
                    nextBtn.setEnabled(true);
                }else{
                    nextBtn.setEnabled(false);
                }
            }
        };
    }

    private void run(){
        pinOtpCodeEdTxt.addTextChangedListener(textWatcher);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(verificationService);
                Config.getInstance().resetUserVerifikasi();
                startActivity(UserVerificationActivity.createIntent(getApplicationContext()));
                finish();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runVerificationSMSGoToMain();
            }
        });

        resendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendTxt.setVisibility(View.GONE);
                if( Config.FEATURE_SMS_OTP_BANKDKI ){
                    if( verificationService != null ){
                        stopService(verificationService);
                    }

                    startService(verificationService);
                }
                else {
                    sendSms();
                }
            }
        });

        if( Config.getInstance().getVerificationStatus() == Config.VERIFICATION_FAILED ){
            timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.red_500));
            timerTxt.setText("00:00");
            resendTxt.setVisibility(View.VISIBLE);
        }
        else if( Config.getInstance().getVerificationStatus() == Config.VERIFICATION_SUCCESS &&
                 Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_VERIFIKASI_SMS_OTP ){
            timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.green_500));
            timerTxt.setText("DONE!");
            resendTxt.setVisibility(View.GONE);
            cancelBtn.setVisibility(View.GONE);
            String code = Config.getInstance().getPrefVerificationCode();
            displayCode(code);
            pinOtpCodeEdTxt.setEnabled(false);
            startMainActivity();
        }
        else {
            mHandler.postDelayed(mTimerChecker,2000);
        }
    }

    private void runVerificationSMSGoToMain(){
        String noHp = Config.getInstance().getSessionPhone();
        String code = pinOtpCodeEdTxt.getText().toString();

        if( Config.FEATURE_SMS_OTP_BANKDKI ){

            VerificationSMSApi verificationSMSApi = new VerificationSMSApi();
            verificationSMSApi.execute(noHp, code);
        }
        else{
            if (code.equals(Config.getInstance().getPrefVerificationCode())) {
                stopService(verificationService);
                mHandler.removeCallbacks(mSendingChecker);
                Config.getInstance().setOnboardingStatus(Config.ONBOARDING_FINISH);
                Config.getInstance().setVerificationStatus(Config.VERIFICATION_SUCCESS);
                startMainActivity();
            } else {
                showFragmentDialog(DIALOG_INFO_VCODE_INVALID);
            }
        }
    }

    public class VerificationSMSApi extends AsyncTask<String, Void, BasicResponse>{
        ApiStudo apiStudo = new ApiStudo();
        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(R.string.text_section_verification)
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
            stopService(verificationService);
            mHandler.removeCallbacks(mSendingChecker);
        }

        @Override
        protected BasicResponse doInBackground(String... strings) {
            String noHp = strings[0];
            String otp  = strings[1];

            BasicResponse basicResponse = apiStudo.checkOTP(noHp, otp);
            if( basicResponse == null || basicResponse.rc == 0 ){
                if( basicResponse == null ){
                    msgError = apiStudo.getMsgError();
                }
                else if( basicResponse.rc == 0 )
                {
                    msgError = basicResponse.msg;
                }
            }

            return basicResponse;
        }

        @Override
        protected void onPostExecute(BasicResponse response) {
            super.onPostExecute(response);

            if( infoProcess != null ){
                infoProcess.hide();
            }

            if( response != null ){
                if( response.rc == 1 ){
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_FINISH);
                    Config.getInstance().setVerificationStatus(Config.VERIFICATION_SUCCESS);
                    startMainActivity();
                }
                else {
                    Config.getInstance().setOnboardingStatus(Config.VERIFICATION_FAILED);
                    infoError
                            .setTitle(getString(R.string.text_error))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
            else {
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(msgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {

                            }
                        })
                        .show();
            }
        }
    }

    public class VerificationReceiver extends BroadcastReceiver{

        public void register(){
            IntentFilter filters = new IntentFilter();
            filters.addAction(Config.INTENT_VERIFICATION_TIMER);
            filters.addAction(Config.INTENT_VERIFICATION_SUCCESS);
            filters.addAction(Config.INTENT_VERIFICATION_FAILED);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_IN_PROGRESS);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_SENT);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_UNSENT);
            filters.addAction(Config.INTENT_VERIFICATION_SMS_RECEIVED);
            try {
                registerReceiver(this, filters);
            } catch (Exception e) {
                Config.getInstance().addLogs("error register verification receiver --> " + e.getMessage());
            }
        }

        public void unregister(){
            try {
                unregisterReceiver(this);
            }catch (Exception e) {
                Config.getInstance().addLogs("error unregister verification receiver --> " + e.getMessage());
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Config.INTENT_VERIFICATION_TIMER)) {
                timerRun = true;
                int remaining = intent.getIntExtra("TIMER",0);
                boolean start = intent.getBooleanExtra("START",false);
                if( timerBar.getProgress() == 0 ){
                    timerBar.setProgressWithAnimation(remaining,900);
                }
                else {
                    timerBar.setProgress(remaining);
                }

                int minutes = remaining / 60;
                int seconds = remaining % 60;
                timerTxt.setText(String.format("%02d:%02d", minutes, seconds));
                if(remaining < 60 && resendTxt.getVisibility() == View.GONE){
                    resendTxt.setVisibility(View.VISIBLE);
                }

                if( remaining == 0 ){
                    timerRun = false;
                }
            }
            else if(intent.getAction().equals(Config.INTENT_VERIFICATION_SUCCESS)){
                timerRun = false;
                timerBar.setColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.green_500));
                timerBar.setProgressWithAnimation(100);
                timerTxt.setText("DONE!");
                resendTxt.setVisibility(View.GONE);
                String code = intent.getStringExtra(Config.KEY_PARAM_VCODE);
                if(!StringComponent.isNull(code)){
                    displayCode(code);
                }

                startMainActivity();
            }
            else if(intent.getAction().equals(Config.INTENT_VERIFICATION_FAILED)){
                timerRun = false;
                timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.red_500));
            }
            else if( intent.getAction().equals(Config.INTENT_VERIFICATION_SMS_IN_PROGRESS) ){
                infoProcess.setTitle("")
                        .setMessage(R.string.text_waiting_send_sms)
                        .setColoredCircle(R.color.dialogInfoBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                        .setCancelable(false)
                        .show();

                mHandler.removeCallbacks(mSendingChecker);
                mHandler.postDelayed(mSendingChecker,Config.SMS_TIMEOUT);
            }
            else if( intent.getAction().equals(Config.INTENT_VERIFICATION_SMS_SENT) ){
                if( infoProcess != null ){
                    infoProcess.hide();
                }

                mHandler.removeCallbacks(mSendingChecker);
                resendTxt.setVisibility(View.GONE);
                timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.black));
            }
            else if(intent.getAction().equals(Config.INTENT_VERIFICATION_SMS_UNSENT)){
                timerRun = false;
                if(infoProcess !=null) {
                    infoProcess.hide();
                }

                mHandler.removeCallbacks(mSendingChecker);
                timerBar.setProgressWithAnimation(0);
                timerTxt.setTextColor(ContextCompat.getColor(CodeVerificationActivity.this, R.color.red_500));
                timerTxt.setText(String.format("00:00"));
                //TODO: FAILED, DO NOTHING
                showFragmentDialog(DIALOG_INFO_SENDSMS_FAILED);
            }
            else if(intent.getAction().equals(Config.INTENT_VERIFICATION_SMS_RECEIVED)){
                timerRun = false;
                String code = intent.getStringExtra(Config.KEY_PARAM_VCODE);
                displayCode(code);
            }
        }
    }

    private void showFragmentDialog(final int dialogType) {
        if (dialogType == DIALOG_SELECT_SIMCARRIER) {
            List<String> listItems = new ArrayList<>();
            for (SubscriberInfo simInfo : subscribers)
                listItems.add(simInfo.carrierName);

            try {
                subsDialog = SingleSelectionDialog.newInstance((OnDialogSelectionListener) this, DIALOG_SELECT_SIMCARRIER, getString(R.string.title_select_carrier), listItems.toArray(new String[listItems.size()]));
                subsDialog.setAllowReturnTransitionOverlap(false);
                subsDialog.setAllowEnterTransitionOverlap(false);
                subsDialog.setCancelable(false);
                dialogTag = TAG_DIALOG_SELECT_SIM;
                subsDialog.show(getSupportFragmentManager(), TAG_DIALOG_SELECT_SIM);
            } catch (IllegalStateException e) {
                showDialog = true;
                e.printStackTrace();
            }
        }
        else if (dialogType == DIALOG_INFO_SENDSMS_FAILED) {
            if (infoProcess != null) {
                infoProcess.hide();
            }

            infoError
                    .setTitle(getString(R.string.title_sendSms_failed))
                    .setMessage(getString(R.string.text_send_sms_failed))
                    .setColoredCircle(R.color.dialogErrorBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                    .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                    .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                    .setButtonText(getString(R.string.dialog_ok_button))
                    .setErrorButtonClick(new Closure() {
                        @Override
                        public void exec() {

                        }
                    })
                    .show();
        }
        else if( dialogType == DIALOG_INFO_VCODE_INVALID ){
            if (infoProcess != null) {
                infoProcess.hide();
            }

            infoError
                    .setTitle(getString(R.string.verification_failed))
                    .setMessage(getString(R.string.desc_verification_code_invalid))
                    .setColoredCircle(R.color.dialogErrorBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                    .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                    .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                    .setButtonText(getString(R.string.dialog_ok_button))
                    .setErrorButtonClick(new Closure() {
                        @Override
                        public void exec() {

                        }
                    })
                    .show();
        }
    }

    private void startMainActivity(){
        Intent BerandaAct = BerandaActivity.createIntent(getApplicationContext());
        startActivity(BerandaAct);
        finish();
    }

    public void displayCode(String code){
        pinOtpCodeEdTxt.setText(code);
    }

    private void sendSms() {
        subscribers.clear();
        if (Config.SDK_INT >= 22) {
            if (PermissionRequester.requestReadPhoneStatePermissionIfNeeded(this, Config.PERMISSIONS_REQUEST_READ_PHONE_STATE)) {
                SubscriptionManager subscriptionManager = SubscriptionManager.from(getApplicationContext());
                List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                for(SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
                    SubscriberInfo simInfo = new SubscriberInfo();
                    simInfo.subsId = subscriptionInfo.getSubscriptionId();
                    simInfo.carrierName = subscriptionInfo.getCarrierName().toString();
                    subscribers.add(simInfo);
                }

                if (subscribers.size() > 1){
                    showFragmentDialog(DIALOG_SELECT_SIMCARRIER);
                }
                else{
                    startSendSMSTask();
                }
            }
        }
        else {
            startSendSMSTask();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Config.PERMISSIONS_REQUEST_READ_PHONE_STATE:
                if (PermissionRequester.isPermissionGranted(grantResults)) {
                    sendSms();
                } else {
                    startSendSMSTask();
                }
                break;

            case Config.PERMISSIONS_REQUEST_SEND_SMS:
                if (PermissionRequester.isPermissionGranted(grantResults)) {
                    startSendSMSTask();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_permission_send_sms_denied), Toast.LENGTH_SHORT).show();
                }
                break;
            case Config.PERMISSIONS_REQUEST_RECEIVE_SMS:
                if (PermissionRequester.isPermissionGranted(grantResults)) {
                    startSendSMSTask();
                } else {
                    Toast.makeText(this, getString(R.string.toast_permission_receive_sms_denied), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onSelected(DialogFragment dialog, int which) {
        if (dialog instanceof SingleSelectionDialog) {
            final SingleSelectionDialog sdialog = (SingleSelectionDialog) dialog;
            if (sdialog.getDialogId() == DIALOG_SELECT_SIMCARRIER) {
                selectedSimInfo = which;
                startSendSMSTask();
            }
        }
    }

    private void startSendSMSTask(){
        if( PermissionRequester.requestSendSMSIfNeeded(this, Config.PERMISSIONS_REQUEST_SEND_SMS) &&
                PermissionRequester.requestReceiveSMSIfNeeded(this, Config.PERMISSIONS_REQUEST_RECEIVE_SMS)) {
            verificationService.putExtra(Config.PARAM_SELECT_SIM, selectedSimInfo);
            startService(verificationService);
        }
    }
}
