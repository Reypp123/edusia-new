package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeNoticeDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;

import java.util.Arrays;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.api.BankDKI;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;
import id.adadigitals.edusia.data.wallet.WalletPinBankDKI;
import id.adadigitals.edusia.data.wallet.request.AccountInformationRequest;
import id.adadigitals.edusia.data.wallet.request.CheckAccountRequest;
import id.adadigitals.edusia.data.wallet.request.RegisterAccountRequest;
import id.adadigitals.edusia.data.wallet.request.UbahPinRequest;
import id.adadigitals.edusia.data.wallet.response.AccountInformationResponse;
import id.adadigitals.edusia.data.wallet.response.CheckAccountResponse;
import id.adadigitals.edusia.data.wallet.response.FaultError;
import id.adadigitals.edusia.data.wallet.response.RegisterAccountResponse;
import id.adadigitals.edusia.data.wallet.response.UbahPinResponse;
import id.adadigitals.edusia.response.json.BasicResponse;
import id.adadigitals.edusia.response.json.getListMuridResponse;

public class SetupWallet extends AppCompatActivity {

    public static final String PARAMS_WALLET_MODE = "WalletPinParamsMode";

    private LinearLayout walletLayout;
    private LinearLayout layoutPin;
    private LinearLayout loadingLayout;
    private LinearLayout layoutInputPin;
    private LinearLayout layoutConfirmPin;
    private EditText pinEntryEdTxt;
    private EditText confirmPinEntryEdTxt;

    private Button nextBtn;

    public static int MODE_NEW_PIN = 0;
    public static int MODE_VERIFICATION = 2;
    public static int MODE_RESET_PIN = 3;

    int currMode = MODE_NEW_PIN;
    String currPin = "";

    WalletBankDKI mWallet;
    WalletPinBankDKI mWalletPin;

    boolean registerNew = false;
    boolean resetPin = false;
    int invalidPin = 0;

    TextView instructionsTxt;
    TextView btnLupaPin;
    TextView tvProgress;
    TextView tvToolbarTitle;

    private AwesomeProgressDialog infoProcess;
    private AwesomeErrorDialog infoError;
    private AwesomeNoticeDialog infoNotice;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, SetupWallet.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_setup_wallet_new);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        declare();
        run();
    }

    private void declare() {
        currMode             = getIntent().getIntExtra(PARAMS_WALLET_MODE, currMode);
        instructionsTxt      = (TextView) findViewById(R.id.instructionsTxt);
        walletLayout         = (LinearLayout) findViewById(R.id.walletLayout);
        loadingLayout        = (LinearLayout) findViewById(R.id.loadingLayout);
        layoutPin            = (LinearLayout) findViewById(R.id.layoutPin);
        layoutInputPin       = (LinearLayout) findViewById(R.id.layoutInputPin);
        layoutConfirmPin     = (LinearLayout) findViewById(R.id.layoutConfirmPin);
        infoProcess          = new AwesomeProgressDialog(this);
        infoError            = new AwesomeErrorDialog(this);
        infoNotice           = new AwesomeNoticeDialog(this);
        pinEntryEdTxt        = (EditText) findViewById(R.id.pinEntryEdTxt);
        confirmPinEntryEdTxt = (EditText) findViewById(R.id.confirmPinEntryEdTxt);
        nextBtn              = (Button) findViewById(R.id.nextBtn);
        btnLupaPin           = (TextView) findViewById(R.id.btnLupaPin);
        tvProgress          = (TextView) findViewById(R.id.tvProgress);
        tvToolbarTitle      = (TextView) findViewById(R.id.tvToolbarTitle);

        btnLupaPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLupaPinActivity();
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareDataWallet();
            }
        });
    }

    private void prepareDataWallet(){
        String errorInput     = null;
        final String pin      = pinEntryEdTxt.getText().toString();
        String confirm        = confirmPinEntryEdTxt.getText().toString();
        boolean cancel        = false;
        View focusView        = null;

        if(StringComponent.isNull(pin)){
            errorInput  = getString(R.string.error_required_pin);
            focusView   = pinEntryEdTxt;
            cancel      = true;
        }
        else if( (StringComponent.isNull(confirm) && currMode == MODE_NEW_PIN) || (StringComponent.isNull(confirm) && currMode == MODE_RESET_PIN) ){
            errorInput  = getString(R.string.error_required_confirm_password);
            focusView  = confirmPinEntryEdTxt;
            cancel     = true;
        }

        if( currMode == MODE_NEW_PIN || currMode == MODE_RESET_PIN ){
            if( !StringComponent.isNull(confirm) && !pin.equals(confirm) ){
                errorInput  = getString(R.string.error_pin_tidaksama);
                focusView   = confirmPinEntryEdTxt;
                cancel      = true;
            }
        }

        if( cancel ){
            focusView.requestFocus();
            if( !StringComponent.isNull(errorInput) ){
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(errorInput)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {
                            }
                        })
                        .show();
            }
        }
        else {
            if (currMode == MODE_NEW_PIN) {
                if (pin.equals(confirm)) {
                    infoNotice.setTitle(getString(R.string.text_informasi))
                            .setMessage(getString(R.string.text_notice_sms))
                            .setColoredCircle(R.color.dialogInfoBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_error, R.color.white)
                            .setCancelable(true)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogInfoBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setNoticeButtonClick(new Closure() {
                                @Override
                                public void exec() {
                                    SetupWalletAsyncTask setupWalletAsyncTask = new SetupWalletAsyncTask();
                                    setupWalletAsyncTask.execute(pin);
                                }
                            })
                            .show();
                } else {
                    pinEntryEdTxt.setText("");
                    currMode = MODE_NEW_PIN;
                    instructionsTxt.setText(R.string.label_enter_new_pin);
                }
            }
            else if (currMode == MODE_VERIFICATION) {
                infoNotice.setTitle(getString(R.string.text_informasi))
                        .setMessage(getString(R.string.text_notice_sms))
                        .setColoredCircle(R.color.dialogInfoBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_error, R.color.white)
                        .setCancelable(true)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogInfoBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setNoticeButtonClick(new Closure() {
                            @Override
                            public void exec() {
                                SetupWalletAsyncTask setupWalletAsyncTask = new SetupWalletAsyncTask();
                                setupWalletAsyncTask.execute(pin);
                            }
                        })
                        .show();
            }
            else if( currMode == MODE_RESET_PIN ){
                if( pin.equals(confirm) ){
                    SetupResetNewPin setupResetNewPin = new SetupResetNewPin();
                    setupResetNewPin.execute(pin);
                }
                else {
                    pinEntryEdTxt.setText("");
                    currMode = MODE_RESET_PIN;
                    instructionsTxt.setText(R.string.label_enter_new_reset_pin);
                }
            }
        }
    }

    public class SetupWalletAsyncTask extends AsyncTask<String, Void, WalletBankDKI> {

        String msgError = null;
        boolean walletSaved = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(getString(R.string.text_section_verification))
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
        }

        @Override
        protected WalletBankDKI doInBackground(String... strings) {
            String pin          = strings[0];
            ApiStudo apistudo   = new ApiStudo();
            getListMuridResponse listMurid = apistudo.getDaftarMurid();
            List<Student> students = listMurid.data;
            if (currMode == MODE_NEW_PIN) {
                WalletBankDKI walletBankDKI = new WalletBankDKI();
                if (students.get(0).isWaliIbu(Config.getInstance().getSessionPhone())) {
                    Config.getInstance().addLogs("kesini isWaliIbu");
                    String[] name = students.get(0).getIbuNama().split(" ");
                    walletBankDKI.setEmail(students.get(0).getIbuEmail());
                    walletBankDKI.setBirth_date(students.get(0).getIbuTglLahir());
                    walletBankDKI.setBirth_place(students.get(0).getIbuTmptLahir());
                    walletBankDKI.setFirst_name(name[0]);
                    if (name.length > 1) {
                        walletBankDKI.setLast_name(name[1]);
                    }
                } else if (students.get(0).isWaliAyah(Config.getInstance().getSessionPhone())) {
                    Config.getInstance().addLogs("kesini isWaliAyah");
                    String[] name = students.get(0).getAyahNama().split(" ");
                    walletBankDKI.setEmail(students.get(0).getAyahEmail());
                    walletBankDKI.setBirth_date(students.get(0).getAyahTglLahir());
                    walletBankDKI.setBirth_place(students.get(0).getAyahTmptLahir());
                    walletBankDKI.setFirst_name(name[0]);
                    if (name.length > 1) {
                        walletBankDKI.setLast_name(name[1]);
                    }
                }

                walletBankDKI.setAccount_number(Config.getInstance().getSessionPhone());
                walletBankDKI.setMsisdn(Config.getInstance().getSessionPhone());
                RegisterAccountRequest registerAccountRequest = new RegisterAccountRequest();
                registerAccountRequest.setPassword(pin);
                registerAccountRequest.setFirstName(walletBankDKI.getFirst_name());
                registerAccountRequest.setLastName(walletBankDKI.getLast_name());
                registerAccountRequest.setMsisdn(walletBankDKI.getMsisdn());
                registerAccountRequest.setPlaceOfBirth(walletBankDKI.getBirth_place());
                registerAccountRequest.setDateOfBirth(walletBankDKI.getBirth_date());
                registerAccountRequest.setEmail(walletBankDKI.getEmail());

                BankDKI apiBankDKI = new BankDKI();
                RegisterAccountResponse registerAccountResponse = apiBankDKI.accRegister(registerAccountRequest);
                if (registerAccountResponse == null) {
                    msgError = apiBankDKI.getMsgError();
                } else if (registerAccountResponse != null && registerAccountResponse.responseStatus() == 0) {
                    if (registerAccountResponse.getError().getCode() == -1) {
                        msgError = registerAccountResponse.getError().getFaultMessage();
                    } else {
                        msgError = registerAccountResponse.getError().getMessage();
                    }
                } else if (registerAccountResponse != null && registerAccountResponse.responseStatus() == 1) {
                    walletBankDKI.setId(registerAccountResponse.getId());
                    walletBankDKI.setUsername(registerAccountResponse.getUsername());
                    walletBankDKI.setBalance("0");
                    Config.getInstance().getDatabase().walletDKIDao().update(walletBankDKI);
                    if (Config.getInstance().encryptUsernames(students, pin, walletBankDKI)) {
                        for (Student student : students) {
                            BasicResponse response = apistudo.updateParentWallet(student.getParentEncryptedWalletUsername());
                            if (response != null && response.rc == 1) {
                                response = apistudo.updateStudentWallet(student.getNIS(), student.getStudentEncryptedWalletUsername());
                            }

                            if (response == null || response.rc == 0) {
                                if (response == null) {
                                    msgError = apistudo.getMsgError();
                                } else if (response.rc == 0) {
                                    msgError = response.msg;
                                }
                            } else {
                                walletSaved = true;
                            }
                        }
                    }

                    return walletBankDKI;
                }
            } else if (currMode == MODE_VERIFICATION && mWallet != null) {
                CheckAccountRequest checkAccountRequest = new CheckAccountRequest();
                checkAccountRequest.setUsername(mWallet.getUsername());
                checkAccountRequest.setPassword(pin);
                BankDKI apiBankDKI = new BankDKI();
                CheckAccountResponse checkAccountResponse = apiBankDKI.accCheck(checkAccountRequest);
                if (checkAccountResponse == null) {
                    msgError = apiBankDKI.getMsgError();
                } else if (checkAccountResponse != null && checkAccountResponse.responseStatus() == 0) {
                    if (checkAccountResponse.getError().getCode() == FaultError.BAD_CREDENTIAL) {
                        msgError = getString(R.string.error_wallet_bad_credentials);
                        invalidPin++;
                        Config.getInstance().setSessionWalletRetry(invalidPin);
                    } else {
                        msgError = checkAccountResponse.getError().getFaultMessage();
                    }
                } else if (checkAccountResponse != null && checkAccountResponse.responseStatus() == 1) {
                    if (checkAccountResponse.getAccountStatus().equals("true")) {
                        Config.getInstance().addLogs("helo bos --> " + mWallet.getUsername() );
                        if (Config.getInstance().encryptUsernames(students, pin, mWallet)) {
                            for (Student student : students) {
                                BasicResponse response = apistudo.updateParentWallet(student.getParentEncryptedWalletUsername());
                                if (response != null && response.rc == 1)
                                    response = apistudo.updateStudentWallet(student.getNIS(), student.getStudentEncryptedWalletUsername());
                                if (response == null || response.rc == 0) {
                                    if (response == null) {
                                        msgError = apistudo.getMsgError();
                                    } else if (response.rc == 0) {
                                        msgError = response.msg;
                                    }
                                } else {
                                    walletSaved = true;
                                }
                            }
                        }
                        return mWallet;
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(WalletBankDKI walletBankDKI) {
            super.onPostExecute(walletBankDKI);
            if (infoProcess != null) {
                infoProcess.hide();
            }

            if (walletBankDKI != null) {
                if (!walletSaved) {
                    registerNew = false;
                    mWallet = walletBankDKI;
                    refreshUI();
                } else {
                    Intent codeVerificationAct = CodeVerificationActivity.createIntent(getApplicationContext());
                    startActivity(codeVerificationAct);
                    finish();
                }
            } else {
                if (StringComponent.isNull(msgError)) {
                    msgError = getString(R.string.error_gkjelas);
                }
                else if (currMode == MODE_VERIFICATION) {
                    pinEntryEdTxt.setText("");
                    if (Config.getInstance().getSessionWalletRetry() >= 2) {
                        infoError
                                .setTitle(getString(R.string.title_wallet_forgot_pin))
                                .setMessage(getString(R.string.text_wallet_forgot_pin))
                                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                                .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                                .setButtonText(getString(R.string.dialog_ok_button))
                                .setErrorButtonClick(new Closure() {
                                    @Override
                                    public void exec() {
                                        showLupaPinActivity();
                                    }
                                })
                                .show();
                    } else {
                        infoError
                                .setTitle(getString(R.string.verification_failed))
                                .setMessage(msgError)
                                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                                .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                                .setButtonText(getString(R.string.dialog_ok_button))
                                .setErrorButtonClick(new Closure() {
                                    @Override
                                    public void exec() {

                                    }
                                })
                                .show();
                    }
                } else if (currMode == MODE_NEW_PIN) {
                    infoError
                            .setTitle(getString(R.string.title_wallet_setupError))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
        }
    }

    private void run(){
        if (currMode == MODE_NEW_PIN) {
            instructionsTxt.setText(getString(R.string.label_enter_new_pin));
        }
        else if( currMode == MODE_RESET_PIN) {
            loadingLayout.setVisibility(View.GONE);
            layoutPin.setVisibility(View.VISIBLE);
            btnLupaPin.setVisibility(View.GONE);
            tvProgress.setVisibility(View.GONE);
            instructionsTxt.setText(getString(R.string.label_enter_new_reset_pin));
            tvToolbarTitle.setText(getString(R.string.title_setuplupapin_toolbar));
            confirmPinEntryEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        prepareDataWallet();
                        return true;
                    }
                    return false;
                }
            });
        }

        if( currMode != MODE_RESET_PIN ){
            CheckWalletAsyncTask checkWalletTask = new CheckWalletAsyncTask();
            checkWalletTask.execute();
        }
    }

    public void refreshUI() {
        if (registerNew) {
            currMode = MODE_NEW_PIN;
            btnLupaPin.setVisibility(View.GONE);
            instructionsTxt.setText(R.string.label_enter_new_pin);
            confirmPinEntryEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        prepareDataWallet();
                        return true;
                    }
                    return false;
                }
            });
        }
        else if( resetPin ){
            currMode = MODE_RESET_PIN;
            instructionsTxt.setText(getString(R.string.label_enter_new_reset_pin));
            confirmPinEntryEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        prepareDataWallet();
                        return true;
                    }
                    return false;
                }
            });
        }
        else if (mWallet != null) {
            currMode = MODE_VERIFICATION;
            instructionsTxt.setText(R.string.label_verify_pin);
            layoutConfirmPin.setVisibility(View.GONE);
            pinEntryEdTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                        prepareDataWallet();
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    public class CheckWalletAsyncTask extends AsyncTask<Void, Void, WalletBankDKI> {
        FaultError walletError = null;
        String msgError = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingLayout.setVisibility(View.VISIBLE);
            layoutPin.setVisibility(View.INVISIBLE);
        }

        @Override
        protected WalletBankDKI doInBackground(Void... voids) {
            WalletBankDKI walletDKI = Config.getInstance().getDatabase().walletDKIDao().findByMsisdn(Config.getInstance().getSessionPhone());
            if (walletDKI == null) {
                BankDKI bankDKI = new BankDKI();
                AccountInformationRequest accountInformationRequest = new AccountInformationRequest();
                accountInformationRequest.setMsisdn(Config.getInstance().getSessionPhone());
                AccountInformationResponse accountInformationResponse = bankDKI.accInformation(accountInformationRequest);
                if (accountInformationResponse == null) {
                    msgError = bankDKI.getMsgError();
                } else if (accountInformationResponse != null && accountInformationResponse.responseStatus() == 0) {
                    walletError = accountInformationResponse.getError();
                } else if (accountInformationResponse != null && accountInformationResponse.responseStatus() == 1) {
                    walletDKI = accountInformationResponse.getWalletInfo();
                    Config.getInstance().getDatabase().walletDKIDao().insertAll(Arrays.asList(walletDKI));
                }
            }

            return walletDKI;
        }

        @Override
        protected void onPostExecute(WalletBankDKI walletBankDKI) {
            super.onPostExecute(walletBankDKI);
            if (walletBankDKI != null) {
                mWallet = walletBankDKI;
                refreshUI();
                loadingLayout.setVisibility(View.GONE);
                layoutPin.setVisibility(View.VISIBLE);
            } else if (walletError != null) {
                if (walletError.getCode() == FaultError.ACCOUNT_NOT_FOUND) {
                    registerNew = true;
                    refreshUI();
                    loadingLayout.setVisibility(View.GONE);
                    layoutPin.setVisibility(View.VISIBLE);
                } else {
                    infoError
                            .setTitle(getString(R.string.text_error))
                            .setMessage(walletError.getFaultMessage())
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            } else if (!StringComponent.isNull(msgError)) {
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(msgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {

                            }
                        })
                        .show();
            }
            else {
                infoError
                        .setTitle(getString(R.string.title_response_timeout))
                        .setMessage(getString(R.string.text_response_timeout))
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {
                                Config.getInstance().resetUserVerifikasi();
                                Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFICATION);
                                Intent verificationActivity = UserVerificationActivity.createIntent(getApplicationContext());
                                startActivity(verificationActivity);
                            }
                        })
                        .show();
            }
        }
    }

    public class SetupResetNewPin extends AsyncTask<String, Void, WalletBankDKI>{

        String msgError = null;
        boolean walletSaved = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProcess.setTitle(getString(R.string.title_waiting))
                    .setMessage(getString(R.string.text_section_verification))
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
        }

        @Override
        protected WalletBankDKI doInBackground(String... strings) {
            String pin = strings[0];
            ApiStudo apistudo               = new ApiStudo();
            getListMuridResponse listMurid  = apistudo.getDaftarMurid();
            List<Student> students          = listMurid.data;

            if( currMode == MODE_RESET_PIN ){

                mWalletPin = new WalletPinBankDKI();
                mWalletPin.setId(Config.getInstance().getSessionAccountIDWallet());

                UbahPinRequest ubahPinRequest = new UbahPinRequest();
                ubahPinRequest.setId(mWalletPin.getId());
                ubahPinRequest.setPassword(pin);

                BankDKI apiBankDKI = new BankDKI();
                UbahPinResponse ubahPinResponse = apiBankDKI.accExecNewPin(ubahPinRequest);
                if( ubahPinResponse == null ){
                    msgError = apiBankDKI.getMsgError();
                }
                else if( ubahPinResponse != null && ubahPinResponse.responseStatus() == 0 ){
                    if( ubahPinResponse.getError().getCode() == 1 ){
                        msgError = ubahPinResponse.getError().getFaultMessage();
                    }
                    else {
                        msgError = ubahPinResponse.getError().getMessage();
                    }
                }
                else if( ubahPinResponse != null && ubahPinResponse.responseStatus() == 1 ){
                    mWallet = new WalletBankDKI();
                    mWallet.setId(ubahPinResponse.getId());
                    mWallet.setUsername(ubahPinResponse.getUsername());
                    mWallet.setBalance("0");
                    Config.getInstance().getDatabase().walletDKIDao().update(mWallet);
                    if( Config.getInstance().encryptUsernames(students, pin, mWallet)){
                        for (Student student : students) {
                            BasicResponse response = apistudo.updateParentWallet(student.getParentEncryptedWalletUsername());
                            if (response != null && response.rc == 1) {
                                response = apistudo.updateStudentWallet(student.getNIS(), student.getStudentEncryptedWalletUsername());
                            }

                            if (response == null || response.rc == 0) {
                                if (response == null) {
                                    msgError = apistudo.getMsgError();
                                } else if (response.rc == 0) {
                                    msgError = response.msg;
                                }
                            } else {
                                walletSaved = true;
                            }
                        }
                    }

                    return mWallet;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(WalletBankDKI walletBankDKI) {
            super.onPostExecute(walletBankDKI);
            if( infoProcess != null ){
                infoProcess.hide();
            }

            if( walletBankDKI != null ){
                if( !walletSaved ){
                    resetPin = true;
                    refreshUI();
                }
                else {
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_FINISH);
                    Config.getInstance().setVerificationStatus(Config.VERIFICATION_SUCCESS);
                    startMainActivity();
                }
            }
            else {
                if (StringComponent.isNull(msgError)) {
                    msgError = getString(R.string.error_gkjelas);
                }
                else if( currMode == MODE_RESET_PIN ){
                    infoError
                            .setTitle(getString(R.string.title_wallet_setupError))
                            .setMessage(msgError)
                            .setColoredCircle(R.color.dialogErrorBackgroundColor)
                            .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                            .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                            .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                            .setButtonText(getString(R.string.dialog_ok_button))
                            .setErrorButtonClick(new Closure() {
                                @Override
                                public void exec() {

                                }
                            })
                            .show();
                }
            }
        }
    }

    private void showLupaPinActivity(){
        Intent lupaPinAct = LupaPinActivity.CreateIntent(getApplicationContext());
        startActivity(lupaPinAct);
    }

    private void startMainActivity(){
        Intent BerandaAct = BerandaActivity.createIntent(getApplicationContext());
        startActivity(BerandaAct);
        finish();
    }
}
