package id.adadigitals.edusia.xui;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.adapter.SavingsReportAdapter;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.response.json.GetLaporanTabunganResponse;

import id.adadigitals.edusia.api.ApiStudo;

public class SavingHistoryActivity extends AppCompatActivity {

    RecyclerView savingsHistoryList;
    SavingsReportAdapter savingsReportAdapter;

    RelativeLayout progressLayout;

    LinearLayout infoLayout;
    TextView infoTxt;
    AppCompatButton reloadBtn;

    String mNis = null;

    int mPage = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_saving_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
        if (getIntent().getExtras()!=null){
            mNis = getIntent().getStringExtra(Config.KEY_PARAM_NIS);
        }else{
            finish();
        }

        setupUserInterface();
    }

    public void setupUserInterface(){
        infoLayout = (LinearLayout) findViewById(R.id.infoLayout);
        infoTxt = (TextView) findViewById(R.id.infoTxt);

        savingsHistoryList = (RecyclerView) findViewById(R.id.savingsHistoryList);
        progressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        savingsHistoryList.setLayoutManager(linearLayoutManager);
        savingsReportAdapter = new SavingsReportAdapter();
        savingsHistoryList.setAdapter(savingsReportAdapter);

        reloadBtn = (AppCompatButton) findViewById(R.id.reloadBtn);
        reloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        LoadSavingsTask loadSavingsTask = new LoadSavingsTask();
        loadSavingsTask.execute(mNis);
    }

    public class LoadSavingsTask extends AsyncTask<String, Void, GetLaporanTabunganResponse> {

        String mLastErrorMsg;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoLayout.setVisibility(View.GONE);
            savingsHistoryList.setVisibility(View.GONE);
            progressLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected GetLaporanTabunganResponse doInBackground(String... strings) {
            String nis = strings[0];
            ApiStudo apiStudo = new ApiStudo();
            int start = mPage * Config.DEFAULT_LIMIT;
            GetLaporanTabunganResponse getLaporanTabunganResponse = apiStudo.getLaporanTabungan(nis, start, Config.DEFAULT_LIMIT);
            if(getLaporanTabunganResponse!=null && getLaporanTabunganResponse.rc == 1){

            }else if(getLaporanTabunganResponse == null){
                mLastErrorMsg = getString(R.string.error_network_connection);
            }else if(getLaporanTabunganResponse.rc != 1){
                mLastErrorMsg = getLaporanTabunganResponse.msg;
            }
            return getLaporanTabunganResponse;
        }

        @Override
        protected void onPostExecute(GetLaporanTabunganResponse getLaporanTabunganResponse) {
            super.onPostExecute(getLaporanTabunganResponse);
            progressLayout.setVisibility(View.GONE);

            if(getLaporanTabunganResponse !=null){
                if(getLaporanTabunganResponse.rc==1){
                    if(getLaporanTabunganResponse.data!=null && getLaporanTabunganResponse.data.size() > 0){
                        mPage++;
                        savingsReportAdapter.addData(getLaporanTabunganResponse.data);
                        infoLayout.setVisibility(View.GONE);
                        savingsHistoryList.setVisibility(View.VISIBLE);
                    }else{
                        infoLayout.setVisibility(View.VISIBLE);
                        savingsHistoryList.setVisibility(View.GONE);
                        infoTxt.setText(getString(R.string.desc_savings_not_available));
                        reloadBtn.setVisibility(View.GONE);
                    }
                }else if(getLaporanTabunganResponse.rc !=1) {
                    if(StringComponent.isNull(mLastErrorMsg)){
                        mLastErrorMsg = Config.getInstance().getString(R.string.error_gkjelas);
                    }
                    infoLayout.setVisibility(View.VISIBLE);
                    savingsHistoryList.setVisibility(View.GONE);
                }
            }else{
                if(StringComponent.isNull(mLastErrorMsg)){
                    mLastErrorMsg = Config.getInstance().getString(R.string.error_gkjelas);
                }
                infoLayout.setVisibility(View.VISIBLE);
                savingsHistoryList.setVisibility(View.GONE);
            }
        }
    }
}
