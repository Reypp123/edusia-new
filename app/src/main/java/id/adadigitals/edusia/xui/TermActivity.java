package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.support.v7.widget.Toolbar;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;

public class TermActivity extends AppCompatActivity {

    private CheckBox agree;
    private WebView webview;
    private Button btnNext;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_terms);
        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, TermActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private void declare(){
        agree       = (CheckBox) findViewById(R.id.chkAgree);
        webview     = (WebView) findViewById(R.id.webView);
        btnNext     = (Button) findViewById(R.id.nextBtn);
        toolbar     = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        StylingComponent.setSystemBarColor(this, R.color.colorPrimary);
    }

    private void run(){
        btnNext.setEnabled(false);
        agree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if( isChecked ){
                    btnNext.setEnabled(true);
                }
                else {
                    btnNext.setEnabled(false);
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( Config.FACE_RECOGNITION ){

                }
                else {
                    Config.getInstance().setOnboardingStatus(Config.ONBOARDING_SET_PASSWORD);
                    Intent loginActivity = LoginActivity.createIntent(getApplicationContext());
                    loginActivity.putExtra(LoginActivity.PARAMS_LOGIN_MODE, LoginActivity.MODE_SETUP);
                    startActivity(loginActivity);
                }

                finish();
            }
        });

        webview.clearCache(true);
        webview.setWebViewClient(new WebViewClient());
        webview.loadUrl("file:///android_asset/terms.html");
        webview.setBackgroundColor(getResources().getColor(R.color.bg_splash));
        //webview.loadUrl("https://www.google.com/");
    }
}
