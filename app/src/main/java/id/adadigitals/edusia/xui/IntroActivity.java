package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;

public class IntroActivity extends AppCompatActivity {
    private String _LOG = "__INTRO__";

    private Button btnNext, btnSkip;
    private SharedPreferences prefs;
    private ViewPager viewpager;
    private TextView[] dots;
    private int[] layouts;
    private LinearLayout dotsLayout;
    private MyViewAdapter myviewadapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_intro);
        StylingComponent.setSystemBarColor(this, R.color.bg_splash);
        cekFirstLaunch();
        declare();
        process();
    }

    private void declare(){
        viewpager  = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        btnSkip    = (Button) findViewById(R.id.btn_skip);
        btnNext    = (Button) findViewById(R.id.btn_next);
    }

    private void cekFirstLaunch(){
        Boolean isPertamaBuka   = Config.getInstance().getIsFirstLaunch();
        if( !isPertamaBuka || isPertamaBuka == false ){
            launchLoginScreen();
        }
    }

    private void process(){
        layouts = new int[]{
                R.layout.slide1,
                R.layout.slide2,
                R.layout.slide3,
                R.layout.slide4
        };

        addBottomDots(0);
        changeStatusBarColor();

        myviewadapter = new MyViewAdapter();
        viewpager.setAdapter(myviewadapter);
        viewpager.addOnPageChangeListener(viewPagerPageChangeListener);

        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchLoginScreen();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if(current < layouts.length){
                    viewpager.setCurrentItem(current);
                }
                else {
                    launchLoginScreen();
                }
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }

    public void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    private void launchLoginScreen(){
        Config.getInstance().setSessionFirstLaunch(false);

        if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_VERIFICATION ){
            showUserVerificationActivity();
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_TERMS_CONDITION ){
            showTermsAndCondition();
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_SET_PASSWORD ){
            showSetupPasswordActivity(LoginActivity.MODE_SETUP);
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_VERIFIKASI_PASSWORD ){
            showSetupPasswordActivity(LoginActivity.MODE_VERIFIKASI);
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_SETUP_WALLET ){
            if( Config.getInstance().isVerificationInProgress() ){
                showCodeVerificationActivity();
            }
            else {
                showSetupWallet(SetupWallet.MODE_NEW_PIN);
            }
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_VERIFIKASI_DATA_MURID ){
            showValidateDataParent();
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_FACE_ENROLLMENT ){
            showFaceActivity();
        }
        else if( Config.getInstance().getOnboardingStatus() == Config.ONBOARDING_FINISH ){
            showMainActivity();
        }


        finish();
    }

    private void showFaceActivity(){
        Intent faceAct = FaceCaptureActivity.createIntent(getApplicationContext());
        startActivity(faceAct);
    }

    private void showMainActivity(){
        Intent berandaAct = BerandaActivity.createIntent(getApplicationContext());
        startActivity(berandaAct);
        finish();
    }

    public void showUserVerificationActivity(){
        Intent verificationActivity = UserVerificationActivity.createIntent(getApplicationContext());
        startActivity(verificationActivity);
    }

    private void showTermsAndCondition(){
        Intent termsActivity = TermActivity.createIntent(getApplicationContext());
        startActivity(termsActivity);
    }

    private void showSetupPasswordActivity(int loginMode){
        Intent setupPasswordActivity = LoginActivity.createIntent(getApplicationContext());
        setupPasswordActivity.putExtra(LoginActivity.PARAMS_LOGIN_MODE, loginMode);
        startActivity(setupPasswordActivity);
    }

    private void showSetupWallet(int walletMode){
        Intent setupWalletActivity = SetupWallet.createIntent(getApplicationContext());
        setupWalletActivity.putExtra(SetupWallet.PARAMS_WALLET_MODE, walletMode);
        startActivity(setupWalletActivity);
    }

    private void showValidateDataParent(){
        Intent validateDataParent = ValidationDataStudent.createIntent(getApplicationContext());
        startActivity(validateDataParent);
    }

    private void showCodeVerificationActivity(){
        Intent showCodeVerificationAct = CodeVerificationActivity.createIntent(getApplicationContext());
        startActivity(showCodeVerificationAct);
        finish();
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
            addBottomDots(i);

            if(i == layouts.length - 1){
                btnNext.setText(getString(R.string.start));
                btnSkip.setVisibility(View.GONE);
            }
            else{
                btnNext.setText(getString(R.string.next));
                btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageSelected(int i) {

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    public class MyViewAdapter extends PagerAdapter{
        private LayoutInflater layoutInflater;

        public MyViewAdapter(){
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view      = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position,Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

}
