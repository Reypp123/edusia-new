package id.adadigitals.edusia.xui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import id.adadigitals.edusia.R;

import java.io.File;

import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.component.FileComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.component.StylingComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.response.json.getUsernameResponse;

public class UserVerificationActivity extends AppCompatActivity{
    private final String __LOG__ = "__userVerification__";

    private Handler mHandler;
    private RelativeLayout rootLayout;
    private TextView textVerification;
    private ProgressBar progressBar;
    private LinearLayout formLayout;
    private EditText etNIK;
    private EditText etNOHP;
    private Button btnNext;
    private AwesomeErrorDialog infoError;
    private AwesomeProgressDialog infoProgress;
    String mNik = null;
    String mNoHP = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_user_verification);
        StylingComponent.setSystemBarColor(this, R.color.bg_splash);
        declare();
        run();
    }

    public static Intent createIntent(Context context){
        Intent intent = new Intent(context, UserVerificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    private void declare(){
        mHandler = new Handler();
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);
        textVerification = (TextView) findViewById(R.id.text_verification);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        formLayout = (LinearLayout) findViewById(R.id.formLayout);
        etNIK = (EditText) findViewById(R.id.etNIK);
        etNOHP = (EditText) findViewById(R.id.etNOHP);
        btnNext = (Button) findViewById(R.id.btn_next);

        infoError = new AwesomeErrorDialog(this);
        infoProgress = new AwesomeProgressDialog(this);
    }

    private void run(){

        etNIK.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL ){
                    checkInput();
                    return true;
                }
                return false;
            }
        });

        etNOHP.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if( actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL ){
                    checkInput();
                    return true;
                }
                return false;
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInput();
            }
        });
    }

    private void checkInput(){

        etNIK.setError(null);
        etNOHP.setError(null);

        mNik = etNIK.getText().toString();
        mNoHP = etNOHP.getText().toString();
        boolean cancel  = false;
        View focusView  = null;

        if( StringComponent.isNull(mNik) ){
            etNIK.setError(getString(R.string.error_required_nik));
            focusView = etNIK;
            cancel    = true;
        }

        if( StringComponent.isNull(mNoHP) ){
            etNOHP.setError(getString(R.string.error_required_msisdn));
            focusView  = etNOHP;
            cancel     = true;
        }

        if( cancel ){
            focusView.requestFocus();
        }
        else {
            UserVerificationTask userVerificationTask = new UserVerificationTask();
            userVerificationTask.execute(mNik,mNoHP);
        }
    }


    public class UserVerificationTask extends AsyncTask<String, Void, getUsernameResponse>{
        String mNik;
        String mNohp;
        String MsgError = null;
        File fileTemplate   = null;
        Boolean lolos       = false;
        Boolean ConnFail    = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            infoProgress.setTitle(getString(R.string.title_waiting))
                    .setMessage(R.string.text_section_verification)
                    .setColoredCircle(R.color.dialogInfoBackgroundColor)
                    .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                    .setCancelable(false)
                    .show();
            etNIK.setEnabled(false);
            etNOHP.setEnabled(false);
        }

        @Override
        protected getUsernameResponse doInBackground(String... strings) {
            mNik  = strings[0];
            mNohp = strings[1];

            ApiStudo apistudo = new ApiStudo();
            getUsernameResponse getusername = apistudo.getUsername(mNohp, mNik);
            if ( getusername == null ){
                ConnFail    = apistudo.getConnFail();
                MsgError    = apistudo.getMsgError();
            }
            else if( !StringComponent.isNull(getusername.getUsername()) ) {
                Config.getInstance().setSessionDataLogin(mNik, mNohp);
                Config.getInstance().setSessionUsernamegetOnboardingStatuse(getusername.getUsername());

                if( !StringComponent.isNull(getusername.getFaceTemplate() )){
                    String onboardNik = FileComponent.stripExt(getusername.getFaceTemplate());
                    if ( !StringComponent.isNull(onboardNik) ){
                        File outputFile     = Config.getInstance().getOrtuFace( getusername.getFaceTemplate() );
                        String url_photo    = Config.getInstance().getPhotoUrl( getusername.getFaceTemplate() );
                        fileTemplate        = apistudo.downloadFileNya(url_photo, outputFile);

                        //Jika Downloadnya gagal
                        if( fileTemplate == null ){
                            ConnFail    = apistudo.getConnFail();
                            MsgError    = apistudo.getMsgError();
                        }
                    }
                }

                lolos = true;
            }
            else if ( getusername.rc == 0 ){
                if( !getusername.checkNikFound() ){
                    MsgError = getString(R.string.error_nik_notFound);
                }
                else if ( !getusername.checkHPFound() ){
                    MsgError = getString(R.string.error_nohp_notFound);
                }
                else {
                    MsgError = getString(R.string.error_nik_hp_notFound);
                }
            }

            return getusername;
        }

        @Override
        protected void onPostExecute(getUsernameResponse getUsernameResponse) {
            super.onPostExecute(getUsernameResponse);

            if( infoProgress != null){
                infoProgress.hide();
            }

            etNIK.setEnabled(true);
            etNOHP.setEnabled(true);

            if( StringComponent.isNull(MsgError) ){
                MsgError = getString(R.string.error_gkjelas);
            }

            if( lolos ){
                if( getUsernameResponse != null ){
                    if( StringComponent.isNull(getUsernameResponse.getUsername()) ){
                        infoError
                                .setTitle(getString(R.string.verification_failed))
                                .setMessage(MsgError)
                                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                                .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                                .setButtonText(getString(R.string.dialog_ok_button))
                                .setErrorButtonClick(new Closure() {
                                    @Override
                                    public void exec() {

                                    }
                                })
                                .show();

                    }
                    else {
                        if( StringComponent.isNull(getUsernameResponse.getFaceTemplate()) ){
                            Config.getInstance().setSessionStatusVerified(true);
                            Config.getInstance().setOnboardingStatus(Config.ONBOARDING_TERMS_CONDITION);

                            Intent termsActivity = TermActivity.createIntent(UserVerificationActivity.this);
                            startActivity(termsActivity);
                            finish();
                        }
                        else {
                            if( Config.FACE_RECOGNITION ){
                                //Verifikasi Muka
                            }
                            else
                            {
                                Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFIKASI_PASSWORD);
                                Intent loginAct = LoginActivity.createIntent(UserVerificationActivity.this);
                                loginAct.putExtra(LoginActivity.PARAMS_LOGIN_MODE, LoginActivity.MODE_VERIFIKASI);
                                startActivity(loginAct);
                                finish();
                            }
                        }
                    }
                }
            }
            else {
                infoError
                        .setTitle(getString(R.string.text_error))
                        .setMessage(MsgError)
                        .setColoredCircle(R.color.dialogErrorBackgroundColor)
                        .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                        .setCancelable(false).setButtonText(getString(R.string.dialog_ok_button))
                        .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                        .setButtonText(getString(R.string.dialog_ok_button))
                        .setErrorButtonClick(new Closure() {
                            @Override
                            public void exec() {
                                if( ConnFail ){
                                    reset();
                                }
                            }
                        })
                        .show();
            }
        }
    }

    public void reset(){
        Config.getInstance().resetUserVerifikasi();
        Config.getInstance().setOnboardingStatus(Config.ONBOARDING_VERIFICATION);
        Intent verificationActivity = UserVerificationActivity.createIntent(getApplicationContext());
        startActivity(verificationActivity);
        finish();
    }

}
