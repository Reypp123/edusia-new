package id.adadigitals.edusia.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.threeten.bp.format.TextStyle;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.CalendarEvent;

public class CalendarEventAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_CALENDAR_HEADER = 0;
    private static final int TYPE_CALENDAR_ITEM = 1;
    private static final int TYPE_CALENDAR_FOOTER = 2;

    OnMonthChangedListener onMonthChangedListener;
    View calendarHeader;

    public List<CalendarEvent> events = new ArrayList<CalendarEvent>();

    public CalendarEventAdapter(View calendarHeader){
        this.calendarHeader = calendarHeader;
    }

    public void setOnMonthChangedListener(OnMonthChangedListener monthChangedListener){
        this.onMonthChangedListener = monthChangedListener;
    }

    public void refreshEvents(List<CalendarEvent> events){
        if(events!=null){
            this.events.clear();
            this.events.addAll(events);
        }else{
            this.events.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_CALENDAR_HEADER) {
            return new CalendarHeader(calendarHeader);
        } else if(viewType == TYPE_CALENDAR_ITEM) {
            return new CalendarItem(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_calendar_item, parent, false));
        }else{
            return new CalendarFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_calendar_footer, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CalendarHeader) {

        } else if (holder instanceof CalendarItem) {
            CalendarItem eventItem = (CalendarItem) holder;
            eventItem.dayTxt.setText(events.get(position-1).date.getDate().getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.getDefault()));
            eventItem.dateTxt.setText(events.get(position-1).tgl);
            eventItem.eventColor.setBackgroundColor(Color.parseColor(events.get(position-1).getColor()));
            eventItem.eventTxt.setText(events.get(position-1).title);
            eventItem.categoryTxt.setText(events.get(position-1).description);
        }else if (holder instanceof CalendarFooter){
            CalendarFooter calendarFooter = (CalendarFooter) holder;
            if(getItemCount() == 2){
                calendarFooter.footerTxt.setVisibility(View.VISIBLE);
                calendarFooter.topDivider.setVisibility(View.GONE);
            }else {
                calendarFooter.footerTxt.setVisibility(View.GONE);
                calendarFooter.topDivider.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return events.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_CALENDAR_HEADER;
        else if(isPositionFooter(position))
            return TYPE_CALENDAR_FOOTER;
        return TYPE_CALENDAR_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private boolean isPositionFooter(int position) {
        return position == getItemCount()-1;
    }

    public class CalendarHeader extends RecyclerView.ViewHolder {

        MaterialCalendarView calendarView;

        public CalendarHeader(View view) {
            super(view);
            calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
            if(onMonthChangedListener!=null)
                calendarView.setOnMonthChangedListener(onMonthChangedListener);
        }

        public MaterialCalendarView getCalendarView(){
            return calendarView;
        }

    }

    class CalendarItem extends RecyclerView.ViewHolder {

        View eventColor;

        TextView dayTxt;
        TextView dateTxt;
        TextView eventTxt;
        TextView categoryTxt;

        public CalendarItem(View view) {
            super(view);
            eventColor = view.findViewById(R.id.eventColor);
            dayTxt = (TextView) view.findViewById(R.id.dayTxt);
            dateTxt = (TextView) view.findViewById(R.id.dateTxt);
            eventTxt = (TextView) view.findViewById(R.id.eventTxt);
            categoryTxt = (TextView) view.findViewById(R.id.categoryTxt);
        }

    }

    class CalendarFooter extends RecyclerView.ViewHolder {

        TextView footerTxt;
        View topDivider;

        public CalendarFooter(View view) {
            super(view);
            footerTxt = (TextView) view.findViewById(R.id.footerTxt);
            topDivider = view.findViewById(R.id.topDivider);
        }

    }
}
