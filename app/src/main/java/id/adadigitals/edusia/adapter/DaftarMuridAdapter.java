package id.adadigitals.edusia.adapter;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.ImageComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.xui.TimeLineDetailActivity;

public class DaftarMuridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Student> students;
    private Context context;

    public DaftarMuridAdapter(Context context){
        this.students               = new ArrayList<>();
        this.context                = context;
    }

    public void refreshAdapter(List<Student> students){
        this.students.clear();
        this.students.addAll(students);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        final View view = inflater.inflate(R.layout.item_list_murid, viewGroup, false);
        return new MuridItem(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MuridItem muridItem  = (MuridItem) viewHolder;
        final Student   student    = students.get(position);

        if( !StringComponent.isNull(student.getSiswaFotoUrl()) ){
            ImageComponent.displayImageRound(muridItem.imgSiswa.getContext(), muridItem.imgSiswa, student.getSiswaFotoUrl());
        }

        muridItem.muridSekolah.setText(student.getSekolah());
        muridItem.muridName.setText(student.getNama());

        muridItem.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if( isLongClick ){
                    Intent timelineDetail = TimeLineDetailActivity.createIntent(context);
                    timelineDetail.putExtra(Config.KEY_PARAM_NIS, student.getNIS());
                    timelineDetail.putExtra(Config.KEY_PARAM_NAMA_SISWA, student.getNama());
                    context.startActivity(timelineDetail);
                }
                else {
                    Intent timelineDetail = TimeLineDetailActivity.createIntent(context);
                    timelineDetail.putExtra(Config.KEY_PARAM_NIS, student.getNIS());
                    timelineDetail.putExtra(Config.KEY_PARAM_NAMA_SISWA, student.getNama());
                    context.startActivity(timelineDetail);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MuridItem extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        private TextView muridName;
        private TextView muridSekolah;
        private ImageView imgSiswa;
        private Context context;
        private ItemClickListener itemClickListener;

        public MuridItem(View view, Context context){
            super(view);

            this.context      = context;
            muridName         = (TextView) view.findViewById(R.id.muridName);
            muridSekolah      = (TextView) view.findViewById(R.id.muridSekolah);
            imgSiswa          = (ImageView) view.findViewById(R.id.imgSiswa);

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }
}
