package id.adadigitals.edusia.adapter;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.adadigitals.edusia.R;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.wallet.response.ListTransactionResponse;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<ListTransactionResponse.Body.TrxHistoryEnquiryResponse.TrxHistory> trxHistoryList;

    public TransactionHistoryAdapter(){
        this.trxHistoryList = new ArrayList<>();
    }

    public void refreshAdapter(List<ListTransactionResponse.Body.TrxHistoryEnquiryResponse.TrxHistory> trxHistoryList){
        this.trxHistoryList.clear();
        this.trxHistoryList.addAll(trxHistoryList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TransactionItem(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_transaction_new, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TransactionItem eventItem = (TransactionItem) holder;
        ListTransactionResponse.Body.TrxHistoryEnquiryResponse.TrxHistory trxHistory = trxHistoryList.get(position);

        //eventItem.debitTxt.setText("Debit: " + debit);
        //eventItem.creditTxt.setText("Credit: " + credit);
        if(!StringComponent.isNull(trxHistory.getDebit())){
            String debit = "Rp. " + StringComponent.formatThousands(trxHistory.getDebit());
            eventItem.transactionTxt.setTextColor(ResourcesCompat.getColor(Config.getInstance().getResources(), R.color.red_900, null));
            eventItem.transactionTxt.setText(debit);
        }else if(!StringComponent.isNull(trxHistory.getCredit())){
            String credit = "Rp. " + StringComponent.formatThousands(trxHistory.getCredit());
            eventItem.transactionTxt.setTextColor(ResourcesCompat.getColor(Config.getInstance().getResources(), R.color.green_900, null));
            eventItem.transactionTxt.setText(credit);
        }
        eventItem.transactionDateTxt.setText(StringComponent.parseDKITimestamp(trxHistory.getTransactionDate()));
        eventItem.noRefferenceTrxTxt.setText("Transaction No. " + trxHistory.getNoRefferenceTrx());
        eventItem.noreffTxt.setText("Reff No. " + trxHistory.getNoreff());
        //eventItem.nameTxt.setText("Name: " + trxHistory.getName());
        //eventItem.acctFromTxt.setText("Acct From: " + trxHistory.getAcctFrom());
        //eventItem.acctFromMerchantTxt.setText("Acct From Merchant: " + trxHistory.getAcctFromMerchant());
        //eventItem.acctFromNameMerchantTxt.setText("Acct From Name Merchant: " + trxHistory.getAcctFromNameMerchant());
    }

    @Override
    public int getItemCount() {
        return trxHistoryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    class TransactionItem extends RecyclerView.ViewHolder {

        //TextView debitTxt;
        //TextView creditTxt;
        TextView transactionTxt;
        TextView transactionDateTxt;
        TextView noRefferenceTrxTxt;
        TextView noreffTxt;
        //TextView nameTxt;
        //TextView acctFromTxt;
        //TextView acctFromMerchantTxt;
        //TextView acctFromNameMerchantTxt;

        public TransactionItem(View view) {
            super(view);
            //debitTxt = (TextView) view.findViewById(R.id.debitTxt);
            //creditTxt = (TextView) view.findViewById(R.id.creditTxt);
            transactionTxt = (TextView) view.findViewById(R.id.transactionTxt);
            transactionDateTxt = (TextView) view.findViewById(R.id.transactionDateTxt);
            noRefferenceTrxTxt = view.findViewById(R.id.noRefferenceTrxTxt);
            noreffTxt = (TextView) view.findViewById(R.id.noreffTxt);
            //nameTxt = (TextView) view.findViewById(R.id.nameTxt);
            //acctFromTxt = (TextView) view.findViewById(R.id.acctFromTxt);
            //acctFromMerchantTxt = (TextView) view.findViewById(R.id.acctFromMerchantTxt);
            //acctFromNameMerchantTxt = view.findViewById(R.id.acctFromNameMerchantTxt);
        }

    }
}
