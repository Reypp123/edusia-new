package id.adadigitals.edusia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.NilaiSementara;
import id.adadigitals.edusia.xui.NilaiDetailAkademikActivity;

public class ListMataPelajaranAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NilaiSementara> nilaiSementara;
    private Context context;

    public ListMataPelajaranAdapter(Context context){
        this.nilaiSementara = new ArrayList<>();
        this.context        = context;
    }

    public void refreshAdapter(List<NilaiSementara> nilaiSementara){
        this.nilaiSementara.clear();
        this.nilaiSementara.addAll(nilaiSementara);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        final View view         = inflater.inflate(R.layout.item_list_nilai_sementara, viewGroup, false);
        return new NilaiItem(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        NilaiItem nilaiItem                     = (NilaiItem) viewHolder;
        final NilaiSementara nilaiSementaras    = nilaiSementara.get(position);
        int nilai                               = Math.round(Float.valueOf(nilaiSementaras.getNilai()));
        nilaiItem.noTxt.setText(String.valueOf(nilaiSementaras.getNomor()));
        nilaiItem.matapelajaranTxt.setText(nilaiSementaras.getMatapelajaran());
        nilaiItem.nilaiTxt.setText(String.valueOf(nilai));

        nilaiItem.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if( isLongClick ){
                    Intent nilaiDetail = NilaiDetailAkademikActivity.createIntent(context);
                    nilaiDetail.putExtra(Config.KEY_PARAM_NIS, nilaiSementaras.getNis());
                    nilaiDetail.putExtra(Config.KEY_PARAM_NAMA_SISWA, nilaiSementaras.getNama());
                    nilaiDetail.putExtra(Config.KEY_PARAM_SEMESTER, nilaiSementaras.getSemester());
                    nilaiDetail.putExtra(Config.KEY_PARAM_AJARAN, nilaiSementaras.getAjaran());
                    nilaiDetail.putExtra(Config.KEY_PARAM_IDMATPEL, nilaiSementaras.getId_matpel());
                    nilaiDetail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(nilaiDetail);
                }
                else {
                    Intent nilaiDetail = NilaiDetailAkademikActivity.createIntent(context);
                    nilaiDetail.putExtra(Config.KEY_PARAM_NIS, nilaiSementaras.getNis());
                    nilaiDetail.putExtra(Config.KEY_PARAM_NAMA_SISWA, nilaiSementaras.getNama());
                    nilaiDetail.putExtra(Config.KEY_PARAM_SEMESTER, nilaiSementaras.getSemester());
                    nilaiDetail.putExtra(Config.KEY_PARAM_AJARAN, nilaiSementaras.getAjaran());
                    nilaiDetail.putExtra(Config.KEY_PARAM_IDMATPEL, nilaiSementaras.getId_matpel());
                    nilaiDetail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(nilaiDetail);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return nilaiSementara.size();
    }

    class NilaiItem extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        private TextView noTxt;
        private TextView matapelajaranTxt;
        private TextView nilaiTxt;

        private Context context;
        private ItemClickListener itemClickListener;

        public NilaiItem(@NonNull View itemView, Context context) {
            super(itemView);
            this.context = context;

            noTxt               = (TextView) itemView.findViewById(R.id.noTxt);
            matapelajaranTxt    = (TextView) itemView.findViewById(R.id.matapelajaranTxt);
            nilaiTxt            = (TextView) itemView.findViewById(R.id.nilaiTxt);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }
}
