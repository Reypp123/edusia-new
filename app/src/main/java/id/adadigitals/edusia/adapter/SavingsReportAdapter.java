package id.adadigitals.edusia.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.data.TransaksiTabungan;

public class SavingsReportAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<TransaksiTabungan> savingsReportList;

    public SavingsReportAdapter(){
        this.savingsReportList = new ArrayList<>();
    }

    public void refreshAdapter(List<TransaksiTabungan> savingsReportList){
        this.savingsReportList.clear();
        addData(savingsReportList);
    }

    public void addData(List<TransaksiTabungan> savingsReportList){
        this.savingsReportList.addAll(savingsReportList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SavingsItem(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_saving, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SavingsItem eventItem = (SavingsItem) holder;
        TransaksiTabungan transaksiTabungan = savingsReportList.get(position);

        String debit = !StringComponent.isNull(transaksiTabungan.getDebit())?"Rp. " + StringComponent.formatThousands(transaksiTabungan.getDebit()):"--";
        String credit = !StringComponent.isNull(transaksiTabungan.getKredit())?"Rp. " + StringComponent.formatThousands(transaksiTabungan.getKredit()):"--";
        String balance = !StringComponent.isNull(transaksiTabungan.getSaldo())?"Rp. " + StringComponent.formatThousands(transaksiTabungan.getSaldo()):"--";

        eventItem.debitTxt.setText(debit);
        eventItem.creditTxt.setText(credit);
        eventItem.balanceTxt.setText(balance);
        eventItem.dateTxt.setText(transaksiTabungan.getTgl());

    }

    @Override
    public int getItemCount() {
        return savingsReportList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    class SavingsItem extends RecyclerView.ViewHolder {

        TextView debitTxt;
        TextView creditTxt;
        TextView dateTxt;
        TextView balanceTxt;

        public SavingsItem(View view) {
            super(view);
            debitTxt = (TextView) view.findViewById(R.id.debitTxt);
            creditTxt = (TextView) view.findViewById(R.id.creditTxt);
            dateTxt = (TextView) view.findViewById(R.id.dateTxt);
            balanceTxt = (TextView) view.findViewById(R.id.balanceTxt);
        }

    }
}
