package id.adadigitals.edusia.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.KehadiranItem;

public class CalendarKehadiranAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CALENDAR_HEADER = 0;
    private static final int TYPE_CALENDAR_ITEM = 1;
    private static final int TYPE_CALENDAR_FOOTER = 2;

    OnMonthChangedListener onMonthChangedListener;
    View calendarHeader;

    public List<KehadiranItem> kehadiran = new ArrayList<KehadiranItem>();

    public int sumHadir;
    public int sumIzin;
    public int sumSakit;
    public int sumAbsen;

    public CalendarKehadiranAdapter(View calendarHeader){
        this.calendarHeader = calendarHeader;
    }

    public void setSumKehadiran(int sumHadir, int sumIzin, int sumSakit, int sumAbsen){
        this.sumHadir = sumHadir;
        this.sumIzin  = sumIzin;
        this.sumSakit = sumSakit;
        this.sumAbsen = sumAbsen;
    }

    public void setOnMonthChangedListener(OnMonthChangedListener monthChangedListener){
        this.onMonthChangedListener = monthChangedListener;
    }

    public void refreshKehadiran(List<KehadiranItem> kehadiran){
        if( kehadiran != null ){
            this.kehadiran.clear();
            this.kehadiran.addAll(kehadiran);
        }
        else {
            this.kehadiran.clear();
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
//        Config.getInstance().addLogs("position --> " + position);
        if( isPositionHeader(position) ){
            return TYPE_CALENDAR_HEADER;
        }

        return TYPE_CALENDAR_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        return kehadiran.size()+2;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if( viewHolder instanceof CalendarHeader ){

        }
        else if ( viewHolder instanceof CalendarItem){
            CalendarItem calendarItem = (CalendarItem) viewHolder;
            calendarItem.tvCountAbsen.setText(String.valueOf(sumAbsen));
            calendarItem.tvCountHadir.setText(String.valueOf(sumHadir));
            calendarItem.tvCountSakit.setText(String.valueOf(sumSakit));
            calendarItem.tvCountIzin.setText(String.valueOf(sumIzin));
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if( viewType == TYPE_CALENDAR_HEADER ){
            return new CalendarHeader(calendarHeader);
        }
        else{
            return new CalendarItem(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.list_kehadiran_item, viewGroup, false));
        }
    }

    public class CalendarHeader extends RecyclerView.ViewHolder {

        MaterialCalendarView calendarView;

        public CalendarHeader(View view) {
            super(view);
            calendarView = (MaterialCalendarView) view.findViewById(R.id.calendarView);
            if(onMonthChangedListener!=null){
                calendarView.setOnMonthChangedListener(onMonthChangedListener);
            }
        }

        public MaterialCalendarView getCalendarView(){
            return calendarView;
        }
    }

    public class CalendarItem extends RecyclerView.ViewHolder{

        TextView tvCountHadir;
        TextView tvCountIzin;
        TextView tvCountSakit;
        TextView tvCountAbsen;

        public CalendarItem(View view){
            super(view);
            tvCountHadir = (TextView) view.findViewById(R.id.tvCountHadir);
            tvCountIzin  = (TextView) view.findViewById(R.id.tvCountIzin);
            tvCountSakit = (TextView) view.findViewById(R.id.tvCountSakit);
            tvCountAbsen = (TextView) view.findViewById(R.id.tvCountAbsen);
        }
    }
}
