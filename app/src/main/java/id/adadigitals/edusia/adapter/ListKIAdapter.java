package id.adadigitals.edusia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.NilaiDetail;
import id.adadigitals.edusia.xui.NilaiKDAkademikActivity;

public class ListKIAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<NilaiDetail> nilaiDetail;

    public ListKIAdapter(Context context){
        this.nilaiDetail    = new ArrayList<>();
        this.context        = context;
    }

    public void refreshAdapter(List<NilaiDetail> nilaiDetails){
        this.nilaiDetail.clear();
        this.nilaiDetail.addAll(nilaiDetails);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        final View view = inflater.inflate(R.layout.item_list_standard, viewGroup, false);
        return new KiItem(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        KiItem kiItem           = (KiItem) viewHolder;
        final NilaiDetail nilai = nilaiDetail.get(position);

        kiItem.name.setText(nilai.getKi());

        kiItem.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if( isLongClick ){
                    Intent nilaiKD = NilaiKDAkademikActivity.createIntent(context);
                    nilaiKD.putExtra(Config.KEY_PARAM_NIS, nilai.getNis());
                    nilaiKD.putExtra(Config.KEY_PARAM_NAMA_SISWA, nilai.getNama());
                    nilaiKD.putExtra(Config.KEY_PARAM_SEMESTER, nilai.getSemester());
                    nilaiKD.putExtra(Config.KEY_PARAM_AJARAN, nilai.getAjaran());
                    nilaiKD.putExtra(Config.KEY_PARAM_IDMATPEL, nilai.getId_matpel());
                    nilaiKD.putExtra(Config.KEY_PARAM_KI, nilai.getId_ki());
                    nilaiKD.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(nilaiKD);
                }
                else {
                    Intent nilaiKD = NilaiKDAkademikActivity.createIntent(context);
                    nilaiKD.putExtra(Config.KEY_PARAM_NIS, nilai.getNis());
                    nilaiKD.putExtra(Config.KEY_PARAM_NAMA_SISWA, nilai.getNama());
                    nilaiKD.putExtra(Config.KEY_PARAM_SEMESTER, nilai.getSemester());
                    nilaiKD.putExtra(Config.KEY_PARAM_AJARAN, nilai.getAjaran());
                    nilaiKD.putExtra(Config.KEY_PARAM_IDMATPEL, nilai.getId_matpel());
                    nilaiKD.putExtra(Config.KEY_PARAM_KI, nilai.getId_ki());
                    nilaiKD.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(nilaiKD);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return nilaiDetail.size();
    }

    class KiItem extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        public TextView name;
        public Context context;
        public ItemClickListener itemClickListener;

        public KiItem(View view, Context context){

            super(view);
            this.context    = context;
            name            = (TextView) view.findViewById(R.id.name);

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true);
            return true;
        }
    }
}
