package id.adadigitals.edusia.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import id.adadigitals.edusia.R;
import java.util.ArrayList;
import java.util.List;

import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.PosInvoice;

public class PosInvoiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public List<PosInvoice> posInvoiceList;

    public PosInvoiceAdapter(){
        this.posInvoiceList = new ArrayList<>();
    }

    public void refreshAdapter(List<PosInvoice> posInvoiceList){
        this.posInvoiceList.clear();
        addData(posInvoiceList);
    }

    public void addData(List<PosInvoice> posInvoiceList){
        this.posInvoiceList.addAll(posInvoiceList);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InvoiceItem(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_invoice_new, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        InvoiceItem eventItem = (InvoiceItem) holder;
        PosInvoice posInvoice = posInvoiceList.get(position);

        eventItem.dateTxt.setText(posInvoice.invoice.getTanggal());
        eventItem.invoiceNoTxt.setText(Config.getInstance().getString(R.string.label_invoice_no)+posInvoice.invoice.getNomorNota());

        String products = "";
        long grandTotal = 0;
        for(int i=0; i<posInvoice.detail_transaksi.size();i++){
            grandTotal = grandTotal+Long.parseLong(posInvoice.detail_transaksi.get(i).total);
            String subtotal = "Rp. "+posInvoice.detail_transaksi.get(i).total;
            String item = "\t\u25CF "+StringComponent.ellipsize(posInvoice.detail_transaksi.get(i).nama_barang+"", 18) + " x" + posInvoice.detail_transaksi.get(i).jumlah_beli+"\t\t"+subtotal;
            products = products+item;
            if(i+1 < posInvoice.detail_transaksi.size())
                products = products+"\n";
        }
        eventItem.productsTxt.setText(products);

        String total = "Rp. " + StringComponent.formatThousands(grandTotal+"");
        eventItem.totalTxt.setText(total);
    }

    @Override
    public int getItemCount() {
        return posInvoiceList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    class InvoiceItem extends RecyclerView.ViewHolder {

        TextView dateTxt;
        TextView invoiceNoTxt;
        TextView productsTxt;
        TextView totalTxt;

        public InvoiceItem(View view) {
            super(view);
            dateTxt = (TextView) view.findViewById(R.id.dateTxt);
            invoiceNoTxt = (TextView) view.findViewById(R.id.invoiceNoTxt);
            productsTxt = (TextView) view.findViewById(R.id.productsTxt);
            totalTxt = (TextView) view.findViewById(R.id.totalTxt);
        }

    }
}
