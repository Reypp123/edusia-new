package id.adadigitals.edusia.func;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.os.Handler;
import android.util.Log;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.CipherComponent;
import id.adadigitals.edusia.component.DummyComponent;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.data.JakSchoolDatabase;
import id.adadigitals.edusia.data.Student;
import id.adadigitals.edusia.data.wallet.WalletBankDKI;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class Function extends Application {
    private final ExecutorService backgroundExecutor;
    private final Handler handler;
    private SharedPreferences pref;
    private JakSchoolDatabase mDatabase;

    public Function() {
        handler = new Handler();
        backgroundExecutor = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Background executor service");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mDatabase = Room.databaseBuilder(getApplicationContext(), JakSchoolDatabase.class,"edusia.db").build();
    }

    public void runInBackground(Runnable runnable){
        backgroundExecutor.execute(runnable);
    }

    public synchronized JakSchoolDatabase getDatabase(){
        return mDatabase;
    }

    public boolean setSelectedSubscriberId(int subsId){
        synchronized (pref) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(Config.SESS_SELECT_SIM, subsId);
            return editor.commit();
        }
    }

    public void setPrefVerificationCode(String verificationCode){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_VERIFICATION_SECRET, DummyComponent.goHigh(verificationCode));
            editor.commit();
        }
    }

    public void setVerificationStatus(int status){
        synchronized (pref) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(Config.SESS_VERIFICATION_STATUS, status);
            if(status == Config.VERIFICATION_SMS_SENT)
                editor.putLong(Config.SESS_VERIFICATION_STATUS_TIMESTAMP, System.currentTimeMillis());
            editor.commit();
        }
    }

    public void setOnboardingStatus( int OnboardingStatus ){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(Config.SESS_STATUS_ONBOARDING, OnboardingStatus);
            editor.commit();
        }
    }

    public void setSessionLogin(String username, String password){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_USERNAME, username);
            editor.putString(Config.SESS_PASSWORD, password);
            editor.commit();
        }
    }

    public void setSessionStatusVerified(Boolean status){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(Config.SESS_STATUS_VERIFIED, status);
            editor.commit();
        }
    }

    public void setSessionUsernamegetOnboardingStatuse(String username){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_USERNAME,username);
            editor.commit();
        }
    }

    public void setSessionFirstLaunch(Boolean isFirstLaunch){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(Config.SESSNAME_FIRST_LAUNCH,isFirstLaunch);
            editor.commit();
        }
    }

    public void setSessionDataLogin(String nik, String nohp){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_NIK, nik);
            editor.putString(Config.SESS_NOHP, nohp);
            editor.commit();
        }
    }

    public void setSessionEncryptSecret(String encryptSecret){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_ENCRYPT_SECRET, encryptSecret);
            editor.commit();
        }
    }

    public void setSessionWalletRetry(int tryCount){
        synchronized (pref) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt(Config.SESS_WALLET_RETRY_COUNT, tryCount);
            editor.commit();
        }
    }

    public void setSessSelectedSemester(String semester){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_SEMESTER_SELECT, semester);
            editor.commit();
        }
    }

    public void setSessSelectedTahun(String tahunAkademik){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_TAHUN_SELECT, tahunAkademik);
            editor.commit();
        }
    }

    public void setSessionAccountNumberWallet(String accountNumberWallet){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_WALLET_ACCOUNT_NUMBER, accountNumberWallet);
            editor.commit();
        }
    }

    public void setSessionAccountUsernameWallet(String usernameWallet){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_WALLET_ACCOUNT_USERNAME, usernameWallet);
            editor.commit();
        }
    }

    public void setSessionAccountIDWallet(String id){
        synchronized (pref){
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(Config.SESS_WALLET_ACCOUNT_ID, id);
            editor.commit();
        }
    }

    public String getSessionAccountIDWallet(){
        return pref.getString(Config.SESS_WALLET_ACCOUNT_ID, null);
    }

    public String getSessionSemester(){
        return pref.getString(Config.SESS_SEMESTER_SELECT, "1");
    }

    public String getSessionTahunAkademik(){
        return pref.getString(Config.SESS_TAHUN_SELECT, "2018/2019");
    }

    public String getSessionAccountNumberWallet(){
        return pref.getString(Config.SESS_WALLET_ACCOUNT_NUMBER, null);
    }

    public String getSessionAccountUsernameWallet(){
        return pref.getString(Config.SESS_WALLET_ACCOUNT_USERNAME, null);
    }

    public String getPrefVerificationCode(){
        String secret = pref.getString(Config.SESS_VERIFICATION_SECRET, null);
        if(!StringComponent.isNull(secret)){
            return DummyComponent.goLow(secret);
        }
        return null;
    }

    public int getVerificationStatus(){
        int status = pref.getInt(Config.SESS_VERIFICATION_STATUS, Config.VERIFICATION_PENDING);
        long timestamp = pref.getLong(Config.SESS_VERIFICATION_STATUS_TIMESTAMP, -1);
        if(status == Config.VERIFICATION_SMS_SENT &&
                (timestamp == -1 || ((System.currentTimeMillis() - timestamp) > 1000*100)))
            return Config.VERIFICATION_FAILED;
        else
            return status;
    }

    public boolean isVerificationInProgress(){
        int status = getVerificationStatus();
        if(status == Config.VERIFICATION_SMS_SENT || status == Config.VERIFICATION_INPROGRESS ||
                status == Config.VERIFICATION_FAILED)
            return true;
        else
            return false;
    }

    public String getPhotoUrl(String filename){
        if(StringComponent.isNull(filename)){
            return null;
        }

        String urlPhoto = checkUrlApiPhotoStatus();
        return urlPhoto + filename;
    }

    public void resetUserVerifikasi(){
        runInBackground(new Runnable() {
            @Override
            public void run() {
                mDatabase.clearAllTables();
            }
        });
        synchronized (pref) {
            SharedPreferences.Editor editor = pref.edit();
            editor.clear();
            editor.commit();
        }
    }

    public String checkUrlApiPhotoStatus(){
//        return ( Config.DEBUG_MODE == true ) ? Config.URL_PHOTO_TEMPLATE_DEVEL : Config.URL_PHOTO_TEMPLATE_PROD;
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_PHOTO_TEMPLATE_DEVEL : Config.URL_PHOTO_TEMPLATE_PROD;
    }

    public String checkUrlApiStatus(){
//        return ( Config.DEBUG_MODE == true ) ? Config.URL_API_DEVEL : Config.URL_API_PRODUCTION;
        return (Config.STATUS_DEVEL == "DEVEL") ? Config.URL_API_DEVEL : Config.URL_API_PRODUCTION;
    }

    public String getSessionPassword(){
        return pref.getString(Config.SESS_PASSWORD, null);
    }

    public String getSessEncryptSecret(){
        return pref.getString(Config.SESS_ENCRYPT_SECRET, null);
    }

    public String getSessionPasswordAuth(){
        return pref.getString(Config.SESS_PASSWORD, Config.TOKEN_SECRET);
    }

    public String getSessionUsername(){
        return pref.getString(Config.SESS_USERNAME, null);
    }

    public String getSessionPhone() {
        return pref.getString(Config.SESS_NOHP,null);
    }

    public String getSessionNIK(){
        return pref.getString(Config.SESS_NIK, null);
    }

    public String getSessionTokenSecret(){
        return pref.getString(Config.SESS_TOKENSECRET, null);
    }

    public int getSessionWalletRetry(){
        return pref.getInt(Config.SESS_WALLET_RETRY_COUNT, 0);
    }

    public int getSelectedSubscriberId(){
        return pref.getInt(Config.SESS_SELECT_SIM, -1);
    }

    public boolean getIsFirstLaunch(){
        return pref.getBoolean(Config.SESSNAME_FIRST_LAUNCH, Config.DEFAULT_FIRST_LAUNCH);
    }

    public String getFcmToken(){
        return pref.getString(Config.SESS_FCM_KEY, null);
    }

    public int getOnboardingStatus(){
        return pref.getInt(Config.SESS_STATUS_ONBOARDING, Config.ONBOARDING_VERIFICATION);
    }

    public File getOrtuFace(String filename){
        if( StringComponent.isNull(filename) ){
            return null;
        }

        return new File(Config.getInstance().getFolderFaceOrtu(), filename);
    }

    public File getDirectory(){
        File dir = null;
        try{
            if(getFilesDir()!=null && getFilesDir().canWrite())
                dir = new File(getFilesDir(), getString(R.string.app_name));
            else{
                dir =  new File(getCacheDir(), getString(R.string.app_name));
            }
            if(!dir.exists())
                dir.mkdirs();
        }catch(Exception e){
            e.printStackTrace();
            if (dir == null) {
                String cacheDirPath = "/data/data/" + getPackageName() + "/cache/";
                dir = new File(cacheDirPath);
            }
        }
        return dir;
    }

    public File getCacheAppDir(){
        File dir = new File(getDirectory(), ".cache");
        if (!dir.exists()){
            dir.mkdirs();
        }

        return dir;
    }

    public File getFolderFace(){
        File dir = new File(getCacheAppDir(), "template_face");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public File getFolderFaceOrtu(){
        File dir = new File(getFolderFace(), "ortu");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public File getFolderFaceStudent(){
        File dir = new File(getFolderFace(), "student");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public File getPhotosDir(){
        File dir = new File(getCacheAppDir(), "photos");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public File getFolderFotoOrtu(){
        File dir = new File(getPhotosDir(), "ortu");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public File getFolderFotoSiswa(){
        File dir = new File(getPhotosDir(), "student");
        if( !dir.exists() ){
            dir.mkdirs();
        }

        return dir;
    }

    public void addLogs(String msg){
        if( Config.DEBUG_MODE ){
            Log.d(Config.NAME_LOG,msg);
        }
    }

    public boolean encryptUsernames(List<Student> students, String pin, WalletBankDKI mWallet) {
        try {
            CipherComponent ciu = new CipherComponent();
            for (Student student : students) {
                String studentEwu = ciu.encrypt(mWallet.getUsername(),
                        student.getStudentTemplateFile());
                String parentEwu = ciu.encrypt(mWallet.getUsername(),
                        student.getParentTemplateFile());
                if (!StringComponent.isNull(studentEwu) && !StringComponent.isNull(parentEwu)) {
                    student.setStudentEncryptedWalletUsername(studentEwu);
                    student.setParentEncryptedWalletUsername(parentEwu);
                } else {
                    return false;
                }
            }

            setSessionEncryptSecret(ciu.encrypt(pin, students.get(0).getParentTemplateFile()));
            return true;
        } catch (Exception e) {
            Config.getInstance().addLogs("error encryptUsernames " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
