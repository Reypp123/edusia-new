package id.adadigitals.edusia.response.json;

import java.util.List;

import id.adadigitals.edusia.data.NilaiSementara;

public class GetNilaiSementaraResponse extends BasicResponse {
    public List<NilaiSementara> data;
}
