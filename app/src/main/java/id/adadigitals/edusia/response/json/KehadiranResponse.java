package id.adadigitals.edusia.response.json;

import java.util.List;

import id.adadigitals.edusia.data.KehadiranItem;

public class KehadiranResponse extends BasicResponse {
     public List<KehadiranItem> data;
}
