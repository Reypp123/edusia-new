package id.adadigitals.edusia.response.json;

import java.util.List;

import id.adadigitals.edusia.data.NilaiDetail;

public class GetDetailNilaiResponse extends BasicResponse {
    public List<NilaiDetail> data;
}
