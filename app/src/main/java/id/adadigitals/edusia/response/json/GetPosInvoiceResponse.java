package id.adadigitals.edusia.response.json;

import id.adadigitals.edusia.data.PosInvoice;

import java.util.List;

import id.adadigitals.edusia.data.PosInvoice;

public class GetPosInvoiceResponse extends BasicResponse {

    public List<PosInvoice> data;

}