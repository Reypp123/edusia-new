package id.adadigitals.edusia.response.json;

import java.util.List;

import id.adadigitals.edusia.data.TahunAkademik;

public class GetListTahunAkademikResponse extends BasicResponse {
    public List<TahunAkademik> data;
}
