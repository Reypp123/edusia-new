package id.adadigitals.edusia.response.json;

import id.adadigitals.edusia.data.TransaksiTabungan;

import java.util.List;

import id.adadigitals.edusia.data.TransaksiTabungan;

public class GetLaporanTabunganResponse extends BasicResponse{
    public List<TransaksiTabungan> data;
}
