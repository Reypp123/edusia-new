package id.adadigitals.edusia.response.json;

public class GetAccessTokenResponse {
    public String access_token;
    public String token_type;
    public Integer expires_in;
}
