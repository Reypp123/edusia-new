package id.adadigitals.edusia.response.json;

public class getUsernameResponse extends BasicResponse {
    public Data data;

    public class Data{
        public String login_username_nik_ortu;
        public String parent_face_template;
        public int nik;
        public int noHp;
    }

    public String getUsername(){
        if(data != null){
            return data.login_username_nik_ortu;
        }
        else{
            return null;
        }
    }

    public String getFaceTemplate(){
        if(data != null){
            return data.parent_face_template;
        }
        else {
            return null;
        }
    }

    public boolean checkNikFound(){
        if( data != null ){
            return data.nik == 1;
        }
        else {
            return false;
        }
    }

    public boolean checkHPFound(){
        if ( data != null ){
            return data.noHp == 1;
        }
        else {
            return false;
        }
    }
}
