package id.adadigitals.edusia.response.json;

import java.util.List;

import id.adadigitals.edusia.data.TimeLine;

public class GetTimeLineResponse extends BasicResponse {
    public List<TimeLine> data;
}
