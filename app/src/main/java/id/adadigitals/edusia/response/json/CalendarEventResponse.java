package id.adadigitals.edusia.response.json;

import id.adadigitals.edusia.data.CalendarEvent;

import java.util.List;

public class CalendarEventResponse extends BasicResponse {
    public List<CalendarEvent> data;
}
