package id.adadigitals.edusia.response.json;

public class getUserResponse extends BasicResponse {
    public String id;
    public String login_username;
    public String login_tipe;
    public String staff_nip;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin_username() {
        return login_username;
    }

    public void setLogin_username(String login_username) {
        this.login_username = login_username;
    }

    public String getLogin_tipe() {
        return login_tipe;
    }

    public void setLogin_tipe(String login_tipe) {
        this.login_tipe = login_tipe;
    }

    public String getStaff_nip() {
        return staff_nip;
    }

    public void setStaff_nip(String staff_nip) {
        this.staff_nip = staff_nip;
    }
}
