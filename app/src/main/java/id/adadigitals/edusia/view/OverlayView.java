package id.adadigitals.edusia.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.innovatrics.commons.geom.Point;

public class OverlayView extends View {

    private Rect[] rectangles = null;
    private Paint[] rectanglesPaints = null;

    private RectF[] ovals = null;
    private Paint[] ovalsPaints = null;

    private String[] texts = null;
    private Paint[] textsPaints = null;
    private Point[] textsPositions = null;


    private Bitmap targetBMP = null;

    public OverlayView(Context c, AttributeSet attr) {
        super(c, attr);
    }

    public void drawRectangles(Rect[] rectangles, Paint[] rectanglesPaints){
        this.rectangles = rectangles;
        this.rectanglesPaints = rectanglesPaints;
        setWillNotDraw(false);
        postInvalidate();
    }

    public void drawOvals(RectF[] ovals, Paint[] ovalsPaints) {
        this.ovals = ovals;
        this.ovalsPaints = ovalsPaints;
        setWillNotDraw(false);
        postInvalidate();
    }

    public void drawTexts(String[] texts, Point[] textsPositions, Paint[] textsPaints) {
        this.texts = texts;
        this.textsPositions = textsPositions;
        this.textsPaints = textsPaints;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if ( targetBMP != null ) {
            canvas.drawBitmap(targetBMP, null, new Rect(0,0, getWidth(), getHeight()), null);
        }


        if(rectangles  != null){
            for(int i = 0; i < rectangles.length; i++) {
                canvas.drawRect(rectangles[i], rectanglesPaints[i]);
            }
        }

        if (ovals != null) {
            for(int i = 0; i < ovals.length; i++) {
                canvas.drawOval(ovals[i], ovalsPaints[i]);
            }
        }

        if (texts != null) {
            for(int i = 0; i < texts.length; i++) {
                canvas.drawText(texts[i], textsPositions[i].getX(), textsPositions[i].getY(), textsPaints[i]);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
