package id.adadigitals.edusia.view;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;

public class CalendarKehadiranDecorator implements DayViewDecorator {

    private final Drawable highlightDrawable;
    private final String color;
    private final HashSet<CalendarDay> dates;

    public CalendarKehadiranDecorator(String color, Collection<CalendarDay> dates) {
        this.color = color;
        this.dates = new HashSet<>(dates);
        this.highlightDrawable = new ColorDrawable(Color.parseColor(color));
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
//        view.addSpan(new DotSpan(8, Color.parseColor(color)));
        view.setBackgroundDrawable(highlightDrawable);
    }
}
