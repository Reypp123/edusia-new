package id.adadigitals.edusia.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;

import id.adadigitals.edusia.config.Config;

public class VerifikasiNewPinService extends Service {

    final int VERIFY_RETRY_COUNT = 10;
    final int VERIFY_INTERVAL_SECS = 10;

    VerificationTask verificationTask;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Config.getInstance().addLogs("================ service has been started ========== ");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if( verificationTask != null ){
            verificationTask.cancel(true);
            verificationTask = null;
        }

        verificationTask = new VerificationTask();
        verificationTask.execute();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if( verificationTask != null ){
            verificationTask.cancel(true);
            verificationTask = null;
        }
        Config.getInstance().addLogs("Stopping verification service!!! ========================== ");
        super.onDestroy();
    }

    public class VerificationTask extends AsyncTask<Void, Integer, Integer> {

        boolean start = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Config.getInstance().setVerificationStatus(Config.VERIFICATION_INPROGRESS_RESET_PIN);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Intent intent = new Intent(Config.INTENT_VERIFICATION_TIMER);
            intent.putExtra("TIMER", values[0]);
            intent.putExtra("START", start);
            sendBroadcast(intent);
            start = false;
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                int i = (VERIFY_RETRY_COUNT*VERIFY_INTERVAL_SECS) - 1;
                while (Config.getInstance().getVerificationStatus() == Config.VERIFICATION_INPROGRESS_RESET_PIN &&
                        i > 0) {
                    i--;
                    publishProgress(i);
                    Thread.sleep(1000);
                }

            } catch (Exception e) {
                Config.getInstance().addLogs("Error --> " + e.getMessage());
                return null;
            }

            return Config.getInstance().getVerificationStatus();
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if(result != Config.VERIFICATION_SUCCESS_RESET_PIN){
                Config.getInstance().setVerificationStatus(Config.VERIFICATION_FAILED_RESET_PIN);
                Intent intent = new Intent(Config.INTENT_VERIFICATION_FAILED);
                sendBroadcast(intent);
                stopSelf();
            }else{
                Intent intent = new Intent(Config.INTENT_VERIFICATION_SUCCESS);
                sendBroadcast(intent);
                stopSelf();
            }
        }
    }
}
