package id.adadigitals.edusia.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.xui.BerandaActivity;

public class FcmMessagingService extends FirebaseMessagingService {

    private static int VIBRATION_TIME = 600; // in millisecond
    private SharedPreferences pref;

    @Override
    public void onNewToken(String s) {
        Config.getInstance().addLogs("new token --> " + s);
        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Config.SESS_FCM_KEY, s);
        editor.commit();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if( remoteMessage.getData() != null ){
            showNotification(remoteMessage.getData().get("message"),remoteMessage.getData().get("title"));
        }
    }

    private void showNotification(String messageBody, String title){
        Intent intent = new Intent(this,BerandaActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,(int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId        = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri     = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId);
        builder.setContentTitle(title);
        builder.setContentText(messageBody);
        builder.setSmallIcon(R.drawable.icon_notification);
        builder.setDefaults(Notification.DEFAULT_LIGHTS);
        builder.setContentIntent(pendingIntent);
        builder.setSound(defaultSoundUri);
        builder.setAutoCancel(true);
        builder.setPriority(Notification.PRIORITY_HIGH);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_LOW);
            notificationManager.createNotificationChannel(channel);
        }

        int unique_id = (int) System.currentTimeMillis();
        notificationManager.notify(unique_id, builder.build());
        playVibration();
    }

    private void playVibration(){
        try{
            ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VIBRATION_TIME);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
