package id.adadigitals.edusia.service;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.api.ApiStudo;
import id.adadigitals.edusia.config.Config;
import id.adadigitals.edusia.response.json.BasicResponse;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VerifikasiUserService extends Service {

    final int VERIFY_RETRY_COUNT = 10;
    final int VERIFY_INTERVAL_SECS = 10;

    VerificationServiceReceiver verificationReceiver = null;
    boolean smsSending = false;
    static VerificationTask verifyTask;
    static SMSSendTask smsSendTask;
    static SendSMSAPI sendSMSAPI;

    int selectedSimInfo = -1;
    List<SubscriberInfo> subscribers = new ArrayList<SubscriberInfo>();

    public class SubscriberInfo{
        int subId;
        String CarrierName;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Config.getInstance().addLogs("================ service has been started ========== ");

        if( Config.FEATURE_SMS_OTP_BANKDKI ){

        }
        else {
            subscribers.clear();
            if( Config.SDK_INT >= 22 ){
                SubscriptionManager subscriptionManager = SubscriptionManager.from(getApplicationContext());
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                if( subscriptionInfoList != null ){
                    for(SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
                        SubscriberInfo simInfo = new SubscriberInfo();
                        simInfo.subId = subscriptionInfo.getSubscriptionId();
                        simInfo.CarrierName = subscriptionInfo.getCarrierName().toString();
                        subscribers.add(simInfo);
                    }
                }
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        selectedSimInfo = intent.getIntExtra(Config.PARAM_SELECT_SIM, -1);

        if( selectedSimInfo >= 0 ){
            Config.getInstance().setSelectedSubscriberId(selectedSimInfo);
        }
        else {
            selectedSimInfo = Config.getInstance().getSelectedSubscriberId();
        }

        if( Config.getInstance().getVerificationStatus() == Config.VERIFICATION_INPROGRESS
                || Config.getInstance().getVerificationStatus() == Config.VERIFICATION_SMS_SENT){
            Config.getInstance().addLogs("================ VERIFICATION INPROGRESS OR VERIFICATION_SMS_SENT ========== ");
            if(verifyTask == null){
                verifyTask = new VerificationTask();
                verifyTask.execute();
            }else{
                if(verifyTask!=null) {
                    verifyTask.cancel(true);
                    verifyTask = null;
                }
                if(Config.FEATURE_SMS_OTP_BANKDKI) {
                    if( sendSMSAPI != null ){
                        sendSMSAPI.cancel(true);
                        sendSMSAPI = null;
                    }

                    sendSMSAPI = new SendSMSAPI();
                    sendSMSAPI.execute();

                }else{
                    if (smsSendTask != null) {
                        smsSendTask.cancel(true);
                        smsSendTask = null;
                    }
                    smsSendTask = new SMSSendTask();
                    smsSendTask.execute();
                }
            }
        }
        else if(!smsSending){
            Config.getInstance().addLogs("================ smsSending ========== " + smsSending);
            if(verifyTask!=null) {
                verifyTask.cancel(true);
                verifyTask = null;
            }

            if(Config.FEATURE_SMS_OTP_BANKDKI) {
                if( sendSMSAPI != null ){
                    sendSMSAPI.cancel(true);
                    sendSMSAPI = null;
                }

                sendSMSAPI = new SendSMSAPI();
                sendSMSAPI.execute();
            }
            else {
                if (smsSendTask != null) {
                    smsSendTask.cancel(true);
                    smsSendTask = null;
                }
                smsSendTask = new SMSSendTask();
                smsSendTask.execute();
            }
        }

        return START_STICKY;
    }

    public class SendSMSAPI extends AsyncTask<Void, Void, Boolean>{

        ApiStudo apiStudo = new ApiStudo();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            verificationReceiver = new VerificationServiceReceiver();
            verificationReceiver.register();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            BasicResponse basicResponse = apiStudo.kirimOtp( Config.getInstance().getSessionPhone() );
            if( basicResponse == null ){
                return false;
            }
            else if( basicResponse != null ){
                if( basicResponse.rc == 0 ){
                    return false;
                }
                else {
                    smsSending = true;
                    Intent intent = new Intent(Config.INTENT_SENT);
                    sendBroadcast(intent);
                }
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if( !aBoolean ){
                Config.getInstance().setVerificationStatus(Config.VERIFICATION_PENDING);
                Intent intent = new Intent(Config.INTENT_VERIFICATION_SMS_UNSENT);
                sendBroadcast(intent);
                stopSelf();
            }
        }
    }

    public class SMSSendTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            verificationReceiver = new VerificationServiceReceiver();
            verificationReceiver.register();
            Intent intent = new Intent(Config.INTENT_VERIFICATION_SMS_IN_PROGRESS);
            sendBroadcast(intent);

            SmsRetrieverClient client = SmsRetriever.getClient(VerifikasiUserService.this);
            Task<Void> task = client.startSmsRetriever();
            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Config.getInstance().addLogs("error --> " + e.getMessage());
                }
            });
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                String smsNumber = Config.getInstance().getSessionPhone();
                Random r = new Random();
                String vCode = "";
                while(vCode.length()<6) {
                    vCode = vCode + (r.nextInt(8) + 1);
                }
                String message = "<#> "+ Config.getInstance().getString(R.string.desc_sms_verification)+" "+vCode+" "+ Config.getInstance().getString(R.string.app_sms_hash);
                PendingIntent sentPI = PendingIntent.getBroadcast(VerifikasiUserService.this, 0,
                        new Intent(Config.INTENT_SENT), 0);
                PendingIntent deliveredPI = PendingIntent.getBroadcast(VerifikasiUserService.this, 0,
                        new Intent(Config.INTENT_DELIVERED), 0);
                if(subscribers.size() > 1 && selectedSimInfo >= 0 && Config.SDK_INT >= 22){
                    SmsManager sms = SmsManager.getSmsManagerForSubscriptionId(subscribers.get(selectedSimInfo).subId);
                    sms.sendTextMessage(smsNumber, null, message, sentPI, deliveredPI);
                }else{
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(smsNumber, null, message, sentPI, deliveredPI);
                }
                Config.getInstance().setPrefVerificationCode(vCode);
                smsSending = true;
                Intent intent = new Intent(Config.INTENT_SENT);
                sendBroadcast(intent);
            }catch(Exception e){
                Config.getInstance().addLogs("Error --> " + e.getMessage());
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            /*if(AppGlobals.DEBUG_MODE){
                Logr.d("============= DUMMY VERIFICATION SMS SENT ");
                AppGlobals.getInstance().setVerificationStatus(AppGlobals.VERIFICATION_SMS_SENT);
                Intent dummySentIntent = new Intent(AppGlobals.INTENT_VERIFICATION_SMS_SENT);
                sendBroadcast(dummySentIntent);
                verifyTask = new VerificationTask();
                verifyTask.execute();
            } else*/ if(!result) {
                Config.getInstance().setVerificationStatus(Config.VERIFICATION_PENDING);
                Intent intent = new Intent(Config.INTENT_VERIFICATION_SMS_UNSENT);
                sendBroadcast(intent);
                stopSelf();
            }
        }
    }

    @Override
    public void onDestroy() {
        Config.getInstance().addLogs("Stopping verification service!!! ========================== ");
        if(verificationReceiver!=null){
            unregisterReceiver(verificationReceiver);
            verificationReceiver = null;
        }
        if(verifyTask!=null) {
            verifyTask.cancel(true);
            verifyTask = null;
        }
        if(smsSendTask!=null) {
            smsSendTask.cancel(true);
            smsSendTask = null;
        }

        if( sendSMSAPI != null ){
            sendSMSAPI.cancel(true);
            smsSendTask = null;
        }

        super.onDestroy();
    }

    public class VerificationTask extends AsyncTask<Void, Integer, Integer> {

        boolean start = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Config.getInstance().setVerificationStatus(Config.VERIFICATION_INPROGRESS);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Intent intent = new Intent(Config.INTENT_VERIFICATION_TIMER);
            intent.putExtra("TIMER", values[0]);
            intent.putExtra("START", start);
            sendBroadcast(intent);
            start = false;
            super.onProgressUpdate(values);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                int i = (VERIFY_RETRY_COUNT*VERIFY_INTERVAL_SECS) - 1;
                while (Config.getInstance().getVerificationStatus() == Config.VERIFICATION_INPROGRESS &&
                        i > 0) {
                    i--;
                    publishProgress(i);
                    Thread.sleep(1000);
                }

            } catch (Exception e) {
                Config.getInstance().addLogs("Error --> " + e.getMessage());
                return null;
            }

            return Config.getInstance().getVerificationStatus();
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if(result != Config.VERIFICATION_SUCCESS){
                Config.getInstance().setVerificationStatus(Config.VERIFICATION_FAILED);
                Intent intent = new Intent(Config.INTENT_VERIFICATION_FAILED);
                sendBroadcast(intent);
                stopSelf();
            }else{
                Intent intent = new Intent(Config.INTENT_VERIFICATION_SUCCESS);
                sendBroadcast(intent);
                stopSelf();
            }
        }
    }

    public class VerificationServiceReceiver extends BroadcastReceiver {

        public void register(){
            IntentFilter filters = new IntentFilter();
            filters.addAction(Config.INTENT_SENT);
            try {
                registerReceiver(this, filters);
            } catch (Exception e) {
                Config.getInstance().addLogs("Error register verification service receiver --> " + e.getMessage());
            }
        }

        public void unregister(){
            try {
                unregisterReceiver(this);
            }catch (Exception e) {
                Config.getInstance().addLogs("Error unregister verification service receiver --> " + e.getMessage());
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Config.getInstance().addLogs("Masuk ke onReceive ");
            if(intent.getAction().equals(Config.INTENT_SENT) && smsSending){
                switch (getResultCode()){
                    case Activity.RESULT_OK:
                        Config.getInstance().addLogs("RESULT_OK ");
                        Config.getInstance().setVerificationStatus(Config.VERIFICATION_SMS_SENT);
                        Intent smsSentIntent = new Intent(Config.INTENT_VERIFICATION_SMS_SENT);
                        sendBroadcast(smsSentIntent);
                        verifyTask = new VerificationTask();
                        verifyTask.execute();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        if(!Config.DEBUG_MODE) {
                            Config.getInstance().setVerificationStatus(Config.VERIFICATION_PENDING);
                            Intent smsFailedIntent = new Intent(Config.INTENT_VERIFICATION_SMS_UNSENT);
                            sendBroadcast(smsFailedIntent);
                            stopSelf();
                        }
                        break;
                    default:
                        break;
                }

                smsSending = false;
                if(verificationReceiver!=null){
                    unregisterReceiver(verificationReceiver);
                    verificationReceiver = null;
                }
            }
        }

    }
}
