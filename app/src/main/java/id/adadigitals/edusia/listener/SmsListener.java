package id.adadigitals.edusia.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.telephony.SmsMessage;
import android.widget.Toast;

import id.adadigitals.edusia.R;
import id.adadigitals.edusia.component.StringComponent;
import id.adadigitals.edusia.config.Config;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import id.adadigitals.edusia.config.Config;

public class SmsListener extends BroadcastReceiver {
    public static final String pdu_type = "pdus";

    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs;
        String strMessage = "";
        String format = bundle.getString("format");
        Object[] pdus = (Object[]) bundle.get(pdu_type);
        if( pdus != null ){
            msgs    = new SmsMessage[pdus.length];
            for( int i = 0; i < msgs.length; i++ ){

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                }
                else {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }

                strMessage += msgs[i].getMessageBody();
            }

            if( !StringComponent.isNull(strMessage) ){
                if( Config.FEATURE_SMS_OTP_BANKDKI ){
                    String split[] = strMessage.split(" ",0);
                    for(String s:split){
                        Config.getInstance().addLogs("split sms --> " + s);
                    }
                }
                else {
                    String code = strMessage.replace("<#> "+context.getString(R.string.desc_sms_verification),"");
                    code        = code.replace(context.getString(R.string.app_sms_hash),"").trim();
                    if(code.equals(Config.getInstance().getPrefVerificationCode()) && code.length() == 6){
                        Config.getInstance().setVerificationStatus(Config.VERIFICATION_SUCCESS);
                        Config.getInstance().setOnboardingStatus(Config.ONBOARDING_FINISH);
                        Intent receivedCode = new Intent(Config.INTENT_VERIFICATION_SMS_RECEIVED);
                        receivedCode.putExtra(Config.KEY_PARAM_VCODE, code);
                        context.sendBroadcast(receivedCode);
                    }
                }
            }
        }
    }
}
