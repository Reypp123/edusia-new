package id.adadigitals.edusia.config;

import android.os.Build;

import id.adadigitals.edusia.BuildConfig;
import id.adadigitals.edusia.func.Function;

public class Config extends Function {
    public static final long SPLASH_DURATION = 2500L;
    public static final long SMS_TIMEOUT = 10*1000;

    public static final Boolean DEFAULT_FIRST_LAUNCH                    = true;
    public static final Boolean FACE_RECOGNITION                        = false;

    public static final String SESS_STATUS_ONBOARDING                   = "onboarding_status";
    public static final String SESS_USERNAME                            = "sess_username";
    public static final String SESS_PASSWORD                            = "sess_password";
    public static final String SESS_NIK                                 = "sess_nik";
    public static final String SESS_NOHP                                = "sess_nohp";
    public static final String SESS_TOKENSECRET                         = "sess_tokensecret";
    public static final String SESS_ENCRYPT_SECRET                      = "sess_encrypt_secret";
    public static final String SESS_WALLET_RETRY_COUNT                  = "sess_wallet_retry_count";
    public static final String SESS_SELECT_SIM                          = "sess_wallet_retry_count";
    public static final String SESS_STATUS_VERIFIED                     = "sess_status_verified";
    public static final String SESS_VERIFICATION_STATUS                 = "verification_status";
    public static final String SESS_VERIFICATION_STATUS_TIMESTAMP       = "verification_timestamp";
    public static final String SESS_VERIFICATION_SECRET                 = "verification_secret";
    public static final String SESS_WALLET_ACCOUNT_NUMBER               = "sess_wallet_accountNumber";
    public static final String SESS_WALLET_ACCOUNT_USERNAME             = "sess_wallet_accountUsername";
    public static final String SESS_WALLET_ACCOUNT_ID                   = "sess_wallet_accountID";
    public static final String SESS_FCM_KEY                             = "sess_fcm_key";
    public static final String SESS_SEMESTER_SELECT                     = "sess_semester_select";
    public static final String SESS_TAHUN_SELECT                        = "sess_tahun_select";

    public static final String SESSNAME_FIRST_LAUNCH                    = "setFirstLaunch";
    public static final String URL_API_DEVEL                            = "https://devedusia.adadigitals.com/";
    public static final String URL_API_PRODUCTION                       = "https://edusia.adadigitals.com/";

    public static final String URL_API_DEV_BANKDKI                      = (Config.API_MANAGEMENT_DEV == true) ? "http://bankdkidev.api.mashery.com/" : "http://bankdki.api.mashery.com/";
    public static final String URL_REFRESH_TOKEN_BANKDKI                = URL_API_DEV_BANKDKI + "oauth2";

    public static final String URL_PHOTO_TEMPLATE_DEVEL                 = URL_API_DEVEL + "template/assets/images/user/";
    public static final String URL_PHOTO_TEMPLATE_PROD                  = URL_API_PRODUCTION + "template/assets/images/user/";
    public static final String STATUS_DEVEL                             = "PRODUCTION";

    public static final String INTENT_SENT = "SMS_SENT";
    public static final String INTENT_DELIVERED = "SMS_DELIVERED";

    public static final String INTENT_VERIFICATION_SMS_IN_PROGRESS = BuildConfig.APPLICATION_ID+".xui.verification.SMS_IN_PROGRESS";
    public static final String INTENT_VERIFICATION_SMS_SENT = BuildConfig.APPLICATION_ID+".xui.verification.SMS_SENT";
    public static final String INTENT_VERIFICATION_SMS_UNSENT = BuildConfig.APPLICATION_ID+".xui.verification.SMS_UNSENT";
    public static final String INTENT_VERIFICATION_SMS_RECEIVED = BuildConfig.APPLICATION_ID+".xui.verification.SMS_RECEIVED";
    public static final String INTENT_VERIFICATION_TIMER = BuildConfig.APPLICATION_ID+".xui.verification.TIMER";
    public static final String INTENT_VERIFICATION_SUCCESS = BuildConfig.APPLICATION_ID+".xui.verification.SUCCESS";
    public static final String INTENT_VERIFICATION_FAILED = BuildConfig.APPLICATION_ID+".xui.verification.FAILED";

    public static final String PARAM_SELECT_SIM                         = "param_select_sim";
    public static final String KEY_PARAM_FACE_INDEX                     = "param_face_index";
    public static final String KEY_PARAM_FACE_GROUP                     = "param_face_group";
    public static final String KEY_PARAM_FACE_UID                       = "param_face_uid";
    public static final String KEY_PARAM_PHOTO_PATH                     = "param_photo_path";
    public static final String KEY_PARAM_TEMPLATE_PATH                  = "param_template_path";
    public static final String KEY_PARAM_VCODE                          = "param_verification_code";

    public static final String KEY_PARAM_ACCOUNT    = "param_account";
    public static final String KEY_PARAM_USERNAME   = "param_username";
    public static final String KEY_PARAM_SECRET     = "param_secret";
    public static final String KEY_PARAM_NIS        = "param_nis";
    public static final String KEY_PARAM_NAMA_SISWA = "param_nama_murid";
    public static final String KEY_PARAM_SEMESTER   = "param_semester_murid";
    public static final String KEY_PARAM_AJARAN     = "param_ajaran_murid";
    public static final String KEY_PARAM_IDMATPEL   = "param_id_matpel";
    public static final String KEY_PARAM_KI         = "param_name_ki";

    public static final String NAME_LOG                                 = "__EDUSIA__";

    public static final int SDK_INT                                     = Build.VERSION.SDK_INT;
    public static final boolean DEBUG_MODE                              = BuildConfig.DEBUG;
    public static final int ID_YAYASAN                                  = 2;

    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE        = 10;
    public static final int PERMISSIONS_REQUEST_SEND_SMS                = 11;
    public static final int PERMISSIONS_REQUEST_RECEIVE_SMS             = 12;
    public static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE  = 13;

    public static final int ONBOARDING_VERIFICATION                     = 0;
    public static final int ONBOARDING_TERMS_CONDITION                  = 1;
    public static final int ONBOARDING_SET_PASSWORD                     = 2;
    public static final int ONBOARDING_VERIFIKASI_DATA_MURID            = 3;
    public static final int ONBOARDING_FACE_ENROLLMENT                  = 4;
    public static final int ONBOARDING_VERIFIKASI_PASSWORD              = 5;
    public static final int ONBOARDING_SETUP_WALLET                     = 6;
    public static final int ONBOARDING_VERIFIKASI_SMS_OTP               = 7;
    public static final int ONBOARDING_FINISH                           = 8;

    public static int ORTU_FACE_GROUP                                   = 0;
    public static int MURID_FACE_GROUP                                  = 1;

    public static final int FACE_CAPTURE_INTERRUPTED = 0;
    public static final int FACE_CAPTURE_CAMERA_INIT_FAILED = 1;
    public static final int FACE_CAPTURE_NO_CAMERA_PERMISSION = 2;
    public static final int FACE_CAPTURE_SUCCESS = 3;
    public static final int FACE_CAPTURE_FAIL = 4;
    public static final int FACE_CAPTURE_VERIFIED = 5;
    public static final int FACE_CAPTURE_NOT_VERIFIED = 6;
    public static final int FACE_CAPTURE_TEMPLATE_INCOMPATIBLE = 7;

    public static final int VERIFICATION_FAILED = -1;
    public static final int VERIFICATION_PENDING = 0;
    public static final int VERIFICATION_SMS_SENT = 1;
    public static final int VERIFICATION_INPROGRESS = 2;
    public static final int VERIFICATION_SUCCESS= 3;

    public static final int VERIFICATION_FAILED_RESET_PIN       = 4;
    public static final int VERIFICATION_PENDING_RESET_PIN      = 5;
    public static final int VERIFICATION_SMS_SENT_RESET_PIN     = 6;
    public static final int VERIFICATION_INPROGRESS_RESET_PIN   = 7;
    public static final int VERIFICATION_SUCCESS_RESET_PIN      = 8;

    public static final String TOKEN_SECRET = "123456";

    public static final int DEFAULT_LIMIT = 50;

    public static final String WALLET_ENUM_CHANNEL      = "STUDO";
    public static final boolean FEATURE_SMS_OTP_BANKDKI = true;
    public static final boolean API_MANAGEMENT          = true; //true = pakai api management , false = pakai api yang lama
    public static final boolean API_MANAGEMENT_DEV      = false; //url status api management DEV = true, PROD = false

    public static final String PACKAGE_APP_JAKONE_MOBILE = "com.bankdki.jakone.mobile";

    private static Config instance;

    public static Config getInstance() {
        if (instance == null) {
            throw new IllegalStateException();
        }

        return instance;
    }

    public Config(){
        super();
        instance = this;
    }
}
